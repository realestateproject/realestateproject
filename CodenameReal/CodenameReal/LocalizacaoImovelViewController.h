//
//  LocalizacaoImovelViewController.h
//  CodenameReal
//
//  Created by jesse filho on 11/4/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface LocalizacaoImovelViewController : UIViewController <CLLocationManagerDelegate,UITextFieldDelegate, MKMapViewDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UITextField *textField;

- (IBAction)textField:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *myAddress;
@property (strong, nonatomic) CLLocation *location;
@property (strong, nonatomic) CLLocationManager *locationManager;
@end
