//
//  PularAvancarViewController.swift
//  CodenameReal
//
//  Created by jesse filho on 11/23/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

import UIKit

class PularAvancarViewController: UIViewController {

    @IBAction func voltar(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    @IBAction func pularEtapa(sender: AnyObject) {
        
        let alertView = UIAlertController(title: "Opa!", message: "Precisamos de mais algumas confirmações, não desista. São mais três telas =)", preferredStyle: .Alert)
        alertView.addAction(UIAlertAction(title: "Continuar", style: .Default, handler: { (alertAction) -> Void in
            print("Continuar")
            self.performSegueWithIdentifier("avancarProfile", sender: nil)
            
        }))
        alertView.addAction(UIAlertAction(title: "Pular", style: .Cancel, handler: { (alertAction) -> Void in
            print("Pular")
            self.dismissViewControllerAnimated(true, completion: nil)
        }))
        presentViewController(alertView, animated: true, completion: nil)

    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
