//
//  AppDelegate.m
//  CodenameReal
//
//  Created by Matheus Coelho Berger on 10/6/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

#import "AppDelegate.h"
#import <PayPalMobile.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <CoreData/CoreData.h>

#import "CodenameReal-Swift.h"
#import <Foundation/Foundation.h>
#import <SystemConfiguration/SystemConfiguration.h>


@interface AppDelegate ()
@property (retain, nonatomic) NSManagedObjectModel* managedObjectModel;
@property (retain, nonatomic) NSURL* applicationDocumentsDirectory;
@property (retain, nonatomic) NSPersistentStoreCoordinator* persistentStoreCoordinator;
@property (retain, nonatomic) NSManagedObjectContext* managedObjectContext;
@end

@implementation AppDelegate

+(instancetype) sharedDelegate {
    return (AppDelegate*)[UIApplication sharedApplication].delegate;
}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    // remove the top line of tab bar
    [[UITabBar appearance] setShadowImage:[[UIImage alloc] init]];
    // remove the bottom line of navigation
    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
#pragma "Enter your credentials"
    [PayPalMobile initializeWithClientIdsForEnvironments:@{
                                                           
                                                           PayPalEnvironmentSandbox : @"Af4HM_bzqqQBbpAaVmSm4IMf_iayViJrfiBcGAPvjiKukjIYs7njeWKbvWQfAsdxs81NguUAHZtCvbpV"}];
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                    didFinishLaunchingWithOptions:launchOptions];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBSDKAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}
#pragma mark - Core Data lazy variables 

- (NSURL*)getApplicationDocumentsDirectory{
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.razeware.HitList" in the application's documents Application Support directory.
    if (self.applicationDocumentsDirectory) {
        return self.applicationDocumentsDirectory;
    } else {
        NSArray *urls;
        urls = [[NSFileManager defaultManager] URLsForDirectory: NSDocumentDirectory inDomains: NSUserDomainMask];
        self.applicationDocumentsDirectory = ((NSURL*)urls[[urls count]-1]);
        return self.applicationDocumentsDirectory;
    }
}

- (NSManagedObjectModel*)getManagedObjectModel {
    // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
    if (self.managedObjectModel) {
        return self.managedObjectModel;
    } else {
        NSURL* modelURL = [[NSBundle mainBundle] URLForResource: @"CodenameRealDataModel" withExtension:@"momd"];
        self.managedObjectModel =  [[NSManagedObjectModel alloc]initWithContentsOfURL:modelURL];
        return self.managedObjectModel;
    }
}

- (NSPersistentStoreCoordinator*) getPersistentStoreCoordinator{
    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
    // Create the coordinator and store
    if (self.persistentStoreCoordinator) {
        return self.persistentStoreCoordinator;
    }else{
        NSPersistentStoreCoordinator* coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel: [self getManagedObjectModel]];
        NSURL* url = [self.getApplicationDocumentsDirectory URLByAppendingPathComponent:@"alluDB.sqlite"];//("SingleViewCoreData.sqlite")
        //    NSString* failureReason = @"There was an error creating or loading the application's saved data.";
        
        NSError *error;
        [coordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:url options:nil error:&error];
        
        if (error) {
            NSLog(@"%@",error.description);
            return nil;
        }else{
            self.persistentStoreCoordinator = coordinator;
            return coordinator;
        }
    }
}

- (NSManagedObjectContext*)getManagedObjectContext{
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
    if (self.managedObjectContext) {
        return self.managedObjectContext;
    }else{
        NSPersistentStoreCoordinator* coordinator = [self getPersistentStoreCoordinator];
        self.managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];//  (concurrencyType: .MainQueueConcurrencyType)
        self.managedObjectContext.persistentStoreCoordinator = coordinator;
        return self.managedObjectContext;
    }
    
}

#pragma mark - Core Data Saving support

- (void)saveContext{
    if (self.managedObjectContext.hasChanges) {
        NSError* error;
        [self.managedObjectContext save:&error];
        if (error) {
            NSLog(@"Erro: %@",error.description);
        }
    }
}


@end

@implementation UINavigationItem (myCustomization)

-(UIBarButtonItem *)backBarButtonItem
{
    return [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
}

@end
