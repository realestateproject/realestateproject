
//
//  LocationManager.swift
//  CodenameReal
//
//  Created by Thiago Borges Jordani on 11/30/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

import Foundation
import CoreLocation
import CloudKit

class LocationManager : NSObject{
    let database: CKDatabase
    var ckLocation: LocationRecord?
    init(database: CKDatabase) {
        self.database = database
    }
    
    
    //MARK: Get locations for locatario
    func fetchLocationsForLocatarioWithEmail(emailLocatario: String, callback: ((success: Bool, callbackLocation: [LocationRecord]?) -> ())?){
    
        let predicate = NSPredicate(format: "EmailLocatario == %@", emailLocatario)
        let query = CKQuery(recordType: "Location" , predicate: predicate)
        database.performQuery(query, inZoneWithID: nil) { (records, error) -> Void in
            if error != nil {
                // erro com conexao cloudkit
                print("There was an error: \(error)")
                callback?(success: false, callbackLocation: [])
            }else{
                let returnArray = NSMutableArray()
                let locationPropertiesArray = NSMutableArray()
                
                for recor in records!{
                    let aux = LocationRecord(record: recor)
                    returnArray.addObject(aux)
                    locationPropertiesArray.addObject(aux.owner.recordID)
                }
                //FAZER ESSA PORRA
                if records?.count > 0{
                    let fetchRecordsOperation = CKFetchRecordsOperation(recordIDs: locationPropertiesArray.copy() as! [CKRecordID])
                    fetchRecordsOperation.fetchRecordsCompletionBlock = {ownerDics, error in
                        if(error != nil){
                            print("There was an error: \(error)")
                            callback?(success: false, callbackLocation: returnArray.copy() as? [LocationRecord])
                        }else{
                            for auxRecord in returnArray {
                                for auxDic in ownerDics! {
                                    if auxDic.0 ==  (auxRecord as! LocationRecord).owner.recordID{//(auxRecord as! LocationRecord).owner.recordID == (Array(auxOwnr.keys)[0] as! CKRecordID){
                                        (auxRecord as! LocationRecord).ownerRecord = Users(record: auxDic.1)
                                    }
                                }
                            }
                            callback?(success: true, callbackLocation: returnArray.copy() as? [LocationRecord])
                        }
                    }
                    self.database.addOperation(fetchRecordsOperation)
                }else{
                    callback?(success: false, callbackLocation: [])
                }
            }
        }
    }
    
    //MARK: Get locations for locador
    func fetchLocationsFromUserEmail(userEmail: String, callback: ((success: Bool, callbackLocation: [LocationRecord]?) -> ())?){
        let predicate = NSPredicate(format: "EmailDono == %@", userEmail)
        let query = CKQuery(recordType: "Location" , predicate: predicate)
        database.performQuery(query, inZoneWithID: nil) { (records, error) -> Void in
            if error != nil {
                // erro com conexao cloudkit
                print("There was an error: \(error)")
                callback?(success: false, callbackLocation: [])
            }else{
                let returnArray = NSMutableArray()
                let locationPropertiesArray = NSMutableArray()
                
                for recor in records!{
                    let aux = LocationRecord(record: recor)
                    returnArray.addObject(aux)
                    locationPropertiesArray.addObject(aux.owner.recordID)
                }
                //FAZER ESSA PORRA
                if records?.count > 0{
                    let fetchRecordsOperation = CKFetchRecordsOperation(recordIDs: locationPropertiesArray.copy() as! [CKRecordID])
                    fetchRecordsOperation.fetchRecordsCompletionBlock = {ownerDics, error in
                        if(error != nil){
                            print("There was an error: \(error)")
                            callback?(success: false, callbackLocation: returnArray.copy() as? [LocationRecord])
                        }else{
                            for auxRecord in returnArray {
                                for auxDic in ownerDics! {
                                    if auxDic.0 ==  (auxRecord as! LocationRecord).owner.recordID{//(auxRecord as! LocationRecord).owner.recordID == (Array(auxOwnr.keys)[0] as! CKRecordID){
                                        (auxRecord as! LocationRecord).ownerRecord = Users(record: auxDic.1)
                                    }
                                }
                            }
                            callback?(success: true, callbackLocation: returnArray.copy() as? [LocationRecord])
                        }
                    }
                    self.database.addOperation(fetchRecordsOperation)
                }else{
                    callback?(success: false, callbackLocation: [])
                }
            }
        }
    }
    
    func fetchAllLocationsFromEmail (userEmail: String, callback: ((success: Bool, callbackLocation: [LocationRecord]?) -> ())?){
        let predicate = NSPredicate(format: "EmailDono == %@", userEmail)
        let query = CKQuery(recordType: "Location" , predicate: predicate)
        
         database.performQuery(query, inZoneWithID: nil) { (records, error) -> Void in
            if error != nil {
                print("There was an error: \(error)")
                callback?(success: false, callbackLocation: nil)
            } else{
                
                let returnArray = NSMutableArray()
                let locationPropertiesArray = NSMutableArray()
                
                for recor in records!{
                    let aux = LocationRecord(record: recor)
                    returnArray.addObject(aux)
                    //locationPropertiesArray.addObject(aux.owner.recordID)
                }
                
                
                print("Record fetched successfully")
                if let results = records {
                    print("\(results.count)")
                    if results.count >= 0{
                        let fetchRecordsOperation = CKFetchRecordsOperation(recordIDs: locationPropertiesArray.copy() as! [CKRecordID])
                        
                        
                        fetchRecordsOperation.fetchRecordsCompletionBlock = {ownerDics, error in
                            if(error != nil){
                                print("There was an error: \(error)")
                                callback?(success: false, callbackLocation: returnArray.copy() as? [LocationRecord])
                            }else{
                                for auxRecord in returnArray {
                                    for auxDic in ownerDics! {
                                        if auxDic.0 ==  (auxRecord as! LocationRecord).owner.recordID{//(auxRecord as! LocationRecord).owner.recordID == (Array(auxOwnr.keys)[0] as! CKRecordID){
                                            (auxRecord as! LocationRecord).ownerRecord = Users(record: auxDic.1)
                                        }
                                    }
                                }
                                callback?(success: true, callbackLocation: returnArray.copy() as? [LocationRecord])
                            }
                        }
                        self.database.addOperation(fetchRecordsOperation)
                        
                        
                        // end of if
                        
                    } else {
                        callback?(success: false, callbackLocation: nil )
                    }
                }
            }
    
    
        }
    }
    
    //MARK: Find location with PIN
    func fetchPIN(location: LocationRecord, callback: ((success: Bool, callbackLocation: LocationRecord?) -> ())?){
        let predicate = NSPredicate(format: "Pin == %@", location.pin)
        let query = CKQuery(recordType: location.record.recordType , predicate: predicate)
        database.performQuery(query, inZoneWithID: nil) { (records, error) -> Void in
            if error != nil {
                // erro com conexao cloudkit
                print("There was an error: \(error)")
                callback?(success: false, callbackLocation: nil)
            }
            else {
                print("Record saved successfully")
                if let results = records {
                    print("\(results.count)")
                    if results.count == 0{
                        callback?(success: false, callbackLocation: self.ckLocation)
                    } else {
                        
                        //return all location
                        self.ckLocation = LocationRecord(record: results[0])
                        callback?(success: true, callbackLocation: self.ckLocation)
                    }
                }
            }
        }
        
    }
    
    //MARK: Find location with location record
    func checkLocation(location: LocationRecord, callback: ((success: Bool, callbackLocation: LocationRecord?) -> ())?){
        let predicate = NSPredicate(format: "RecordID == %@", location.record.recordID)
        let query = CKQuery(recordType: location.record.recordType , predicate: predicate)
        database.performQuery(query, inZoneWithID: nil) { (records, error) -> Void in
            if error != nil {
                // erro com conexao cloudkit
                print("There was an error: \(error)")
                callback?(success: false, callbackLocation: nil)
            }
            else {
                print("Record saved successfully")
                if let results = records {
                    print("\(results.count)")
                    if results.count == 0{
                        callback?(success: false, callbackLocation: self.ckLocation)
                    } else {
                        self.ckLocation = LocationRecord(record: results[0])
                        callback?(success: true, callbackLocation: self.ckLocation)
                    }
                }
            }
        }
    }
    
    //MARK: Add location to CK
    func publishLocation(location: LocationRecord, callback: ((success: Bool, callbackLocation: LocationRecord?) -> ())?){
        
        let modifyRecordsOperation = CKModifyRecordsOperation(recordsToSave: [location.record,(location.ownerRecord!.record)], recordIDsToDelete: [])
        modifyRecordsOperation.savePolicy = CKRecordSavePolicy.AllKeys
        modifyRecordsOperation.modifyRecordsCompletionBlock = { savedRecords, deletedRecordIDs, error in
            NSLog("Completed Save to cloud")
            if error != nil {
                print("There was an error: \(error)")
                callback?(success: false, callbackLocation: nil)
            } else {
                print("Record saved successfully")
                callback?(success: true, callbackLocation: nil)
            }
            
        }
        
        database.addOperation(modifyRecordsOperation)
        
    }
    //MARK: Associar locação à um locatario
    //MARK: Update location with location record + Email
    func updateLocation(location: LocationRecord, callback:((success: Bool) -> ())?){
        let updateOperation = CKModifyRecordsOperation(recordsToSave: [location.record], recordIDsToDelete: nil)
        updateOperation.perRecordCompletionBlock = { record, error in
            if error != nil {
                // Really important to handle this here
                print("Unable to modify record: \(record). Error: \(error)")
            }
        }
        updateOperation.modifyRecordsCompletionBlock = { saved, _, error in
            if let error = error {
                if error.code == CKErrorCode.PartialFailure.rawValue {
                    print("There was a problem completing the operation. The following records had problems: \(error.userInfo[CKPartialErrorsByItemIDKey])")
                }
                callback?(success: false)
            } else {
                callback?(success: true)
            }
        }
        database.addOperation(updateOperation)
    }
    
    
}
