//
//  RecoverTesteViewController.swift
//  CodenameReal
//
//  Created by Thiago Borges Jordani on 10/22/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

import UIKit


class RecoverTesteViewController: UIViewController {
    @IBOutlet weak var fotoVC: UIImageView!
    @IBOutlet weak var nameVC: UILabel!
    @IBOutlet weak var cpfVC: UILabel!
    @IBOutlet weak var rgVC: UILabel!
    @IBOutlet weak var mName: UILabel!
    @IBOutlet weak var fNameVC: UILabel!
    @IBOutlet weak var gender: UILabel!
    @IBOutlet weak var civilEst: UILabel!
    @IBOutlet weak var prof: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let helper = CoreDataHelper.init()
        
        var pessoas = helper.fetchWithEntity("Pessoa")
        let pessoa = pessoas[0]
        
        nameVC.text = pessoa.valueForKey("name") as! String
        
        fotoVC.image = UIImage(data: pessoa.valueForKey("picture") as! NSData)
    }
    
//    override func viewWillAppear(animated: Bool) {
//        super.viewWillAppear(animated)
//    }
}