//
//  barraConfirmacao.swift
//  CodenameReal
//
//  Created by jesse filho on 12/2/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//
import UIKit

protocol ButtonConfirmacaoClickedDelegate {
    func confirmacao()
}

class barraConfirmacao: UIView {
    
   
    
    var delegate : ButtonConfirmacaoClickedDelegate!
    
    //    class func instanceFromNib() -> UIView {
    //        return UINib(nibName: "nib file name", bundle: nil).instantiateWithOwner(nil, options: nil)[0] as UIView
    //    }
    
    
    @IBAction func confirmacao(sender: UIButton) {
        print("confirmado")
        
        delegate.confirmacao()
        
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    convenience init()
    {
        self.init()
    }
}

extension UIView {
    class func loadFromNibNamedConfirmacao(nibNamed: String, bundle : NSBundle? = nil) -> UIView? {
        return UINib(
            nibName: nibNamed,
            bundle: bundle
            ).instantiateWithOwner(nil, options: nil)[0] as? UIView
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
override func drawRect(rect: CGRect) {
// Drawing code
}
*/
