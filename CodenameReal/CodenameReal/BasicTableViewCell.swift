//
//  BasicTableViewCell.swift
//  CodenameReal
//
//  Created by Matheus Coelho Berger on 1/19/16.
//  Copyright © 2016 Matheus Coelho Berger. All rights reserved.
//

import UIKit

class BasicTableViewCell: UITableViewCell {

    @IBOutlet weak var left: UILabel!
    @IBOutlet weak var right: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
