//
//  BuscaViewController.m
//  CodenameReal
//
//  Created by Matheus Coelho Berger on 11/27/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

#import "BuscaViewController.h"

@interface BuscaViewController ()

@property (weak, nonatomic) IBOutlet UITextField *PIN;

@end

@implementation BuscaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.PIN.delegate = self;
    [self.PIN setKeyboardType:UIKeyboardTypeNumberPad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goToNewCard:) name:@"addCard" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

- (IBAction)procurarImóvel:(id)sender
{
    [self textFieldShouldReturn:self.PIN];
    
    [self performSegueWithIdentifier:@"goToImovel" sender:self];
}

- (void)goToNewCard: (NSNotification *)notification
{
    [self.tabBarController setSelectedIndex:0];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/




@end
