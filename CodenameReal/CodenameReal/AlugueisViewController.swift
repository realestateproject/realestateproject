//
//  AlugueisViewController.swift
//  CodenameReal
//
//  Created by Matheus Coelho Berger on 1/12/16.
//  Copyright © 2016 Matheus Coelho Berger. All rights reserved.
//

import UIKit
import CloudKit
import CoreData

class AlugueisViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    let locationManager = LocationManager(database: CKContainer.defaultContainer().publicCloudDatabase)
    var location = LocationRecord()
    
    //let usersManager = UsersManager(database: CKContainer.defaultContainer().publicCloudDatabase)
    var user = Users()
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var valorTotal: UILabel!
    @IBOutlet weak var valorAtrasado: UILabel!
    
    var meusAlugueis : NSArray = []
    
     var sumValorTotal = 0.0
    
    func loadLocacoes(){
        print("Entrou em loadLocacoes -- Buscando locacoes")
        //print(CodenameRealStaticObjects.locatarioImoveis.count)
        
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let context = appDelegate.getManagedObjectContext()
        let fetchRequest = NSFetchRequest(entityName: "MeusAlugueis")
        fetchRequest.predicate = NSPredicate(format: "emaildoLocatario == %@", CodenameRealStaticObjects.loggedUser.email)

        do {
            let results = try context.executeFetchRequest(fetchRequest)
          print("Numero de imoveis alugados pelo usuario \(results.count)")
            
            if(results.count > 0){
               print(results)
            self.meusAlugueis = results as AnyObject! as! NSArray
                
                
                
                
        }
            

            
            dispatch_async(dispatch_get_main_queue(),{
                self.loadTableView()
                self.tableView.reloadData()
                self.sum()
                self.indicatorLoadStop()
                //self.navigationController?.popToRootViewControllerAnimated(true)
            })
        } catch let error as NSError {
            // failure
            print("Fetch failed: \(error.localizedDescription)")
            dispatch_async(dispatch_get_main_queue(),{
                self.indicatorLoadStop()
            })
        }

        
       

    }    
//    func sum ( valorAluguel : Double){
//        
//        self.sumValorTotal =  self.sumValorTotal + valorAluguel
//        
//        self.valorTotal.text = String(self.sumValorTotal)
//    }
    
    func sum (){
        
        
        
        for  val in self.meusAlugueis{
            self.sumValorTotal = self.sumValorTotal + (val.valueForKey("valorAluguel") as! Double)
            print("valor total de imóveis disponíveis para alugar \(self.sumValorTotal)")
        }
        self.valorTotal.text = String(self.sumValorTotal)
        self.sumValorTotal = 0.0
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        let nibName = UINib(nibName: "ImovelTableViewCell", bundle:nil)
        self.tableView.registerNib(nibName, forCellReuseIdentifier: "ImovelCell")
        self.tableView.rowHeight = 440.0
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("goToCheckout:"), name: "goToCheckout", object: nil)


    
    }
    
    func loadTableView (){
        print("Carregou métodos da tableview")
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func viewWillAppear(animated: Bool) {
        self.indicatorLoadStart()
        dispatch_async(dispatch_get_main_queue(),{
            print("Testando conexão e ao fim, irá carregar os dados")
            self.testeConexao()
        })
        
//        self.tableView.reloadData()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - TableView
    
    
    //Delegate
    
    func customizarTableView(tableView: UITableView) -> Bool
    {
        
        return true
    }
    
    //Datasource
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        print("<<<<<<<<< numero de linhas \(self.meusAlugueis.count)" )
        return self.meusAlugueis.count

    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("ImovelCell", forIndexPath: indexPath) as! ImovelTableViewCell
        
        
        
        if(cell != 0 ){
            let locationCell = self.meusAlugueis[indexPath.row]
            print("\((locationCell.valueForKey("valorAluguel")))")
            
            
            if(locationCell.valueForKey("tituloLocacao") as! String == ""){
                cell.tituloImovel.text = "Sem título"
            }else{
                cell.tituloImovel.text = locationCell.valueForKey("tituloLocacao") as? String
            }
            
            
//            if(locationCell.pin == ""){
//                cell.donoImovel.text = "Sem PIN"
//            }else{
//                cell.donoImovel.text = "PIN:  " + locationCell.pin
//            }
            
            if(locationCell.valueForKey("fotoLocacao")! as! NSData == 0){
                cell.imagemImovel.image = UIImage(named: "Foto_Vazia_750x612")
            }else{
                cell.imagemImovel.image = UIImage(data: locationCell.valueForKey("fotoLocacao")! as! NSData)
            }
            
            
            if(locationCell.valueForKey("valorAluguel") as! Double == 0.0){
                cell.valorAluguel.text = "0,00"
            }else{
                let valAlTot = locationCell.valueForKey("valorAluguel") as! Double
                cell.valorAluguelData =  String(valAlTot)

            }
            
            if(locationCell.valueForKey("dataVencimento") as! Int == 0){
                cell.dataVencimento.text = "00/00/0000"
            }else{
                cell.dataVencimentoData = String(locationCell.valueForKey("dataVencimento") as! Int)
            }
            
        }
        return cell

    }
    
    //MARK: - Observations
    
    func goToCheckout(notification: NSNotification)
    {
        self.performSegueWithIdentifier("goToCheckout", sender: self)
    }
    

    // MARK: - activity Indicator
    
    var activityIndicator = UIActivityIndicatorView()
    
    func indicatorLoadStart (){
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.White)
        activityIndicator.backgroundColor = UIColor(white: 0.1, alpha: 0.7)
        activityIndicator.frame = self.view.frame
        activityIndicator.center = self.view.center
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        self.view.bringSubviewToFront(activityIndicator)
    }
    
    func indicatorLoadStop(){
        activityIndicator.stopAnimating()
        activityIndicator.removeFromSuperview()
    }
    // MARK: - Teste de Conexão
    func testeConexao (){
        let status = Reach().connectionStatus()
        switch status {
        case .Unknown, .Offline:
            print("Not connected")
            
            let alertController = UIAlertController(title: "Sem Conexão", message: "Verifique sua conexão com a internet", preferredStyle: .Alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .Default) { (action) in
                dispatch_async(dispatch_get_main_queue(),{
                    // testar essa ação.
                    self.indicatorLoadStop()
                    self.navigationController?.popToRootViewControllerAnimated(true)
                })
            }
            alertController.addAction(OKAction)
            
            self.presentViewController(alertController, animated: true) {
                // ...
            }
        case .Online(.WWAN):
            print("Connected via WWAN")
            //self.loadLocacoes()
            
        case .Online(.WiFi):
            print("Connected via WiFi")
            
            dispatch_async(dispatch_get_main_queue(),{
                print("Dados carregados")
                self.loadLocacoes()
            })
            
        }
    }
    
    func alertErro(msg : String){
        let alert = UIAlertController(title: "Opss!", message: msg, preferredStyle: UIAlertControllerStyle.Alert)
        
        
        alert.addAction(UIAlertAction(title: "Certo", style: .Default, handler: { action in
            switch action.style{
            case .Default:
                self.indicatorLoadStop()
                self.navigationController?.popViewControllerAnimated(true)
            case .Cancel:
                print("cancel")
            case .Destructive:
                print("destructive")
            }
        }))
        
        dispatch_async(dispatch_get_main_queue(),{
            self.indicatorLoadStop()
            self.presentViewController(alert, animated: true, completion: nil)
            
        })
    }
    
}
