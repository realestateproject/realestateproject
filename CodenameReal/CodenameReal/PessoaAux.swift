//
//  PessoaAux.swift
//  CodenameReal
//
//  Created by Thiago Borges Jordani on 11/18/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

import Foundation

class PessoaAux : NSObject {
    
     var cpf: String?
     var estadoCivil: String?
     var father: String?
     var gender: String?
     var idPessoa: NSNumber?
     var mother: String?
     var name: String?
     var picture: NSData?
     var profissao: String?
     var rg: String?
}