//
//  EditarPerfilTableViewController.swift
//  CodenameReal
//
//  Created by jesse filho on 1/12/16.
//  Copyright © 2016 Matheus Coelho Berger. All rights reserved.
//

import UIKit
import CloudKit

class EditarPerfilTableViewController: UITableViewController, UITextFieldDelegate {
    
    let usersManager = UsersManager(database: CKContainer.defaultContainer().publicCloudDatabase)
    var user = Users()

    @IBOutlet weak var textFieldNome: UITextField!
    @IBOutlet weak var textFieldSobrenome: UITextField!
    @IBOutlet weak var textFieldAgencia: UITextField!
    @IBOutlet weak var textFieldConta: UITextField!
    @IBOutlet weak var textFieldEndereco: UITextField!
    @IBOutlet weak var textFieldCPF: UITextField!
    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var textFieldSenha: UITextField!
    
    var tipoConexao: String = ""
    
    //Modificar ou adicionar dados ao CloudKit
    @IBAction func salvarModificação(sender: AnyObject) {
        self.indicatorLoadStart()
        
        self.user.agencia = textFieldAgencia.text!
        self.user.nome = textFieldNome.text!
        self.user.sobrenome = textFieldSobrenome.text!
        self.user.conta = textFieldConta.text!
        self.user.cpf = textFieldCPF.text!
        self.user.endereco = textFieldEndereco.text!
        self.user.email = textFieldEmail.text!
        self.user.senha = textFieldSenha.text!
        
        self.user.tipoConexao = self.tipoConexao
        
        self.usersManager.update(self.user) { (success) -> () in
            if success {
                print(" dados foram modificados")
                dispatch_async(dispatch_get_main_queue(),{
                    //self.carregarDados()
                    self.alert("Seus dados foram Editados com sucesso")
                    
                })
            }else{
                dispatch_async(dispatch_get_main_queue(),{
                    //self.carregarDados()
                    self.alertErro("Algo deu errado, tente novamente mais tarde.")
                    
                })
                print("Erro ao modificar os dados")
                
            }
        }
        
    }
    
//carrega dados verificando se eles existem ou não, caso não exista é obrigatório a insercão dos mesmo
    func carregarDados(userData : Users){
        
        if(userData.nome == ""){
            print("mudar cor do text field para vermelho")
        }else{
            self.textFieldNome.text = userData.nome
        }
        
        if(userData.sobrenome == ""){
            
        }else{
            self.textFieldSobrenome.text = userData.sobrenome
        }
        
        if(userData.agencia == ""){
            self.textFieldAgencia.text = "null"
        }else{
            self.textFieldAgencia.text = userData.agencia
        }
        
        if(userData.conta == ""){
           //mudar cor do textfield
        }else{
            self.textFieldConta.text = userData.conta

        }
        
        if(userData.cpf == ""){
            //mudar cor do textfield
        }else{
            self.textFieldCPF.text = userData.cpf
            
        }
        
        if(userData.endereco == ""){
            //mudar cor do textfield
        }else{
            self.textFieldEndereco.text = userData.endereco
            
        }
        
        if(userData.email == ""){
            //mudar cor do textfield
        }else{
            self.textFieldEmail.text = userData.email
            
        }
        
        if(userData.senha == ""){
            //mudar cor do textfield
        }else{
            self.textFieldSenha.text = userData.senha
            
        }
        
     
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.indicatorLoadStart()
        self.testeConexao()
        
        
        
        self.textFieldNome.delegate = self
        self.textFieldSobrenome.delegate = self
        self.textFieldAgencia.delegate = self
        self.textFieldConta.delegate = self
        self.textFieldEndereco.delegate = self
        self.textFieldCPF.delegate = self
        self.textFieldEmail.delegate = self
        self.textFieldSenha.delegate = self
        // Do any additional setup after loading the view.
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
    }

    
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    func textViewShouldEndEditing(textView: UITextView) -> Bool {
        print(textView.text)
        return true
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    
    func buscaDadosUsuario() {
        // verificar se este email já está sendo usado
        
        user.email = CodenameRealStaticObjects.loggedUser.email //"jfnf@cin.ufpe.br"
        print(user.email)
            
            self.usersManager.checkEmail(user, callback: { (success, users) -> () in
                if success {
                    print("Dados carregados")
                    print(users?.nome)
                    dispatch_async(dispatch_get_main_queue(),{
                        self.carregarDados(users!)
                        self.indicatorLoadStop()
                    })
 
                }else{
                   print("Erro ao carregar dados em editar perfil")
                   self.alertErro("Erro ao carregar os dados, tente novamente mais tarde.")
                    dispatch_async(dispatch_get_main_queue(),{
                        self.indicatorLoadStop()
                        self.navigationController?.popToRootViewControllerAnimated(true)
                    })
                }
            })
            
        
        
    }
    // MARK: - activity Indicator
    
    var activityIndicator = UIActivityIndicatorView()
    
    func indicatorLoadStart (){
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.White)
        activityIndicator.backgroundColor = UIColor(white: 0.1, alpha: 0.7)
        activityIndicator.frame = self.view.frame
        activityIndicator.center = self.view.center
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        self.view.bringSubviewToFront(activityIndicator)
    }
    
    func indicatorLoadStop(){
        activityIndicator.stopAnimating()
        activityIndicator.removeFromSuperview()
    }
    
    func alert(msg : String){
        let alert = UIAlertController(title: "Salvo com sucesso", message: msg, preferredStyle: UIAlertControllerStyle.Alert)
        
        
        alert.addAction(UIAlertAction(title: "Certo", style: .Default, handler: { action in
            switch action.style{
            case .Default:
                
                self.indicatorLoadStop()
                 self.navigationController?.popViewControllerAnimated(true)
            case .Cancel:
                print("cancel")
                
            case .Destructive:
                print("destructive")
            }
        }))
        
        dispatch_async(dispatch_get_main_queue(),{
            self.indicatorLoadStop()
            self.presentViewController(alert, animated: true, completion: nil)
            
        })
    }
    func alertErro(msg : String){
        let alert = UIAlertController(title: "Opss!", message: msg, preferredStyle: UIAlertControllerStyle.Alert)
        
        
        alert.addAction(UIAlertAction(title: "Certo", style: .Default, handler: { action in
            switch action.style{
            case .Default:
                
                self.indicatorLoadStop()
                self.navigationController?.popViewControllerAnimated(true)
            case .Cancel:
                print("cancel")
                
            case .Destructive:
                print("destructive")
            }
        }))
        
        dispatch_async(dispatch_get_main_queue(),{
            self.indicatorLoadStop()
            self.presentViewController(alert, animated: true, completion: nil)
            
        })
    }

    // MARK: - Teste de Conexão
    func testeConexao (){
        let status = Reach().connectionStatus()
        switch status {
        case .Unknown, .Offline:
            print("Not connected")
            
            let alertController = UIAlertController(title: "Sem Conexão", message: "Verifique sua conexão com a internet e tente novamente", preferredStyle: .Alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .Default) { (action) in
                dispatch_async(dispatch_get_main_queue(),{
                    self.indicatorLoadStop()
                    self.navigationController?.popToRootViewControllerAnimated(true)
                })
                
            }
            alertController.addAction(OKAction)
            
            self.presentViewController(alertController, animated: true) {
                // ...
            }
        case .Online(.WWAN):
            print("Connected via WWAN")
            self.tipoConexao = "WWAN"
            self.buscaDadosUsuario()
            
        case .Online(.WiFi):
            print("Connected via WiFi")
            self.tipoConexao = "WiFi"
            self.buscaDadosUsuario()
        }
    }

}
