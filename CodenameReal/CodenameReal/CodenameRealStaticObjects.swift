//
//  CodenameRealStaticObjects.swift
//  CodenameReal
//
//  Created by Thiago Borges Jordani on 12/4/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

import Foundation

@objc class CodenameRealStaticObjects: NSObject{
    static var locatadorLocations: [LocationRecord]  = []
    static var locatarioImoveis = [LocationRecord]()   // imoveis alugados ou de interesse por parte do locatario
    static var updatedLocatadorLocations = false
    static var locatarioLocations: [LocationRecord]  = []
    static var updatedLocatarioLocations = false
    static var loggedUser = Users()
    
    static var alugueis = LocationRecord()
    
    static func resetObjects(){
        locatadorLocations = []
        locatarioImoveis = []
        updatedLocatadorLocations = false
    }
}