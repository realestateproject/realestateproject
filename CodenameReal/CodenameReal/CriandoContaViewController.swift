//
//  CriandoContaViewController.swift
//  CodenameReal
//
//  Created by jesse filho on 11/23/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

import CloudKit

class CriandoContaViewController: UIViewController, UITextFieldDelegate {
    
    let usersManager = UsersManager(database: CKContainer.defaultContainer().publicCloudDatabase)
    var user = Users()
 
    
    
    
    @IBOutlet weak var email: UITextField!
    
    @IBOutlet weak var senha: UITextField!
    
    
    @IBAction func verificaSenha(sender: UITextField) {
        // verificar segurança da senha
        print(senha.text)
        
        if (email.text == ""){
            let msg = "Parece que você esqueceu de colocar sua senha."
            self.alert(msg)
        }
    }
    
    @IBAction func voltar(sender: AnyObject) {
       // self.navigationController?.popViewControllerAnimated(true)
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    @IBAction func FecharTela(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    //Botao Criar Contas
    @IBAction func criarconta(sender: AnyObject) {
        super.viewDidLoad()
        dispatch_async(dispatch_get_main_queue(),{
            print("Testando conexão e ao fim, irá carregar os dados")
            self.testeConexao()
        })
        

    }
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
       
        
        if (emailbool){
            email.text = emailAux!
        }
        self.email.delegate = self
        self.senha.delegate = self
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Keyboards
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: - Recebe dados
    var emailAux: String?
    var emailbool: Bool = false
    func insereEmail(emailTexto: String){
        emailAux = emailTexto
        emailbool = true
    }
    
    // MARK: - Segue com Dados
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if (segue.identifier == "telaCadastroConta"){
            
            if ( segue.destinationViewController.respondsToSelector(Selector("insereEmail:"))){
                segue.destinationViewController.performSelector(Selector("insereEmail:"), withObject: email.text)
            }
            
            if ( segue.destinationViewController.respondsToSelector(Selector("insereSenha:"))){
                segue.destinationViewController.performSelector(Selector("insereSenha:"), withObject: senha.text)
            }
  
        }

    }

    
    // MARK: - validation account CloudKit

    func validateEmail(candidate: String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluateWithObject(candidate)
        //    validateEmail("test@google.com")     // true
        //    validateEmail("invalid@@google.com") // false
    }
    
    
    // MARK: - verify account CloudKit

    func consultaEmail() {
        // verificar se este email já está sendo usado
        print(email.text)
        
        if (!self.validateEmail(email.text!)){
            let msg = "Parece que seu email é inválido."
            self.alert(msg)
        }
        else {
            user.email = email.text!
            
            
            dispatch_async(dispatch_get_main_queue(),{
                self.indicatorLoadStart()
            })
            
            self.usersManager.checkEmail(user, callback: { (success, users) -> () in
                if success {
                    //nao crio
                    let msg = "Esse email já está cadastrado."
                    self.alert(msg)
                    print("email cadastrado")
                    
                }else{
                    //colocar cursor focado na senha
                    
                    self.user.email = self.email.text!
                    self.user.senha = self.senha.text!
                    
                    Users.novoUser.email = self.email.text!
                    
                    self.usersManager.createAccountUsers(self.user, callback: { (success, users) -> () in
                        if success {
                            print("Conta Criada")
                            
                            dispatch_async(dispatch_get_main_queue(),{
                                self.indicatorLoadStop()
                                self.performSegueWithIdentifier("telaCadastroConta", sender: nil)
                            })
                            
                        }else {
                            print("Erro em  createAccountUsers")
                        }
                    })
                }
            })
            
        }
        
    }
    
     // MARK: - activity Indicator
    
    var activityIndicator = UIActivityIndicatorView()
    
    func indicatorLoadStart (){
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.White)
        activityIndicator.backgroundColor = UIColor(white: 0.1, alpha: 0.7)
        activityIndicator.frame = self.view.frame
        activityIndicator.center = self.view.center
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        self.view.bringSubviewToFront(activityIndicator)
    }
    
    func indicatorLoadStop(){
        activityIndicator.stopAnimating()
        activityIndicator.removeFromSuperview()
    }
    
    // MARK: - Alert
    func alert(msg : String){
        let alert = UIAlertController(title: "Ops!!!", message: msg, preferredStyle: UIAlertControllerStyle.Alert)
        
        
        alert.addAction(UIAlertAction(title: "Certo", style: .Default, handler: { action in
            switch action.style{
            case .Default:
                print("default")
                
                print(" colocar foco no textfield")
                
            case .Cancel:
                print("cancel")
                
            case .Destructive:
                print("destructive")
            }
        }))
        
        dispatch_async(dispatch_get_main_queue(),{
            self.indicatorLoadStop()
            self.presentViewController(alert, animated: true, completion: nil)
            
        })
    }
    
    // MARK: - Teste de Conexão
    func testeConexao (){
        let status = Reach().connectionStatus()
        switch status {
        case .Unknown, .Offline:
            print("Not connected")
            
            let alertController = UIAlertController(title: "Sem Conexão", message: "Verifique sua conexão com a internet e tente novamente", preferredStyle: .Alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .Default) { (action) in
                dispatch_async(dispatch_get_main_queue(),{
                    self.indicatorLoadStop()
                    self.navigationController?.popToRootViewControllerAnimated(true)
                })
                
            }
            alertController.addAction(OKAction)
            
            self.presentViewController(alertController, animated: true) {
                // ...
            }
        case .Online(.WWAN):
            print("Connected via WWAN")
            self.consultaEmail()
            
            
        case .Online(.WiFi):
            print("Connected via WiFi")
            self.consultaEmail()
            
        }
    }



}
