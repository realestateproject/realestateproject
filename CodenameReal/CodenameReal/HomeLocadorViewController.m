//
//  HomeLocadorViewController.m
//  CodenameReal
//
//  Created by jesse filho on 11/24/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

#import "HomeLocadorViewController.h"
#import "ImovelHomeLocadorViewController.h"
#import "CodenameReal-Swift.h"
#import "UIColor+Locador.h"
#import <CloudKit/CloudKit.h>

@interface HomeLocadorViewController ()

@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) NSMutableArray *imoveis;
@property (weak, nonatomic) IBOutlet UIVisualEffectView *baseForPageView;

@property (weak, nonatomic) IBOutlet UILabel *moedaValorTotal;
@property (weak, nonatomic) IBOutlet UILabel *moedaValorAtrasado;

@property (weak, nonatomic) IBOutlet UILabel *valorTotal;
@property (weak, nonatomic) IBOutlet UILabel *valorAtrasado;

@end

@implementation HomeLocadorViewController


#pragma mark - Activity Indicator Methods
-(void)indicatorLoadStart{
    self.activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    self.activityIndicator.backgroundColor = [UIColor colorWithWhite:0.1 alpha:0.7];
    self.activityIndicator.frame = self.view.frame;
    self.activityIndicator.center = self.view.center;
    [self.activityIndicator startAnimating];
    [self.view addSubview: self.activityIndicator];
    [self.view bringSubviewToFront: self.activityIndicator];
}

-(void)indicatorLoadStop{
    [self.activityIndicator stopAnimating];
    [self.activityIndicator removeFromSuperview];
}

#pragma mark - Alert Methods
-(void)alert:(NSString*)text{
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Ops!!!" message:text preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Certo" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        switch (action.style){
            case UIAlertActionStyleDefault:
                NSLog(@"default");
            
                NSLog(@" colocar foco no textfield");
            
            case UIAlertActionStyleCancel:
                NSLog(@"cancel");
            
            case UIAlertActionStyleDestructive:
                NSLog(@"destructive");
        }
    }]];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alert animated:true completion:nil];
    });
}

#pragma mark - Lifecycle Methods
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
    NSLog(@"entrou viewDidLoad");
    
    self.moedaValorTotal.text = @"BRL";
    self.moedaValorAtrasado.text = @"BRL";
    
    self.valorTotal.text = @"0,00";
    self.valorAtrasado.text = @"0,00";
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(mostrarPin:) name:@"mostrarPin" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goToRelation:) name:@"goToRelation" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addCard:) name:@"addCardLocador" object:nil];
    
    self.imoveis = [[NSMutableArray alloc] init];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSLog(@"entrou viewWillAppear");
    
    self.tabBarController.tabBar.hidden = NO;
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setBackgroundColor:[UIColor locadorColor]];
    
    if (![CodenameRealStaticObjects updatedLocatadorLocations]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self indicatorLoadStart];
        });
        [[[LocationManager alloc]initWithDatabase:[[CKContainer defaultContainer] privateCloudDatabase]] fetchLocationsFromUserEmail:[CodenameRealStaticObjects loggedUser].email callback:^( BOOL success, NSArray<LocationRecord *> * results) {
            if (success) {
                self.imoveis = results.mutableCopy;
                CodenameRealStaticObjects.locatadorLocations = results.mutableCopy;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self addCard:nil];
                    [self indicatorLoadStop];
                });
                NSLog(@"Recebeu: %lu imóveis",(unsigned long)[results count]);
                
                CodenameRealStaticObjects.updatedLocatadorLocations = true;
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self indicatorLoadStop];
                });
                [self alert:@"Não foi possivel carregar imoveis"];
            }
        }];
    }else{
        dispatch_async(dispatch_get_main_queue(), ^{
            self.imoveis = CodenameRealStaticObjects.locatadorLocations.mutableCopy;
//            [self.pageController reloadInputViews];
            [self addCard:nil];
        });
    }
    
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"NovaLocacao"]) {
        [[LocacaoAux novaLocacao] resetarInstancia];
    }
}


#pragma mark - PageView

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = [(ImovelHomeLocadorViewController *)viewController index];
    
    if (index == 0)
    {
        return nil;
    }
    
    index--;
    
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = [(ImovelHomeLocadorViewController *)viewController index];
    
    if (index == [self.imoveis count] -1)
    {
        return nil;
    }
    
    index++;
    
    return [self viewControllerAtIndex:index];
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return [self.imoveis count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;
}

- (ImovelHomeLocadorViewController *) viewControllerAtIndex:(NSUInteger)index
{
    ImovelHomeLocadorViewController *imovel = [[ImovelHomeLocadorViewController alloc] init];
    
    imovel = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeContentView"];
    
    switch (index)
    {
        case 0:
            imovel.stringMoeda = @"BRL";
            
//            imovel.titulo = @"Milone's Apartament";
//            imovel.stringNomeLocador = @"Milone da Silva";
//            
//            imovel.stringValor = @"700,00";
//            imovel.stringDataVencimento = @"October 30th";
//            
//            imovel.stringStatusPagamento = @"Everything ok";
//            
//            imovel.imagemNome = @"test";
            
            imovel.index = index;
            break;
        case 1:
            imovel.stringMoeda = @"BRL";
            
//            imovel.titulo = @"Stronda's Apartament";
//            imovel.stringNomeLocador = @"Stronda Sativo";
//            
//            imovel.stringValor = @"500,00";
//            imovel.stringDataVencimento = @"November 5th";
//            
//            imovel.stringStatusPagamento = @"Everything ok";
//            
//            imovel.imagemNome = @"test";
            
            imovel.index = index;
            break;
        default:
            break;
    }
    return imovel;
}

#pragma mark - Observations

- (void) mostrarPin: (NSNotification *)notification
{
    //SEL goToCheckoutSelector = @selector(goToCheckout:);
    
    //[self performSegueWithIdentifier:@"goToCheckout" sender:self];
    
    // colcoar alertview aqui com o pin do carte
}

- (void) goToRelation: (NSNotification *)notification
{
    [self performSegueWithIdentifier:@"goToRelation" sender:self];
}

- (void)addCard: (NSNotification *) notification
{
    NSLog(@"entrou na função de add card");
    double contCusto = 0;
    for (LocationRecord* casa in self.imoveis) {
        contCusto = contCusto + (casa.valorAluguel + casa.valorIPTU)*.94;
    }
    self.valorTotal.text = [NSString stringWithFormat:@"%.2f",contCusto];
    if (!self.pageController)
    {
        ImovelHomeLocadorViewController *imovel = [self viewControllerAtIndex:0];
        NSLog(@"o view controller é: %@", imovel);
        
//        [self.imoveis addObject:imovel];
        
        NSLog(@"ele reconheceu que o page view controller nao foi criado");
        
        NSLog(@"criando pageview controller");
        
        self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
        
        self.pageController.dataSource = self;
        
        [self.pageController.view setFrame:self.baseForPageView.frame];
        
        [self.pageController setViewControllers:@[imovel] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
        
        [self addChildViewController:self.pageController];
        [self.view addSubview:self.pageController.view];
        [self.pageController didMoveToParentViewController:self];
    }
    else
    {
        NSLog(@"adicionando objeto no array");
        
//        [self.imoveis addObject:@"teste"];
        
        [self.pageController reloadInputViews];
    }
}
@end
