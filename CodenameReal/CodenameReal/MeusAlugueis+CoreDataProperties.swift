//
//  MeusAlugueis+CoreDataProperties.swift
//  CodenameReal
//
//  Created by jesse filho on 1/30/16.
//  Copyright © 2016 Matheus Coelho Berger. All rights reserved.
//

import Foundation
import CoreData

extension MeusAlugueis {
    
    @NSManaged var agenciaBankDono : String
    @NSManaged var contaBankDono : String
    @NSManaged var dataVencimento : String
    @NSManaged var fotoLocacao : NSData
    @NSManaged var nomeDono : String
    @NSManaged var tituloLocacao : String
    @NSManaged var valorAluguel : Double
    @NSManaged var emaildoLocatario : String
    
    
}