//
//  FirtScrrenLoginViewController.swift
//  CodenameReal
//
//  Created by jesse filho on 11/23/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

import UIKit

class FirtScrrenLoginViewController: UIViewController {

    @IBAction func voltar(sender: AnyObject) {
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if(CoreDataHelper.getLoggedUser()){
            let storyboard = UIStoryboard(name: "Locador", bundle: nil)
            let vc = storyboard.instantiateViewControllerWithIdentifier("TabBarControllerAddImoveis") as UIViewController
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.presentViewController(vc, animated: true, completion: nil)
            })   
        } else {
            print("bugou logout")
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
