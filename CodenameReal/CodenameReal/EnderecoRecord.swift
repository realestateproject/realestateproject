////
////  EnderecoRecord.swift
////  CodenameReal
////
////  Created by Thiago Borges Jordani on 12/3/15.
////  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
////
//
//import Foundation
//import CloudKit
//
//class EnderecoRecord: NSObject {
//    let record: CKRecord
//    
//    override init(){
//        record = CKRecord(recordType: "Endereco")
//    }
//    
//    init(endereco: EnderecoAux){
//        record = CKRecord(recordType: "Endereco")
//        
//        super.init()
//        self.cep = endereco.cep
//        self.complemento = endereco.complemento
//        self.idEndereco = endereco.idEndereco
//        self.numero = endereco.numero
//        self.rua = endereco.rua
//}
//
//    var locacaoPin: String{
//        get{
//            return(self.record.objectForKey("locacaoPin") as! String)
//        }
//        set (val){
//            record.setObject(val, forKey: "locacaoPin")
//        }
//    }
//    var cep : String {
//        get{
//            return(self.record.objectForKey("cep") as! String)
//        }
//        set (val){
//            record.setObject(val, forKey: "cep")
//        }
//    }
//    
//    var complemento : String {
//        get{
//            return(self.record.objectForKey("complemento") as! String)
//        }
//        set (val){
//            record.setObject(val, forKey: "complemento")
//        }
//    }
//    
//    var idEndereco : String {
//        get{
//            return(self.record.objectForKey("idEndereco") as! String)
//        }
//        set (val){
//            record.setObject(val, forKey: "idEndereco")
//        }
//    }
//    
//    var numero : String {
//        get{
//            return(self.record.objectForKey("numero") as! String)
//        }
//        set (val){
//            record.setObject(val, forKey: "numero")
//        }
//    }
//    
//    var rua : String {
//        get{
//            return(self.record.objectForKey("rua") as! String)
//        }
//        set (val){
//            record.setObject(val, forKey: "rua")
//        }
//    }
//
//}
//
//