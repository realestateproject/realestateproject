    //
//  TabBarLogoutController.m
//  CodenameReal
//
//  Created by jesse filho on 11/23/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

#import "TabBarLogoutController.h"
#import "CodenameReal-Swift.h"



// itens in tab bar
NSString *icon_HomeLogout_Default = @"Tabbar_1_Home_Default";
NSString *icon_HomeLogout_Selected =@"Tabbar_1_Home_Selected";
NSString *icon_BuscaLogout_Default = @"Tabbar_2_Busca_Default";
NSString *icon_BuscaLogout_Selected =@"Tabbar_2_Busca_Selected";
//NSString *icon_FavoritosLogout_Default = @"Tabbar_3_Favoritos_Default";
//NSString *icon_FavoritosLogout_Selected =@"Tabbar_3_Favoritos_Selected";
NSString *icon_PerfilLogout_Default = @"Tabbar_5_UserBar_Default";
NSString *icon_PerfilLogout_Selected =@"Tabbar_5_UserBar";


@interface TabBarLogoutController ()

@end

@implementation TabBarLogoutController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.delegate = self;
    
    
    [self ResetApparence];
    [self appearanceTabBar];
    
    
    if([CoreDataHelper getLoggedUser]){
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Locador" bundle:nil];
        UIViewController* vc = [storyboard instantiateViewControllerWithIdentifier:@"TabBarControllerAddImoveis"];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:vc animated:YES completion:^{
                
            }];
        });
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

-(void) appearanceTabBar{
    
    
    [[UITabBar appearance] setShadowImage:nil];
    UITabBarItem *tabBarItem = [self.tabBar.items objectAtIndex:0];
    UIImage *unselectedImage = [UIImage imageNamed:icon_HomeLogout_Default ];
    UIImage *selectedImage = [UIImage imageNamed:icon_HomeLogout_Selected ];
    [tabBarItem setImage: [unselectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tabBarItem setSelectedImage: selectedImage];
    
    UITabBarItem *tabBarItem1 = [self.tabBar.items objectAtIndex:1];
    UIImage *unselectedImage1 = [UIImage imageNamed:icon_BuscaLogout_Default];
    UIImage *selectedImage1 = [UIImage imageNamed:icon_BuscaLogout_Selected];
    [tabBarItem1 setImage: [unselectedImage1 imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tabBarItem1 setSelectedImage: selectedImage1];
//
//    UITabBarItem *tabBarItem2 = [self.tabBar.items objectAtIndex:2];
//    UIImage *unselectedImage2 = [UIImage imageNamed:icon_PerfilLogout_Default];
//    UIImage *selectedImage2 = [UIImage imageNamed:icon_PerfilLogout_Selected];
//    [tabBarItem2 setImage: [unselectedImage2 imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
//    [tabBarItem2 setSelectedImage: selectedImage2];
//    //
//    //    UITabBarItem *tabBarItem3 = [self.tabBar.items objectAtIndex:3];
//    //    UIImage *unselectedImage3 = [UIImage imageNamed:icon_informacoes_Default];
//    //    UIImage *selectedImage3 = [UIImage imageNamed:icon_informacoes_Selected];
//    //    [tabBarItem3 setImage: [unselectedImage3 imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
//    //    [tabBarItem3 setSelectedImage: selectedImage3];

}

-(void) ResetApparence{
    //Reset to native style bar
    [[UITabBar appearance] setShadowImage:nil];
    [[UITabBar appearance] setBackgroundImage:nil];
    [[UITabBar appearance] setTintColor:[UIColor whiteColor]];
}

@end

