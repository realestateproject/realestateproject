//
//  HomeTableViewCell.m
//  CodenameReal
//
//  Created by Matheus Coelho Berger on 10/15/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

#import "HomeTableViewCell.h"

@implementation HomeTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)irCheckout:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"goToCheckout" object:self];
}

- (IBAction)irRelacionamento:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"goToRelation" object:self];
}
@end
