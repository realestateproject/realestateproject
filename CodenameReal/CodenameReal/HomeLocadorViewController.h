//
//  HomeLocadorViewController.h
//  CodenameReal
//
//  Created by jesse filho on 11/24/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeLocadorViewController : UIViewController <UIPageViewControllerDataSource>

@property (strong, nonatomic) UIPageViewController *pageController;

@end
