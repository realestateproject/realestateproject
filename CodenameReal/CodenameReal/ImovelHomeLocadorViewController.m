//
//  ImovelHomeLocadorViewController.m
//  CodenameReal
//
//  Created by jesse filho on 11/24/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

#import "ImovelHomeLocadorViewController.h"
#import "CodenameReal-Swift.h"
#import <CloudKit/CloudKit.h>

@interface ImovelHomeLocadorViewController ()

@property (weak, nonatomic) IBOutlet UILabel *tituloImovel;
@property (weak, nonatomic) IBOutlet UILabel *nomeLocador;

@property (weak, nonatomic) IBOutlet UILabel *moedaMensalidade;
@property (weak, nonatomic) IBOutlet UILabel *valorMensalidade;
@property (weak, nonatomic) IBOutlet UILabel *dataVencimento;

@property (weak, nonatomic) IBOutlet UIImageView *imagemImovel;

@property (weak, nonatomic) IBOutlet UILabel *statusPagamento;
@property (weak, nonatomic) IBOutlet UIButton *pin;

@property (weak, nonatomic) IBOutlet UIImageView *imagemFundo;

@property (strong, nonatomic) LocationRecord *location;

@end

@implementation ImovelHomeLocadorViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    // Do any additional setup after loading the view.
    self.location = [[CodenameRealStaticObjects locatadorLocations] objectAtIndex:self.index];
    NSLog(@"%@", self.stringMoeda);
    
    self.moedaMensalidade.text = self.stringMoeda;
    
    self.tituloImovel.text = self.location.locTitle;
    self.nomeLocador.text = [CodenameRealStaticObjects loggedUser].nome;
    
    self.valorMensalidade.text = [NSString stringWithFormat:@"%.2f", (self.location.valorAluguel + self.location.valorIPTU)*.94];
    self.dataVencimento.text = [NSString stringWithFormat:@"%ld",(long)self.location.diaVencimento];
    CKAsset* fotoAsset = self.location.foto;
    NSData* fotoData = [NSData dataWithContentsOfURL:[fotoAsset fileURL ]];
    self.imagemImovel.image = [UIImage imageWithData:fotoData];
    [self.pin setTitle:self.location.pin forState:UIControlStateNormal];
//    self.pin.titleLabel.text = self.location.pin;
    self.statusPagamento.text = self.stringStatusPagamento;
    
    //[self.pint setTitle:self.pinString forState:UIControlStateNormal];
   
   // self.imagemImovel.image = [UIImage imageNamed:self.imagemNome];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)goToCheckout:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"mostrarPin" object:nil];
}

- (IBAction)goToRelation:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"goToRelation" object:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
