//
//  TipoResidenciaTableViewController.m
//  CodenameReal
//
//  Created by jesse filho on 10/29/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

#import "TipoResidenciaTableViewController.h"
#import "CodenameReal-Swift.h"

@implementation TipoResidenciaTableViewController
{
    NSArray *tableData;
    NSIndexPath* _checkedIndexPath;
    int indexPathRow;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Initialize table data
    tableData = [NSArray arrayWithObjects:@"Apartamento padrão", @"Casa de Condomínio", @"Casa de Vila", @"Casa Padrão", @"Cobertura", @"Flat", @"Kitnet / Conjugados", @"Loft", @"Residência Estudantil",  nil];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    int contador = 0;
    LocacaoAux *teste = [LocacaoAux novaLocacao];
    NSLog(@"Tipo residencia: %@",teste.tipoResidencia);
    for (NSString* tipo in tableData) {
        if ([tipo isEqualToString:teste.tipoResidencia]) {
            _checkedIndexPath = [NSIndexPath indexPathForRow:contador inSection:0];
            indexPathRow = contador;
            break;
        }else{
            contador = contador + 1;
            indexPathRow = -999;
        }
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    if(indexPathRow >= 0 && indexPathRow == indexPath.row)
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    
    cell.textLabel.text = [tableData objectAtIndex:indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Uncheck the previous checked row
    if(self.checkedIndexPath)
    {
        UITableViewCell* uncheckCell = [tableView
                                        cellForRowAtIndexPath:self.checkedIndexPath];
        uncheckCell.accessoryType = UITableViewCellAccessoryNone;
    }
    if([self.checkedIndexPath isEqual:indexPath])
    {
        self.checkedIndexPath = nil;
        [LocacaoAux novaLocacao].tipoResidencia = @"";
    }
    else
    {
        UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        self.checkedIndexPath = indexPath;
        [LocacaoAux novaLocacao].tipoResidencia = [tableData objectAtIndex:self.checkedIndexPath.row];
        self.tabBarController.selectedIndex = 1;
    }
}
@end
