//
//  LocalizacaoImovelViewController.m
//  CodenameReal
//
//  Created by jesse filho on 11/4/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

#import "LocalizacaoImovelViewController.h"
#import <CoreLocation/CoreLocation.h>
#import <AddressBookUI/AddressBookUI.h>
#import "CodenameReal-Swift.h"

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)


@implementation LocalizacaoImovelViewController

@synthesize mapView = _mapView;

UIGestureRecognizer *tapper;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _mapView.delegate = self;
    
//    self.navigationItem.title = @"Localização";
//    [self.navigationController.navigationBar pushNavigationItem:self.navigationItem animated:NO];
   
    
    tapper = [[UITapGestureRecognizer alloc]
              initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapper];
    
    
    self.textField.delegate = self;
    
    self.textField.text = [LocacaoAux novaLocacao].local.rua;
    if ([self.textField.text length] > 0) {
        [self updateMapWithLocation];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locationManager startUpdatingLocation];
    NSLog(@"%@", [self deviceLocation]);
    
    
    
}
#pragma mark - Map Methodes
- (NSString *)deviceLocation {
    return [NSString stringWithFormat:@"Sua Localização é latitude: %f longitude: %f", self.locationManager.location.coordinate.latitude, self.locationManager.location.coordinate.longitude];
}
- (IBAction)myLocation:(UIButton *)sender {
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
#ifdef __IPHONE_8_0
    if(IS_OS_8_OR_LATER) {
        // Use one or the other, not both. Depending on what you put in info.plist
        [self.locationManager requestWhenInUseAuthorization];
        [self.locationManager requestAlwaysAuthorization];
    }
#endif
    [self.locationManager startUpdatingLocation];
    
    
    self.mapView.showsUserLocation = YES;
    [ self.mapView setMapType:MKMapTypeStandard];
    [ self.mapView setZoomEnabled:YES];
    [ self.mapView setScrollEnabled:YES];
}


- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 800, 800);
    [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    // If it's the user location, just return nil.
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    // Handle any custom annotations.
    if ([annotation isKindOfClass:[MKPointAnnotation class]])
    {
        // Try to dequeue an existing pin view first.
        MKAnnotationView *pinView = (MKAnnotationView*)[self.mapView dequeueReusableAnnotationViewWithIdentifier:@"CustomPinAnnotationView"];
        if (!pinView)
        {
            // If an existing pin view was not available, create one.
            pinView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"CustomPinAnnotationView"];
            pinView.draggable = YES;
            pinView.canShowCallout = NO;
            pinView.calloutOffset = CGPointMake(0, 32);
        } else {
            pinView.annotation = annotation;
        }
        return pinView;
    }
    return nil;
}

#pragma mark - TextField Methodes
- (IBAction)textField:(UITextField *)sender {
    [self updateMapWithLocation];
}
//press return to keyboard hide
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [self updateMapWithLocation];
    [textField resignFirstResponder];
    return NO;
}

#pragma mark - View Methodes
// click on the view to keyboard hide
- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}
- (IBAction)procurar:(id)sender {
    [self.textField resignFirstResponder];
}
- (IBAction)didClickProximoButton:(UIButton *)sender {
    [LocacaoAux novaLocacao].local.rua = self.textField.text;
    self.tabBarController.selectedIndex = self.tabBarController.selectedIndex + 1;
}
- (void)updateMapWithLocation{
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [LocacaoAux novaLocacao].local.rua = self.textField.text;
    [geocoder geocodeAddressString:self.textField.text completionHandler:^(NSArray *placemarks, NSError *error) {
        if (error) {
            NSLog(@"%@", error);
        } else {
            
            CLPlacemark *placemark = [placemarks lastObject];
            MKPlacemark *placemarker = [[MKPlacemark alloc] initWithPlacemark:placemark];
            
            float spanX = 0.00725;
            float spanY = 0.00725;
            MKCoordinateRegion region = self.mapView.region;
            region.center = placemarker.region.center;
            region.span = MKCoordinateSpanMake(spanX, spanY);
            [self.mapView setRegion:region animated:YES];
            [self.mapView addAnnotation:placemarker];
        }
    }];
}

@end
