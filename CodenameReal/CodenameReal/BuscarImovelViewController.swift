//
//  BuscarImovelViewController.swift
//  CodenameReal
//
//  Created by jesse filho on 1/26/16.
//  Copyright © 2016 Matheus Coelho Berger. All rights reserved.
//

import UIKit
import MapKit
import CloudKit
import CoreData

class BuscarImovelViewController: UITableViewController, MKMapViewDelegate, ButtonConfirmacaoClickedDelegate {

    //MARK: CK Variables & Constants
    let locationManager = LocationManager(database: CKContainer.defaultContainer().publicCloudDatabase)
    var location: LocationRecord?
    
    //MARK: Variables
    var currentLocation = LocacaoAux.novaLocacao
    var pin = ""
    
    //MARK: Outlets
    
    @IBOutlet weak var photoImageView: UIImageView!
    
    @IBOutlet weak var tituloResidencia: UILabel!
    
    @IBOutlet weak var tipoResidencia: UILabel!
    
    @IBOutlet weak var valorAluguel: UILabel!
    
    @IBOutlet weak var agendamentoVisita: UILabel!
    
    @IBOutlet weak var metrosQuadrados: UILabel!
    
    @IBOutlet weak var qtnQuartos: UILabel!
    
    @IBOutlet weak var qtnBanheiro: UILabel!
    
    @IBOutlet weak var qtnVagas: UILabel!
    
    @IBOutlet weak var textDescricao: UITextView!
    
    
    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var endereco: UILabel!
    
    //@IBOutlet weak var bairro: UILabel!
    
    @IBOutlet weak var maxContrato: UILabel!
    
    @IBOutlet weak var minContrato: UILabel!
    
    @IBOutlet weak var diaPagamento: UILabel!
    @IBOutlet weak var comodidadesImovel: UITableView!
    
    
    //Celulas Comodidades
    @IBOutlet weak var celulaEspacoMobiliado: UITableViewCell!
    @IBOutlet weak var celulaAquecimento: UITableViewCell!
    @IBOutlet weak var celulaAcessibilidade: UITableViewCell!
    @IBOutlet weak var celulaElevador: UITableViewCell!
    @IBOutlet weak var celulaQuintal: UITableViewCell!
    @IBOutlet weak var celulaPiscina: UITableViewCell!
    @IBOutlet weak var celulaAcademia: UITableViewCell!
    @IBOutlet weak var celulaBrinquedoteca: UITableViewCell!
    @IBOutlet weak var celulaPortaria: UITableViewCell!
    @IBOutlet weak var celulaJardim: UITableViewCell!
    @IBOutlet weak var celulaAnimais: UITableViewCell!
    
    
    
    var locacaoAux : LocationRecord?
    // MARK: - Recebe dados
    func insereLocacao(locacaoBuscado: LocationRecord){
        locacaoAux = locacaoBuscado
        
    }
    
    //MARK: Actions
    @IBAction func maisSobreValor(sender: UIButton) {
        print("mais valor")
    }
    
    var arrayMeuAluguel = [LocationRecord]()
    func confirmacao() { // -> (success:Bool, error:String){
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let context = appDelegate.getManagedObjectContext()
        
        //Creating entities
        let entity =  NSEntityDescription.entityForName("MeusAlugueis", inManagedObjectContext:context)
        let meusAlugueis = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: context)
      
        
        let valorAlTotal = (locacaoAux?.valorAluguel)! + (locacaoAux?.valorIPTU)!
        meusAlugueis.setValue(NSNumber(double: valorAlTotal), forKey:  "valorAluguel")
        meusAlugueis.setValue(locacaoAux?.locTitle, forKey:  "tituloLocacao")
        meusAlugueis.setValue(NSData(contentsOfURL: (locacaoAux?.foto.fileURL)!), forKey:  "fotoLocacao")
        meusAlugueis.setValue(locacaoAux?.diaVencimento, forKey:  "dataVencimento")
        meusAlugueis.setValue(CodenameRealStaticObjects.loggedUser.email, forKey: "emaildoLocatario")

        print(meusAlugueis.valueForKey("valorAluguel")?.doubleValue)
        print(valorAlTotal)
        do {
            try context.save()
            self.navigationController?.popViewControllerAnimated(true)
             self.tabBarController?.selectedIndex = 0
            //return (true, "")
        } catch let error as NSError  {
            /*Adicionar codigo para o tratamento de erros referentes `a tela em que o usuario se encontra*/
            print("Could not save \(error), \(error.userInfo)")
            
            self.alert("Erro inesperado, tente novamente mais tarde. \(error), \(error.userInfo)")
            //return (false,error.description)
        }
        
        
    
        
        
        
//         NSNotificationCenter.defaultCenter().postNotification("addCard")
        
//        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//        let context = appDelegate.getManagedObjectContext()
//        
//        print("Salvar essa locação no coredata")
//
//        CodenameRealStaticObjects.locatarioImoveis = arrayMeuAluguel
//        print(CodenameRealStaticObjects.locatarioImoveis)
//        self.tabBarController?.selectedIndex = 0
//
//        for meusAlugueisObjeto in arrayMeuAluguel{
//            print("Salvo no CoreData")
//            let novoAluguel = NSEntityDescription.insertNewObjectForEntityForName("MeusAlugueis", inManagedObjectContext: context) as NSManagedObject
//            novoAluguel.setValue(meusAlugueisObjeto, forKey: "meusAlugueisObjeto")
//        }
        
//        self.tabBarController?.selectedIndex = 0

    }
    //MARK: Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mapView.delegate = self
        self.loadViewData()
        self.locateHome()
        dispatch_async(dispatch_get_main_queue(),{
            self.indicatorLoadStop()
        })
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.indicatorLoadStart()
        print("Testando conexão e ao fim, irá carregar os dados")
        self.testeConexao()
    }
    //MARK: - Table view data source
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        
        print("You selected cell #\(indexPath.row)!")
        
        switch indexPath.row{
            
        case 25:
            print("Voltando")
            //self.navigationController?.popViewControllerAnimated(true)
            
        default: break
            
        }
        
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let barraConfirma = barraConfirmacao.loadFromNibNamedConfirmacao("barraConfirmacao")! as! barraConfirmacao
        
        barraConfirma.delegate = self
        
        return barraConfirma
    }
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 80.0
    }
    // MARK: - Map delegate
    func locateHome(){
        
        let address = self.endereco.text
        print(address)
        //let address = "teles junior 65"
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(address!, completionHandler: {(placemarks, error) -> Void in
            
            let placeArray = placemarks! as [CLPlacemark]
            
            // Place details
            var placeMark: CLPlacemark!
            placeMark = placeArray[0]
            
            // Address dictionary
            print(placeMark.addressDictionary)
            
            
            // Location name
            let locationName = placeMark.addressDictionary!["Name"] as? String
            print(locationName)
            
            
            // Street address
            let street = placeMark.addressDictionary!["Thoroughfare"] as? String
            print(street)
            
            
            // subLocality address
            let subLocality = placeMark.addressDictionary!["SubLocality"] as? String
            print(subLocality)
            
            
            
            // City
            let city = placeMark.addressDictionary!["City"] as? String
            print(city)
            
            
            // Zip code
            let zip = placeMark.addressDictionary!["ZIP"] as? String
            print(zip)
            // Country
            let state = placeMark.addressDictionary!["State"] as? String
            print(state)
            
            // Country
            let country = placeMark.addressDictionary!["Country"] as? String
            print(country)
            
            
            self.mapView.addAnnotation(MKPlacemark(placemark: placeMark))
            
            let location = CLLocationCoordinate2D(latitude: placeMark.location!.coordinate.latitude, longitude: placeMark.location!.coordinate.longitude)
            self.mapView.region = MKCoordinateRegionMakeWithDistance(location, 1450, 1450)
            
            print(location)
            self.mapView.centerCoordinate = location
            
            self.endereco.text =  "Localizado próximo: " + subLocality! + ", " +  city! + ", " + state! + ", " + country!
            
            print("Endereco ok")
        })
    }
    func mapView(mapView: MKMapView,
        viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
            
            if annotation is MKUserLocation {
                //return nil so map view draws "blue dot" for standard user location
                return nil
            }
            
            let reuseId = "pin"
            var pinView = mapView.dequeueReusableAnnotationViewWithIdentifier(reuseId)
            if pinView == nil {
                pinView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
                pinView!.canShowCallout = false
                pinView!.image = UIImage(named:"Locat_Mapa_Localizador_190x190")!
                
            }
            else {
                pinView!.annotation = annotation
            }
            
            return pinView
    }
    
    
    func loadViewData() {
        
        
        self.valorAluguel.text = "R$ \(((locacaoAux?.valorAluguel)! + (locacaoAux?.valorIPTU)!))"
        
        if let tit = locacaoAux?.locTitle{
            
            self.tituloResidencia.text = tit//LocacaoAux.novaLocacao.locTitle
        }
        if let tip = locacaoAux?.tipoResidencia{
            self.tipoResidencia.text = tip
        }
        if let size = locacaoAux?.size{
            
            self.metrosQuadrados.text = "\(size)"
        }
        if let qua = locacaoAux?.bedrooms{
            self.qtnQuartos.text = "\(qua)"
        }
        if let toi = locacaoAux?.toilets{
            self.qtnBanheiro.text = "\(toi)"
        }
        if let gar = locacaoAux?.garageSpaces{
            self.qtnVagas.text = "\(gar)"
        }
        if let res = locacaoAux?.resumo{
            self.textDescricao.text = res
        }
        
        self.endereco.text = locacaoAux?.rua
        if let res = locacaoAux?.tempoMaxContrato{
            self.maxContrato.text = "\((res))"
        }
        if let res = locacaoAux?.tempoMinContrato{
            self.minContrato.text = "\((res))"
        }
        
        if let res = locacaoAux?.diaVencimento{
            self.diaPagamento.text = "Dia \((res))"
        }
        
        
        //TODO
        let data = NSData(contentsOfURL: (locacaoAux?.foto as CKAsset!).fileURL)
        self.photoImageView.image = UIImage(data: data!)
        
        
        if ((locacaoAux?.mobiliado) != nil && (locacaoAux?.mobiliado)! == 1 ) {
            
            self.celulaEspacoMobiliado.accessoryType = UITableViewCellAccessoryType.Checkmark
            
        }
        if ((locacaoAux?.elevadore) != nil && (locacaoAux?.elevadore)! == 1 ) {
            
            self.celulaElevador.accessoryType = UITableViewCellAccessoryType.Checkmark
            
        }
        if ((locacaoAux?.acessibilidade) != nil && (locacaoAux?.acessibilidade)! == 1 ) {
            
            self.celulaAcessibilidade.accessoryType = UITableViewCellAccessoryType.Checkmark
            
        }
        if ((locacaoAux?.aquecimento) != nil && (locacaoAux?.aquecimento)! == 1 ) {
            
            self.celulaAquecimento.accessoryType = UITableViewCellAccessoryType.Checkmark
            
        }
        if ((locacaoAux?.piscina) != nil && (locacaoAux?.piscina)! == 1 ) {
            
            self.celulaPiscina.accessoryType = UITableViewCellAccessoryType.Checkmark
            
        }
        if ((locacaoAux?.quintal) != nil && (locacaoAux?.quintal)! == 1 ) {
            
            self.celulaQuintal.accessoryType = UITableViewCellAccessoryType.Checkmark
            
        }
        if  ((locacaoAux?.jardim) != nil && (locacaoAux?.jardim)! == 1 ) {
            
            self.celulaJardim.accessoryType = UITableViewCellAccessoryType.Checkmark
            
        }
        if ((locacaoAux?.animais) != nil && (locacaoAux?.jardim)! == 1 ) {
            
            self.celulaAnimais.accessoryType = UITableViewCellAccessoryType.Checkmark
            
        }
        if ((locacaoAux?.portaria) != nil && (locacaoAux?.portaria)! == 1 ) {
            
            self.celulaPortaria.accessoryType = UITableViewCellAccessoryType.Checkmark
            
        }
        if  ((locacaoAux?.brinquedoteca) != nil && (locacaoAux?.brinquedoteca)! == 1 ) {
            
            self.celulaBrinquedoteca.accessoryType = UITableViewCellAccessoryType.Checkmark
            
        }
        if ((locacaoAux?.academia) != nil && (locacaoAux?.academia)! == 1 ) {
            
            self.celulaAcademia.accessoryType = UITableViewCellAccessoryType.Checkmark
            
        }
        
        
    }
    
    // MARK: - activity Indicator
    
    var activityIndicator = UIActivityIndicatorView()
    
    func indicatorLoadStart (){
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.White)
        activityIndicator.backgroundColor = UIColor(white: 0.1, alpha: 0.7)
        activityIndicator.frame = self.view.frame
        activityIndicator.center = self.view.center
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        self.view.bringSubviewToFront(activityIndicator)
    }
    
    func indicatorLoadStop(){
        activityIndicator.stopAnimating()
        activityIndicator.removeFromSuperview()
    }
    
    func alert(msg : String){
        let alert = UIAlertController(title: "Ops!!!", message: msg, preferredStyle: UIAlertControllerStyle.Alert)
        
        
        alert.addAction(UIAlertAction(title: "Certo", style: .Default, handler: { action in
            switch action.style{
            case .Default:
                print("default")
                
                print(" colocar foco no textfield")
                
            case .Cancel:
                print("cancel")
                
            case .Destructive:
                print("destructive")
            }
        }))
        
        dispatch_async(dispatch_get_main_queue(),{
            self.indicatorLoadStop()
            self.presentViewController(alert, animated: true, completion: nil)
            
        })
    }
    
    // MARK: - Teste de Conexão
    func testeConexao (){
        let status = Reach().connectionStatus()
        switch status {
        case .Unknown, .Offline:
            print("Not connected")
            
            let alertController = UIAlertController(title: "Sem Conexão", message: "Verifique sua conexão com a internet e tente novamente", preferredStyle: .Alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .Default) { (action) in
                dispatch_async(dispatch_get_main_queue(),{
                    self.indicatorLoadStop()
                    self.navigationController?.popToRootViewControllerAnimated(true)
                })
                
            }
            alertController.addAction(OKAction)
            
            self.presentViewController(alertController, animated: true) {
                // ...
            }
        case .Online(.WWAN):
            print("Connected via WWAN")
           
            
            
        case .Online(.WiFi):
            print("Connected via WiFi")
            
            
        }
    }
}