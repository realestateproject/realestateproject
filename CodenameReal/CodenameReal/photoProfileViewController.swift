//
//  photoProfileViewController.swift
//  CodenameReal
//
//  Created by jesse filho on 11/23/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

import CloudKit

class photoProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    let usersManager = UsersManager(database: CKContainer.defaultContainer().publicCloudDatabase)
    var user = Users()
    
    var activityIndicator = UIActivityIndicatorView()
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    func indicatorLoadStart (){
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.White)
        activityIndicator.backgroundColor = UIColor(white: 0.1, alpha: 0.7)
        activityIndicator.frame = self.view.frame
        activityIndicator.center = self.view.center
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        self.view.bringSubviewToFront(activityIndicator)
    }
    
    func indicatorLoadStop(){
        activityIndicator.stopAnimating()
        activityIndicator.removeFromSuperview()
    }

    
    let imagePicker = UIImagePickerController()
    
    
    @IBAction func voltar(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    var foto : Bool = false
    
    func avancarApp(){
        print("avançado")
        
        if (!foto){
            print("Sem foto null")
            self.alert("Você precisa de uma foto para ser perfil")
        }else{
            dispatch_async(dispatch_get_main_queue(),{
                self.indicatorLoadStart()
            })
            self.user.email = Users.novoUser.email
            
            self.usersManager.checkEmail(user, callback: { (success, users) -> () in
                if success {
                    
                    users?.fotoPerfil = self.imageButtonImage.image!
                    Users.novoUser.fotoPerfil = self.imageButtonImage.image!
                    //UIImageToCKAssetManager.imageToCKAsset(self.imageButtonImage.image!, tempImageName:"fotoPerfil.png")
                    self.usersManager.updateUsers(users!, callback: { (success) -> () in
                        if success{
                            dispatch_async(dispatch_get_main_queue(),{
                                self.indicatorLoadStop()
                                self.performSegueWithIdentifier("cadastroFoto", sender: nil)
                            })
                        }else{
                            print("Erro em  createAccountUsers avancar photoprofile 1")
                        }
                    })
                    
                }else{
                    print("Erro em  createAccountUsers photoprofile 2")
                }
            })
        }

    }
    @IBAction func avancar(sender: UITapGestureRecognizer) {
        dispatch_async(dispatch_get_main_queue(),{
            print("Testando conexão e ao fim, irá carregar os dados")
            self.testeConexao()
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
         imagePicker.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: - PhotoPickerController

    
    @IBAction func pickerPhotoProfile(sender: AnyObject) {
        print(" photo was pressed")
        
        let optionMenu = UIAlertController(title: nil, message: "Escolha sua opção:", preferredStyle: UIAlertControllerStyle.ActionSheet)
        optionMenu.view.alpha = 0.8
        
        let photoLibraryOption = UIAlertAction(title: "Galeria de Fotos", style: .Default, handler: { (alert: UIAlertAction!) -> Void in
            print("from library")
            
            
            //shows the photo library
            self.imagePicker.allowsEditing = true
            self.imagePicker.sourceType = .PhotoLibrary
            self.imagePicker.modalPresentationStyle = .Popover
            self.presentViewController(self.imagePicker, animated: true, completion: nil)
        })
        let cameraOption = UIAlertAction(title: "Camera", style: .Default, handler: { (alert: UIAlertAction!) -> Void in
            print("take a photo")
            //shows the camera
            self.imagePicker.allowsEditing = true
            self.imagePicker.sourceType = .Camera
            self.imagePicker.modalPresentationStyle = .Popover
            self.presentViewController(self.imagePicker, animated: false, completion: nil)
            
        })
        let cancelOption = UIAlertAction(title: "Cancelar", style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancel")
            //self.dismissViewControllerAnimated(true, completion: nil)
            //self.navigationController?.popViewControllerAnimated(true)
        })
        
        //Adding the actions to the action sheet. Camera will only show up as an option if the camera is available in the first place.
        optionMenu.addAction(cancelOption)
        optionMenu.addAction(photoLibraryOption)
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) == true {
            optionMenu.addAction(cameraOption)} else {
            print ("I don't have a camera.")
        }
        
        
        self.presentViewController(optionMenu, animated: true, completion: nil)
        
    }
    
    @IBOutlet weak var imageButton: UIButton!
    @IBOutlet weak var imageButtonImage: UIImageView!
    
    // MARK: - Image Picker Delegates
    //The UIImagePickerController is a view controller that gets presented modally. When we select or cancel the picker, it runs the delegate, where we handle the case and dismiss the modal.
    
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        print("finished picking image")
        //self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        //handle media here i.e. do stuff with photo
        
        print("imagePickerController called")
        
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        imageButtonImage.image = chosenImage
        foto = true
        //self.navigationController?.popViewControllerAnimated(true)
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        //what happens when you cancel
        //which, in our case, is just to get rid of the photo picker which pops up
        
        print("imagePickerControllerDidCancel called")
        dismissViewControllerAnimated(true, completion: nil)
        //self.navigationController?.popViewControllerAnimated(true)
        //self.tableView.reloadData()
    }
    
     // MARK: - Image CloudKit
   
    
    func alert(msg : String){
        let alert = UIAlertController(title: "Ops!!!", message: msg, preferredStyle: UIAlertControllerStyle.Alert)
        
        
        alert.addAction(UIAlertAction(title: "Certo", style: .Default, handler: { action in
            switch action.style{
            case .Default:
                print("default")
                
                print(" colocar foco no textfield")
                
            case .Cancel:
                print("cancel")
                
            case .Destructive:
                print("destructive")
            }
        }))
        
        dispatch_async(dispatch_get_main_queue(),{
            self.indicatorLoadStop()
            self.presentViewController(alert, animated: true, completion: nil)
            
        })
    }
    
    // MARK: - Teste de Conexão
    func testeConexao (){
        let status = Reach().connectionStatus()
        switch status {
        case .Unknown, .Offline:
            print("Not connected")
            
            let alertController = UIAlertController(title: "Sem Conexão", message: "Verifique sua conexão com a internet e tente novamente", preferredStyle: .Alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .Default) { (action) in
                dispatch_async(dispatch_get_main_queue(),{
                    self.indicatorLoadStop()
                    self.navigationController?.popToRootViewControllerAnimated(true)
                })
                
            }
            alertController.addAction(OKAction)
            
            self.presentViewController(alertController, animated: true) {
                // ...
            }
        case .Online(.WWAN):
            print("Connected via WWAN")
            self.avancarApp()
            
            
        case .Online(.WiFi):
            print("Connected via WiFi")
            self.avancarApp()
            
        }
    }


}
