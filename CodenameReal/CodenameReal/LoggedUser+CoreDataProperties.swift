//
//  LoggedUser+CoreDataProperties.swift
//  CodenameReal
//
//  Created by Thiago Borges Jordani on 12/2/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension LoggedUser {

    @NSManaged var email: String?
    @NSManaged var nome: String?
    @NSManaged var sobrenome: String?
    @NSManaged var foto: NSData?

}
