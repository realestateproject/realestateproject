//
//  HomeLocadorSwiftViewController.swift
//  CodenameReal
//
//  Created by Thiago Borges Jordani on 12/4/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

import UIKit
import CloudKit

class HomeLocadorSwiftViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!

    let locationManager = LocationManager(database: CKContainer.defaultContainer().publicCloudDatabase)
    var imoveis: [LocationRecord] = []
    
    @IBOutlet weak var valorTotal: UILabel!
    @IBOutlet weak var moedaValorTotal: UILabel!
    @IBOutlet weak var moedaValorAtrasado: UILabel!
    @IBOutlet weak var valorAtrasado: UILabel!

    
    var sumValorTotal = 0.0
    //Precisa carregar o array local antes de setar a tableview
    
    //MARK: Custom Methods
    func loadCardsFromCK(){
        print("Buscando locacoes")
        print(CodenameRealStaticObjects.loggedUser.email)
        self.locationManager.fetchAllLocationsFromEmail(CodenameRealStaticObjects.loggedUser.email, callback: { (success, locations) -> () in
            if success{
                print("Carregando locações em locationManager.fetchAllLocationsFromEmail")
                print("<<<<<<< SYSTEM MSG: Quantidade de locacoes pertecente ao locador de  \(CodenameRealStaticObjects.loggedUser.email) são de \(locations!.count) locações")
               
                self.imoveis = locations!
                CodenameRealStaticObjects.locatadorLocations = locations!
                CodenameRealStaticObjects.updatedLocatadorLocations = true
                dispatch_async(dispatch_get_main_queue(),{
                    print("Dados carregados")
                    self.tableView.reloadData()
                    self.sum()
                    self.indicatorLoadStop()
                })
                
            }else{
                print("Erro ao carregar os dados")
                self.alertErro("Erro ao carregar os dados, tente novamente mais tarde.")
                dispatch_async(dispatch_get_main_queue(),{
                    self.indicatorLoadStop()
                    //self.navigationController?.popToRootViewControllerAnimated(true)
                })
            }
        })

        
        //        if CodenameRealStaticObjects.updatedLocatadorLocations{
//            
//        } else {
//            self.locationManager.fetchLocationsFromUserEmail(CodenameRealStaticObjects.loggedUser.email){ (success, callbackLocation) -> () in
//                if success {
//                    CodenameRealStaticObjects.locatadorLocations = callbackLocation!
//                    CodenameRealStaticObjects.updatedLocatadorLocations = true
//                    self.addCardLocador(NSNotification(name: "", object: nil))
//                }else{
//                    print("Erro ao carregar tabelas do usuario")
//                }
//            }
//        }
    }
    
    
    
    //MARK: LifeCycle Methods
    override func viewDidLoad(){
    super.viewDidLoad()
         print("entrou viewDidLoad");
        
        self.sumValorTotal = 0.0
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("mostrarPin:"), name: "mostrarPin", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("goToRelation:"), name: "goToRelation", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("addCardLocador:"), name: "addCardLocador", object: nil)
        
        
        let nibName = UINib(nibName: "ImovelTableViewCell", bundle:nil)
        self.tableView.registerNib(nibName, forCellReuseIdentifier: "ImovelCell")
        self.tableView.rowHeight = 440.0

        
        
       
        self.moedaValorTotal.text = "R$"
        self.moedaValorAtrasado.text = "R$"
        
    }
    func loadTableView (){
        print("Carregou métodos da tableview")
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        print("entrou viewWillAppear");
        
        self.tabBarController!.tabBar.hidden = false;
        self.navigationController!.navigationBar.setBackgroundImage(nil, forBarMetrics: UIBarMetrics.Default)
        self.navigationController!.navigationBar.backgroundColor = UIColor.locadorColor()
        
        self.indicatorLoadStart()
        print("Testando conexão e ao fim, irá carregar os dados")
        self.testeConexao()
        
       self.sumValorTotal = 0.0
        
    }
    
    
    //MARK: Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "NovaLocacao") {
            LocacaoAux.novaLocacao.resetarInstancia();
        }
    }
    
    //MARK: TABLEVIEW
    
    //Delegate
    
    func customizarTableView(tableView: UITableView) -> Bool
    {
        
        return true
    }
    
    //Datasource
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        print("<<<<<<<<< numero de linhas \(self.imoveis.count)" )
        return self.imoveis.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("ImovelCell", forIndexPath: indexPath) as! ImovelTableViewCell
    
        
        if(cell != 0 ){
            let locationCell = self.imoveis[indexPath.row]
            print("\(locationCell.valorAluguel) + \(locationCell.tipoResidencia)")
            
            if(locationCell.locTitle == ""){
                cell.tituloImovel.text = "Sem título"
            }else{
                cell.tituloImovel.text = locationCell.locTitle
            }
            
            
            if(locationCell.pin == ""){
                cell.donoImovel.text = "Sem PIN"
            }else{
                cell.donoImovel.text = "PIN:  " + locationCell.pin
            }
            
            if(locationCell.foto == 0){
                cell.imagemImovel.image = UIImage(named: "Foto_Vazia_750x612")
            }else{
                cell.imagemImovel.image = UIImage(data: NSData(contentsOfURL: locationCell.foto.fileURL)!)
            }
            
            
            if(locationCell.valorAluguel == 0){
                cell.valorAluguel.text = "00,00"
            }else{
                let cellRentValue = (locationCell.valorAluguel + locationCell.valorIPTU)*0.94
                cell.valorAluguelData = String(cellRentValue)
                print("Valor do aluguel imovel \(indexPath.row): \(cellRentValue)")
                
            }
            
            if(locationCell.valorAluguel == 0){
                cell.dataVencimento.text = "00/00/0000"
            }else{
               
                cell.dataVencimentoData = String(locationCell.diaVencimento)
            }

        }

        
        return cell
    }

    func sum (){
        
        
        
        for  val in self.imoveis{
           self.sumValorTotal = self.sumValorTotal + (val.valorAluguel + val.valorIPTU)*0.94
            print("valor total de imóveis disponíveis para alugar \(self.sumValorTotal)")
        }
        self.valorTotal.text = String(self.sumValorTotal)
        self.sumValorTotal = 0.0
    }
    
    //MARK: Observations

    func mostrarPin(notification: NSNotification){
        //SEL goToCheckoutSelector = @selector(goToCheckout:);
        
        //[self performSegueWithIdentifier:@"goToCheckout" sender:self];
        
        // colcoar alertview aqui com o pin do carte
    }
    
    func goToRelation(notification: NSNotification){
        self.performSegueWithIdentifier("goToRelation", sender: self)
    }

    func customLoadView(){

        
    }
    
    func alertErro(msg : String){
        let alert = UIAlertController(title: "Opss!", message: msg, preferredStyle: UIAlertControllerStyle.Alert)
        
        
        alert.addAction(UIAlertAction(title: "Certo", style: .Default, handler: { action in
            switch action.style{
            case .Default:
                
                self.indicatorLoadStop()
                self.navigationController?.popViewControllerAnimated(true)
            case .Cancel:
                print("cancel")
                
            case .Destructive:
                print("destructive")
            }
        }))
        
        dispatch_async(dispatch_get_main_queue(),{
            self.indicatorLoadStop()
            self.presentViewController(alert, animated: true, completion: nil)
            
        })
    }
    
    // MARK: - Teste de Conexão
    func testeConexao (){
        let status = Reach().connectionStatus()
        switch status {
        case .Unknown, .Offline:
            print("Not connected")
            
            let alertController = UIAlertController(title: "Sem Conexão", message: "Verifique sua conexão com a internet", preferredStyle: .Alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .Default) { (action) in
                dispatch_async(dispatch_get_main_queue(),{
                    // testar essa ação.
                    self.indicatorLoadStop()
                    self.navigationController?.popToRootViewControllerAnimated(true)
                })
            }
            alertController.addAction(OKAction)
            
            self.presentViewController(alertController, animated: true) {
                // ...
            }
        case .Online(.WWAN):
            print("Connected via WWAN")
            //self.loadCardsFromCK()
            
        case .Online(.WiFi):
            print("Connected via WiFi")
            dispatch_async(dispatch_get_main_queue(),{
                print("Dados carregados")
                 self.loadCardsFromCK()
            })
        }
    }

    var activityIndicator = UIActivityIndicatorView()
    
    func indicatorLoadStart (){
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.White)
        activityIndicator.backgroundColor = UIColor(white: 0.1, alpha: 0.7)
        activityIndicator.frame = self.view.frame
        activityIndicator.center = self.view.center
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        self.view.bringSubviewToFront(activityIndicator)
    }
    
    func indicatorLoadStop(){
        activityIndicator.stopAnimating()
        activityIndicator.removeFromSuperview()
    }
    
    


    
}