//
//  TabController.h
//  CodenameReal
//
//  Created by jesse filho on 10/28/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResidenciaTabController : UITabBarController <UITabBarControllerDelegate>

@end
