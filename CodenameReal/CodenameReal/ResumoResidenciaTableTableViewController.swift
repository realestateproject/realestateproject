//
//  ResumoResidenciaTableTableViewController.swift
//  CodenameReal
//
//  Created by jesse filho on 11/17/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

import UIKit
import MapKit
import CloudKit


class ResumoResidenciaTableTableViewController: UITableViewController, MKMapViewDelegate, ButtonClickedDelegate {
    
    //MARK: CK Variables & Constants
    let locationManager = LocationManager(database: CKContainer.defaultContainer().publicCloudDatabase)
    var location: LocationRecord?
    
    //MARK: Variables
    var currentLocation = LocacaoAux.novaLocacao
    var pin = ""
    
    //MARK: Outlets
    @IBOutlet weak var photoImageView: UIImageView!
    
    @IBOutlet weak var tituloResidencia: UILabel!
    
    @IBOutlet weak var tipoResidencia: UILabel!
    
    @IBOutlet weak var valorAluguel: UILabel!
    
    @IBOutlet weak var agendamentoVisita: UILabel!
    
    @IBOutlet weak var metrosQuadrados: UILabel!

    @IBOutlet weak var qtnQuartos: UILabel!
    
    @IBOutlet weak var qtnBanheiro: UILabel!
    
    @IBOutlet weak var qtnVagas: UILabel!
    
    @IBOutlet weak var textDescricao: UITextView!
    
    
    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var endereco: UILabel!
    
    //@IBOutlet weak var bairro: UILabel!
    
    @IBOutlet weak var maxContrato: UILabel!
    
    @IBOutlet weak var minContrato: UILabel!
    
    @IBOutlet weak var diaPagamento: UILabel!
    
    @IBOutlet weak var comodidadesImovel: UITableView!
   
    @IBOutlet weak var pinLabel: UILabel!
    
    //Celulas Comodidades
    @IBOutlet weak var celulaEspacoMobiliado: UITableViewCell!
    @IBOutlet weak var celulaAquecimento: UITableViewCell!
    @IBOutlet weak var celulaAcessibilidade: UITableViewCell!
    @IBOutlet weak var celulaElevador: UITableViewCell!
    @IBOutlet weak var celulaQuintal: UITableViewCell!
    @IBOutlet weak var celulaPiscina: UITableViewCell!
    @IBOutlet weak var celulaAcademia: UITableViewCell!
    @IBOutlet weak var celulaBrinquedoteca: UITableViewCell!
    @IBOutlet weak var celulaPortaria: UITableViewCell!
    @IBOutlet weak var celulaJardim: UITableViewCell!
    @IBOutlet weak var celulaAnimais: UITableViewCell!
    
    //MARK: Actions
    @IBAction func maisSobreValor(sender: UIButton) {
        print("mais valor")
    }
    
    func publicar(){
     
        self.location = LocationRecord(locacao: currentLocation)
        self.location?.pin = self.pin
        self.location?.emailDono = CodenameRealStaticObjects.loggedUser.email
        self.location?.ownerRecord = CodenameRealStaticObjects.loggedUser
        self.location?.owner = CKReference(recordID: CodenameRealStaticObjects.loggedUser.record.recordID, action: CKReferenceAction.None)
        dispatch_async(dispatch_get_main_queue(),{
            self.indicatorLoadStart()
        })
        self.locationManager.publishLocation(location!) { (success, callbackLocation) -> () in
            if success {
                
                let alertView = UIAlertController(title: "Massa!", message: "Publicado com sucesso!", preferredStyle: .Alert)
                alertView.addAction(UIAlertAction(title: "Continuar", style: .Default, handler: { (alertAction) -> Void in
                    print("OK")
                    
                    LocacaoAux.novaLocacao.resetarInstancia()
                    self.navigationController?.navigationController?.popToRootViewControllerAnimated(true);
                    
                }))
                dispatch_async(dispatch_get_main_queue(),{
                    self.indicatorLoadStop()
                    //self.tabBarController?.tabBar.backgroundImage = nil
                    self.presentViewController(alertView, animated: true, completion: nil)
                    
                })
            } else {
                let alertView = UIAlertController(title: "Ops!", message: "Erro ao salvar!", preferredStyle: .Alert)
                alertView.addAction(UIAlertAction(title: "Continuar", style: .Default, handler: { (alertAction) -> Void in
                    print(" not OK")
                    
                    
                }))
                dispatch_async(dispatch_get_main_queue(),{
                    self.indicatorLoadStop()
                    self.presentViewController(alertView, animated: true, completion: nil)
                    
                })
            }
        }
        
//        self.navigationController?.popViewControllerAnimated(true)
        
//        NSNotificationCenter.defaultCenter().postNotificationName("addCardLocador", object: nil)
    }
    
    
    //MARK: Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.gerarPin()
        
        print(pin)
        self.mapView.delegate = self
        //Alterar pra reuso de tela
        //
        self.loadViewData()
        
        self.locateHome()
        
        //self.hidesBottomBarWhenPushed = true
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
         self.hidesBottomBarWhenPushed = false
        self.gerarPin()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.pin = ""
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    //MARK: - Table view data source

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        
        print("You selected cell #\(indexPath.row)!")
        
        switch indexPath.row{

        case 25:
            print("Voltando")
            self.navigationController?.popViewControllerAnimated(true)
        
        default: break
            
        }
        
    }
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let barra = barraNotificacao.loadFromNibNamedNotificacao("barraNotificacao")! as! barraNotificacao
        
        barra.delegate = self
        
        return barra
    }
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 80.0
    }

    // MARK: - Map delegate
    func locateHome(){
        
        
       let address = self.endereco.text
       //let address = "teles junior 65"
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(address!, completionHandler: {(placemarks, error) -> Void in
            
            let placeArray = placemarks! as [CLPlacemark]
            
            // Place details
            var placeMark: CLPlacemark!
            placeMark = placeArray[0]
            
            // Address dictionary
            print(placeMark.addressDictionary)
                
                
                // Location name
                let locationName = placeMark.addressDictionary!["Name"] as? String
                    print(locationName)
                
                
                // Street address
                let street = placeMark.addressDictionary!["Thoroughfare"] as? String
                    print(street)
                
            
                // subLocality address
                let subLocality = placeMark.addressDictionary!["SubLocality"] as? String
                    print(subLocality)
                    
            
            
                // City
                let city = placeMark.addressDictionary!["City"] as? String
                    print(city)
                
                
                // Zip code
                let zip = placeMark.addressDictionary!["ZIP"] as? String
                    print(zip)
                // Country
                let state = placeMark.addressDictionary!["State"] as? String
                print(state)
            
                // Country
                let country = placeMark.addressDictionary!["Country"] as? String
                    print(country)
                
            
                self.mapView.addAnnotation(MKPlacemark(placemark: placeMark))
                
                let location = CLLocationCoordinate2D(latitude: placeMark.location!.coordinate.latitude, longitude: placeMark.location!.coordinate.longitude)
                self.mapView.region = MKCoordinateRegionMakeWithDistance(location, 1450, 1450)
                
                 print(location)
                self.mapView.centerCoordinate = location
            
                self.endereco.text =  "Localizado próximo: " + subLocality! + ", " +  city! + ", " + state! + ", " + country!
            
            print("teste")
        })
    }
    
    func mapView(mapView: MKMapView,
        viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
            
            if annotation is MKUserLocation {
                //return nil so map view draws "blue dot" for standard user location
                return nil
            }
            
            let reuseId = "pin"
            var pinView = mapView.dequeueReusableAnnotationViewWithIdentifier(reuseId)
            if pinView == nil {
                pinView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
                pinView!.canShowCallout = false
                pinView!.image = UIImage(named:"Mapa_Localizador_190x190")!
                
            }
            else {
                pinView!.annotation = annotation
            }
            
            return pinView
    }
    
    //MARK: Custom Methods
    func gerarPin(){
        if(LocacaoAux.novaLocacao.pin.characters.count<4){
            let meuarray = [1,2,3,4]
            for _ in meuarray {
                pin = "\(pin)\(Int(arc4random_uniform(6) + 1))"
            }
            LocacaoAux.novaLocacao.pin = pin
        } else {
            pin = LocacaoAux.novaLocacao.pin
        }
        
    }
    func currencyStringFromNumber(number: Double) -> String {
        let formatter = NSNumberFormatter()
        formatter.numberStyle = NSNumberFormatterStyle.CurrencyStyle
        formatter.locale = NSLocale(localeIdentifier: "pt_BR")
        formatter.maximumFractionDigits = 2
        return formatter.stringFromNumber(number)!
    }
    func loadViewData() {
        
        
        self.valorAluguel.text = self.currencyStringFromNumber((currentLocation.pagamento.valorAluguel + currentLocation.pagamento.valorIPTU)*0.94)//String(format: "%.2f", self.currencyStringFromNumber((currentLocation.pagamento.valorAluguel + currentLocation.pagamento.valorIPTU)*0.94))
        //"\((currentLocation.pagamento.valorAluguel + currentLocation.pagamento.valorIPTU)*0.94)"
        self.tituloResidencia.text = LocacaoAux.novaLocacao.locTitle
        self.tipoResidencia.text = LocacaoAux.novaLocacao.tipoResidencia
        self.metrosQuadrados.text = "\(LocacaoAux.novaLocacao.size)"
        self.qtnQuartos.text = "\(LocacaoAux.novaLocacao.bedrooms)"
        self.qtnBanheiro.text = "\(LocacaoAux.novaLocacao.toilets)"
        self.qtnVagas.text = "\(LocacaoAux.novaLocacao.garageSpaces)"
        self.textDescricao.text = LocacaoAux.novaLocacao.resumo
        
        self.endereco.text = LocacaoAux.novaLocacao.local.rua
        self.maxContrato.text = "\((currentLocation.pagamento.tempoMaxContrato)) meses"
        self.minContrato.text = "\((currentLocation.pagamento.tempoMinContrato)) meses"
        self.diaPagamento.text = "Dia \((currentLocation.pagamento.diaVencimento))"
        
        //TODO
        self.photoImageView.image = LocacaoAux.novaLocacao.foto
        
        self.pinLabel.text = "PIN: \(pin)"
        
        if LocacaoAux.novaLocacao.comodidades.mobiliado {
            self.celulaEspacoMobiliado.accessoryType = UITableViewCellAccessoryType.Checkmark
        }
        
        if LocacaoAux.novaLocacao.comodidades.elevadore {
            self.celulaElevador.accessoryType = UITableViewCellAccessoryType.Checkmark
        }
    
        if LocacaoAux.novaLocacao.comodidades.acessibilidade {
            self.celulaAcessibilidade.accessoryType = UITableViewCellAccessoryType.Checkmark
        }
    
        if LocacaoAux.novaLocacao.comodidades.aquecimento {
            self.celulaAquecimento.accessoryType = UITableViewCellAccessoryType.Checkmark
        }
    
        if LocacaoAux.novaLocacao.comodidades.piscina {
            self.celulaPiscina.accessoryType = UITableViewCellAccessoryType.Checkmark
        }
    
        if LocacaoAux.novaLocacao.comodidades.quintal {
            self.celulaQuintal.accessoryType = UITableViewCellAccessoryType.Checkmark
        }
   
        if LocacaoAux.novaLocacao.comodidades.jardim {
            self.celulaJardim.accessoryType = UITableViewCellAccessoryType.Checkmark
        }
        
        if LocacaoAux.novaLocacao.comodidades.animais {
            self.celulaAnimais.accessoryType = UITableViewCellAccessoryType.Checkmark
        }
   
        if LocacaoAux.novaLocacao.comodidades.portaria {
            self.celulaPortaria.accessoryType = UITableViewCellAccessoryType.Checkmark
        }
   
        if LocacaoAux.novaLocacao.comodidades.brinquedoteca {
            self.celulaBrinquedoteca.accessoryType = UITableViewCellAccessoryType.Checkmark
        }
    
        if LocacaoAux.novaLocacao.comodidades.academia {
            self.celulaAcademia.accessoryType = UITableViewCellAccessoryType.Checkmark
        }
        }
    
    // MARK: - activity Indicator
    
    var activityIndicator = UIActivityIndicatorView()
    
    func indicatorLoadStart (){
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.White)
        activityIndicator.backgroundColor = UIColor(white: 0.1, alpha: 0.7)
        activityIndicator.frame = self.view.frame
        activityIndicator.center = self.view.center
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        self.view.bringSubviewToFront(activityIndicator)
    }
    
    func indicatorLoadStop(){
        activityIndicator.stopAnimating()
        activityIndicator.removeFromSuperview()
    }
    
    func alert(msg : String){
        let alert = UIAlertController(title: "Ops!!!", message: msg, preferredStyle: UIAlertControllerStyle.Alert)
        
        
        alert.addAction(UIAlertAction(title: "Certo", style: .Default, handler: { action in
            switch action.style{
            case .Default:
                print("default")
                
                print(" colocar foco no textfield")
                
            case .Cancel:
                print("cancel")
                
            case .Destructive:
                print("destructive")
            }
        }))
        
        dispatch_async(dispatch_get_main_queue(),{
            self.indicatorLoadStop()
            self.presentViewController(alert, animated: true, completion: nil)
            
        })
    }
}
