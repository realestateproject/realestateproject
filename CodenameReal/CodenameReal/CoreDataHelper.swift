//
//  CoreDataHelper.swift
//  CodenameReal
//
//  Created by Thiago Borges Jordani on 10/19/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

import Foundation
import CoreData


class CoreDataHelper: NSObject {
    
    
    
    
    
    // MARK: Private Methods
    
    // MARK: General Fetch with Entity
    /*
    
    Busca todas as entradas de uma entidade(tabela)
    Parametros:
        entityName - String
            Nome da tabela à ser buscada
    
    */
     func fetchWithEntity(entityName: String) -> [NSManagedObject] {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.getManagedObjectContext()
        let fetch = NSFetchRequest(entityName: entityName)
        var retorno : [NSManagedObject] = []
        
        do {
            let results = try managedContext.executeFetchRequest(fetch)
            retorno = results as! [NSManagedObject]
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        
        return retorno
    }
    
    // MARK: Example Methods for Core Data Insert
    /*
    Metodo pode ser colocado na classe de criação do usuario, ja que sera usado com baixa frequencia (cadastro), o mesmo pode ser feito para o cadastro de imóvel
    
    Copiar e colar na classe desejada, alterando o codigo de acordo com o comportamento desejado
    */
    func savePessoa() {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let context = appDelegate.getManagedObjectContext()
        let entity =  NSEntityDescription.entityForName("Pessoa", inManagedObjectContext:context)
        let person = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: context)
        
        /*
        Substituir pelos valores desejados na aplicação
        */
        person.setValue("", forKey: "name")
        person.setValue("", forKey: "cpf")
        person.setValue("", forKey: "rg")
        person.setValue("", forKey: "mother")
        person.setValue("", forKey: "father")
        person.setValue("", forKey: "estadoCivil")
        person.setValue("", forKey: "profissao")
        
        
        /*
        Checar se o usuario selecionou uma imagem e adicionar a imagem da fonte certa
        */
        if ("" != nil) {
//            let imageData: NSData = UIImageJPEGRepresentation(pictureIV.image!, 0.8)!
//            person.setValue(imageData, forKey: "picture")
        }
        do {
            try context.save()
            /*Adicionar codigo para que o aplicativo de sequencia `a execução quando não ha erros*/
            //self .performSegueWithIdentifier("CDtestSegue", sender: self)
        } catch let error as NSError  {
            /*Adicionar codigo para o tratamento de erros referentes `a tela em que o usuario se encontra*/
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    
//    func saveLocacao(ownerObject: NSManagedObject) {
//        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//        let context = appDelegate.getManagedObjectContext()
//        
//        //Creating entities
//        let entity =  NSEntityDescription.entityForName("Locacao", inManagedObjectContext:context)
//        let locacao = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: context)
//        let addressEntity = NSEntityDescription.entityForName("Endereco", inManagedObjectContext: context)
//        let address = NSManagedObject(entity: addressEntity!, insertIntoManagedObjectContext: context)
//        let locPicturesEntity = NSEntityDescription.entityForName("FotosLocacao", inManagedObjectContext: context)
//        let locPictures = NSManagedObject(entity: locPicturesEntity!, insertIntoManagedObjectContext: context)
//        /*
//        Substituir pelos valores desejados na aplicação
//        */
//        locacao.setValue("", forKey: "name")
//        //...
//        address.setValue("", forKey: "rua")
//        //...
//        //Adding references
//        locacao.setValue(ownerObject, forKey: "owner")
//        locacao.setValue(address, forKey: "local")
//        /*
//
//
//            Remover referencia para figuras da tabela de
//
//
//        */
//        locacao.setValue(locPictures, forKey: "pictures")
//    }
    static func getLoggedUser() -> Bool{
        if( CodenameRealStaticObjects.loggedUser.email.characters.count > 0 ){
            return true//Users.loggedUser
        } else {
            CodenameRealStaticObjects.loggedUser = Users()
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            let managedContext = appDelegate.getManagedObjectContext()
            let fetch = NSFetchRequest(entityName: "LoggedUser")
            var retorno : [NSManagedObject] = []
            
            do {
                let results = try managedContext.executeFetchRequest(fetch)
                retorno = results as! [NSManagedObject]
                if retorno.count>0 {
                    let fetchedLoggedUser = retorno[0] as! LoggedUser
                    CodenameRealStaticObjects.loggedUser.email = fetchedLoggedUser.email!
                    CodenameRealStaticObjects.loggedUser.nome = fetchedLoggedUser.nome!
                    CodenameRealStaticObjects.loggedUser.sobrenome = fetchedLoggedUser.sobrenome!
                    CodenameRealStaticObjects.loggedUser.fotoPerfil = UIImage(data: fetchedLoggedUser.foto!)!
                    return true
                } else {
                     print("Could not fetch, bug do milenio")
                    print("user nao está logado")
                    return false
                }
            } catch let error as NSError {
                print("Could not fetch \(error), \(error.userInfo)")
                print("user nao está logado")
                return false
            }
        }
    }
    
    static func saveLoggedUser() -> (success:Bool, error:String){
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let context = appDelegate.getManagedObjectContext()
        let entity = NSEntityDescription.entityForName("LoggedUser", inManagedObjectContext:context)
        let loggedUser = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: context)
        
        loggedUser.setValue(CodenameRealStaticObjects.loggedUser.nome, forKey: "nome")
        loggedUser.setValue(CodenameRealStaticObjects.loggedUser.sobrenome, forKey: "sobrenome")
        loggedUser.setValue(UIImageJPEGRepresentation(CodenameRealStaticObjects.loggedUser.fotoPerfil, 0), forKey: "foto")
        loggedUser.setValue(CodenameRealStaticObjects.loggedUser.email, forKey: "email")
        
        do {
            try context.save()
            return (true, "")
        } catch let error as NSError  {
            /*Adicionar codigo para o tratamento de erros referentes `a tela em que o usuario se encontra*/
            print("Could not save \(error), \(error.userInfo)")
            return (false,error.description)
        }
    }
    
    static func removeLoggedUser(completion:(success:Bool)->Void){
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let context = appDelegate.getManagedObjectContext()
        let fetchRequest = NSFetchRequest(entityName: "LoggedUser")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        do {
            try appDelegate.getPersistentStoreCoordinator().executeRequest(deleteRequest, withContext: context)
            completion(success: true)
        } catch let error as NSError {
            // TODO: handle the error
            print(error)
            completion(success: false)
        }
    }
    
    func salvarNovaLocacao(){
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let context = appDelegate.getManagedObjectContext()
        
        
        
        
        
        //Creating entities
        let entity =  NSEntityDescription.entityForName("Locacao", inManagedObjectContext:context)
        let locacao = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: context) as! Locacao
        let addressEntity = NSEntityDescription.entityForName("Endereco", inManagedObjectContext: context)
        let address = NSManagedObject(entity: addressEntity!, insertIntoManagedObjectContext: context) as! Endereco
        let locPicturesEntity = NSEntityDescription.entityForName("FotosLocacao", inManagedObjectContext: context)
        let locPictures = NSManagedObject(entity: locPicturesEntity!, insertIntoManagedObjectContext: context) as! FotosLocacao

        locacao.bedrooms = NSNumber(integer:LocacaoAux.novaLocacao.bedrooms)
        locacao.contratoIptu = NSNumber(integer:LocacaoAux.novaLocacao.bedrooms)
        locacao.garageSpaces = NSNumber(integer:LocacaoAux.novaLocacao.garageSpaces)
        locacao.salas = NSNumber(integer:LocacaoAux.novaLocacao.salas)
        locacao.size = NSNumber(integer:LocacaoAux.novaLocacao.size)
        locacao.tipoLocacao = NSNumber(integer:LocacaoAux.novaLocacao.tipoLocacao)
        locacao.toilets = NSNumber(integer:LocacaoAux.novaLocacao.toilets)
//        locacao.bedrooms = NSNumber(integer:LocacaoAux.novaLocacao.bedrooms)
//        locacao.bedrooms = NSNumber(integer:LocacaoAux.novaLocacao.bedrooms)
    }
    
    //MARK: Meu Aluguel
    
    //func salvarNovoAluguel() -> (success:Bool, error:String){
       
    
   

}
