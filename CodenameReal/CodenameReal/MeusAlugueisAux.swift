//
//  MeusAlugueisAux.swift
//  CodenameReal
//
//  Created by jesse filho on 1/31/16.
//  Copyright © 2016 jefbyte. All rights reserved.
//

import Foundation

public class MeusAlugueisAux: NSObject {

    var valorAluguel: Double = 0.0
    var dataVencimento: NSDate?
    var agenciaBankDono: String = ""
    var contaBankDono: String = ""
    var fotoLocacao: NSData?
    var nomeDono: String = ""
    var tituloLocacao: String = ""
    
    static let novoAluguel = MeusAlugueisAux()
    
    func resetarInstancia() {
        valorAluguel = 0.0
        agenciaBankDono = ""
        contaBankDono = ""
        nomeDono = ""
        tituloLocacao = ""
    }
    
    
}
