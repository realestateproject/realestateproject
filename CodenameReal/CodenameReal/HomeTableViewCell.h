//
//  HomeTableViewCell.h
//  CodenameReal
//
//  Created by Matheus Coelho Berger on 10/15/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *tituloImovel;
@property (weak, nonatomic) IBOutlet UILabel *subtituloImovel;

@property (weak, nonatomic) IBOutlet UIImageView *imagemImovel;

@property (weak, nonatomic) IBOutlet UILabel *precoImovel;
@property (weak, nonatomic) IBOutlet UILabel *dataVencimento;

@property (weak, nonatomic) IBOutlet UILabel *statusPagamentos;

@end
