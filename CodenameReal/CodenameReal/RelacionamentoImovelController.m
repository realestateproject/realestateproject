//
//  RelacionamentoImovelController.m
//  CodenameReal
//
//  Created by Matheus Coelho Berger on 11/19/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

#import "RelacionamentoImovelController.h"

@interface RelacionamentoImovelController ()

@property (weak, nonatomic) IBOutlet UIView *viewMarcarVisita;
@property (weak, nonatomic) IBOutlet UIView *containerTableView;
@property (weak, nonatomic) IBOutlet UIStackView *stackBase;

@end

@implementation RelacionamentoImovelController

-(void)viewDidLoad
{
    [super viewDidLoad];
}

- (IBAction)marcarVisita:(id)sender
{
    [self.viewMarcarVisita setHidden:YES];
    
    [self.stackBase setPreservesSuperviewLayoutMargins:YES];
    [self.containerTableView setFrame:CGRectMake(0, 0, 375, 547)];
    [self.stackBase setFrame:CGRectMake(0, 0, 375, 667)];
}

@end
