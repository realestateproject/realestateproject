//
//  UIImageToCKAssetManager.swift
//  CodenameReal
//
//  Created by Thiago Borges Jordani on 12/1/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

import Foundation
import CloudKit

class UIImageToCKAssetManager{
    
    static let documentsDirectoryPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as NSString
    static var imageURL: NSURL!
    
    
    
    class func imageToCKAsset(image: UIImage, tempImageName :String)->CKAsset{
        let imageData: NSData = UIImageJPEGRepresentation(image, 0.8)!
        let path = documentsDirectoryPath.stringByAppendingPathComponent(tempImageName)
        imageURL = NSURL(fileURLWithPath: path)
        imageData.writeToURL(imageURL, atomically: true)
        return CKAsset(fileURL: imageURL)
    }
}