//
//  Pessoa+CoreDataProperties.swift
//  CodenameReal
//
//  Created by Thiago Borges Jordani on 11/30/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Pessoa {

    @NSManaged var cpf: String?
    @NSManaged var estadoCivil: String?
    @NSManaged var father: String?
    @NSManaged var gender: String?
    @NSManaged var idPessoa: NSNumber?
    @NSManaged var mother: String?
    @NSManaged var name: String?
    @NSManaged var picture: NSData?
    @NSManaged var profissao: String?
    @NSManaged var rg: String?

}
