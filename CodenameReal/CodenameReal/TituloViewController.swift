//
//  TituloViewController.swift
//  CodenameReal
//
//  Created by jesse filho on 11/17/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

import UIKit

class TituloViewController: UIViewController, UITextFieldDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.textField.delegate = self
        // Do any additional setup after loading the view.
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.prepareTextField()
    }
    
    func prepareTextField(){
        if LocacaoAux.novaLocacao.locTitle.characters.count > 0 {
            textField.text = LocacaoAux.novaLocacao.locTitle
            textField.becomeFirstResponder()
        }else{
            textField.text = "Digite sua descrição aqui"
            textField.textColor = UIColor.lightGrayColor()
            textField.becomeFirstResponder()
            textField.selectedTextRange = textField.textRangeFromPosition(textField.endOfDocument, toPosition: textField.beginningOfDocument)
        }
    }

    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        // Combine the textView text and the replacement text to
        // create the updated text string
        let currentText:NSString = textField.text!
        let updatedText = currentText.stringByReplacingCharactersInRange(range, withString:string)
        
        // If updated text view will be empty, add the placeholder
        // and set the cursor to the beginning of the text view
        if updatedText.isEmpty {
            
            textField.text = "Placeholder"
            textField.textColor = UIColor.lightGrayColor()
            
            textField.selectedTextRange = textField.textRangeFromPosition(textField.beginningOfDocument, toPosition: textField.beginningOfDocument)
            
            return false
        }
            
            // Else if the text view's placeholder is showing and the
            // length of the replacement string is greater than 0, clear
            // the text view and set its color to black to prepare for
            // the user's entry
        else if textField.textColor == UIColor.lightGrayColor() && !string.isEmpty {
            textField.text = nil
            textField.textColor = UIColor.blackColor()
        }
        
        return true
    }
    

    @IBOutlet weak var textField: UITextField!
    //MARK: IBActions
    @IBAction func tituloResidencia(textField: UITextField) {
        print(textField.text);
    }

    @IBAction func tituloSalvo(sender: AnyObject) {
        let alert = UIAlertController(title: "Salvo", message: "Legal, seus dados estão salvos e seguros. Você pode escolher edita-los quando desejar.", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.Cancel, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { action in
            switch action.style{
            case .Default:
                print("default")
                if let tempTitle = self.textField.text {
                    LocacaoAux.novaLocacao.locTitle = tempTitle
                }
                self.navigationController?.popViewControllerAnimated(true)
                
            case .Cancel:
                print("cancel")
                
            case .Destructive:
                print("destructive")
            }
        }))

    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
   
}
