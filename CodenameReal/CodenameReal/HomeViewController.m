//
//  HomeViewController.m
//  CodenameReal
//
//  Created by Matheus Coelho Berger on 10/23/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

#import "HomeViewController.h"
#import "ImovelHomeViewController.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface HomeViewController ()

@property (strong, nonatomic) NSMutableArray *imoveis;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goToCheckout:) name:@"goToCheckout" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goToRelation:) name:@"goToRelation" object:nil];
    
    ImovelHomeViewController *imovel = [self viewControllerAtIndex:0];
    NSLog(@"o view controller é: %@", imovel);
    
    self.imoveis = [[NSMutableArray alloc] initWithObjects:imovel, nil];
    
    NSLog(@"o nome do imovel é :%@", [self.imoveis lastObject]);
    
    self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    
    self.pageController.dataSource = self;
    self.pageController.delegate = self;
    
    [self.pageController.view setFrame:self.view.frame];
    
    [self.pageController setViewControllers:self.imoveis direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    [self addChildViewController:self.pageController];
    [self.view addSubview:self.pageController.view];
    [self.pageController didMoveToParentViewController:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.tabBarController.tabBar.hidden = NO;
}

#pragma PageView

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = [(ImovelHomeViewController *)viewController index];
    
    if (index == 0)
    {
        return nil;
    }
    
    index--;
    
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = [(ImovelHomeViewController *)viewController index];
    
    if (index == [self.imoveis count] -1)
    {
        return nil;
    }
    
    index++;
    
    return [self viewControllerAtIndex:index];
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return [self.imoveis count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;
}

- (ImovelHomeViewController *) viewControllerAtIndex:(NSUInteger)index
{
    ImovelHomeViewController *imovel = [[ImovelHomeViewController alloc] init];
    
    imovel = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeContentView"];
    
    imovel.stringMoeda = @"BRL";
    
    imovel.titulo = @"Milone's Apartament";
    imovel.stringNomeLocador = @"Milone da Silva";
    
    imovel.stringValor = @"700,00";
    imovel.stringDataVencimento = @"October 30th";
    
    imovel.stringStatusPagamento = @"Everything ok";
    imovel.textoBotaoPagar = @"Pay rent";
    
    imovel.imagemNome = @"test";
    
    imovel.index = index;
    
    return imovel;
}

#pragma Observations

- (void) goToCheckout: (NSNotification *)notification
{
    //SEL goToCheckoutSelector = @selector(goToCheckout:);
    
    [self performSegueWithIdentifier:@"goToCheckout" sender:self];
}

- (void) goToRelation: (NSNotification *)notification
{
    [self performSegueWithIdentifier:@"goToRelation" sender:self];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
