//
//  ImovelHomeLocadorViewController.h
//  CodenameReal
//
//  Created by jesse filho on 11/24/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImovelHomeLocadorViewController : UIViewController

@property (assign, nonatomic) NSUInteger index;

//@property (assign, nonatomic) NSString *titulo;
//@property (assign, nonatomic) NSString *stringNomeLocador;
//
@property (strong, nonatomic) NSString *stringMoeda;
//@property (weak, nonatomic) NSString *stringValor;
//@property (weak, nonatomic) NSString *stringDataVencimento;
//
//@property (weak, nonatomic) NSString *imagemNome;

@property (weak, nonatomic) NSString *stringStatusPagamento;


@property (weak, nonatomic) NSString *pinString;
@property (readwrite, nonatomic) IBOutlet UIButton *pint;

@end
