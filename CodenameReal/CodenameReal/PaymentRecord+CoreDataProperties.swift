//
//  PaymentRecord+CoreDataProperties.swift
//  CodenameReal
//
//  Created by Thiago Borges Jordani on 11/30/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension PaymentRecord {

    @NSManaged var date: NSDate?
    @NSManaged var value: NSNumber?
    @NSManaged var contract: NSManagedObject?

}
