//
//  TabController.m
//  CodenameReal
//
//  Created by jesse filho on 10/28/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

#import "ResidenciaTabController.h"
#import "UIColor+Locador.h"
@interface ResidenciaTabController ()



@end


//UIColor *locador = [UIColor LocadorColor];
NSString *imageBackGroundNav = @"NavBar_Residencial_topo_750x128";
NSString *imageBackGroundTabBar = @"NavBar_Residencial_base_750x120";
// itens in tab bar
NSString *icon_Imovel_Default = @"Icon_1_Imovel_default_58x58";
NSString *icon_Imovel_Selected =@"Icon_1_Imovel_selected_58x58";
NSString *icon_Localidade_Default = @"Icon_2_localidade_default_58x58";
NSString *icon_Localidade_Selected =@"Icon_2_localidade_selected_58x58";
NSString *icon_comodidade_Default = @"Icon_3_comodidades_default_58x58";
NSString *icon_comodidade_Selected =@"Icon_3_comodidades_selected_58x58";
NSString *icon_informacoes_Default = @"Icon_4_informacoes_defaut_58x58";
NSString *icon_informacoes_Selected =@"Icon_4_informacoes_selected_58x58";

@implementation ResidenciaTabController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.delegate = self;
    [self appearanceTabBar];
}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self navigationConfigViewWillappear];
    [self appearanceTabBar];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self navigationConfigViewWillappear];
    [self appearanceTabBar];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:true];
    [self navigationConfigViewWillDisappear];
    [self appearanceTabBar];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    CGRect temp = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    self.view.frame = temp;
    
    [super viewWillLayoutSubviews];
    [self.tabBar invalidateIntrinsicContentSize];
    CGFloat tabSize = 60.0;
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (UIInterfaceOrientationIsLandscape(orientation))
    {
        tabSize = 44.0;
    }
    CGRect tabFrame = self.tabBar.frame;
    tabFrame.size.height = tabSize;
    tabFrame.origin.y = self.view.frame.origin.y
    ;
    //self.tabBar.bounds;
    self.tabBar.frame = tabFrame;
    
    
    // Set the translucent property to NO then back to YES to
    // force the UITabBar to reblur, otherwise part of the
    // new frame will be completely transparent if we rotate
    // from a landscape orientation to a portrait orientation.
    
    self.tabBar.translucent = NO;
    self.tabBar.translucent = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void) appearanceTabBar{
    
   
    [self.tabBar setShadowImage:nil];
    [self.tabBar setBackgroundImage:[UIImage imageNamed:imageBackGroundTabBar]];
    
    UITabBarItem *tabBarItem = [self.tabBar.items objectAtIndex:0];
    UIImage *unselectedImage = [UIImage imageNamed:icon_Imovel_Default ];
    UIImage *selectedImage = [UIImage imageNamed:icon_Imovel_Selected ];
    [tabBarItem setImage: [unselectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tabBarItem setSelectedImage: selectedImage];
    
    UITabBarItem *tabBarItem1 = [self.tabBar.items objectAtIndex:1];
    UIImage *unselectedImage1 = [UIImage imageNamed:icon_Localidade_Default];
    UIImage *selectedImage1 = [UIImage imageNamed:icon_Localidade_Selected];
    [tabBarItem1 setImage: [unselectedImage1 imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tabBarItem1 setSelectedImage: selectedImage1];
    
    UITabBarItem *tabBarItem2 = [self.tabBar.items objectAtIndex:2];
    UIImage *unselectedImage2 = [UIImage imageNamed:icon_comodidade_Default];
    UIImage *selectedImage2 = [UIImage imageNamed:icon_comodidade_Selected];
    [tabBarItem2 setImage: [unselectedImage2 imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tabBarItem2 setSelectedImage: selectedImage2];
    
    UITabBarItem *tabBarItem3 = [self.tabBar.items objectAtIndex:3];
    UIImage *unselectedImage3 = [UIImage imageNamed:icon_informacoes_Default];
    UIImage *selectedImage3 = [UIImage imageNamed:icon_informacoes_Selected];
    [tabBarItem3 setImage: [unselectedImage3 imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tabBarItem3 setSelectedImage: selectedImage3];
    
    
}



-(void) navigationConfigViewWillappear{

    UIImage *image = [UIImage imageNamed:imageBackGroundNav];
    [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];

    
    // set tint color of tab bar
    [self.tabBar setTintColor:[UIColor whiteColor]];
}
-(void) navigationConfigViewWillDisappear{
    
    [self.tabBar setBackgroundImage:nil];
    [self.tabBar setBackgroundColor:[UIColor locadorColor]];
    
    // set tint color of nav
    
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setBackgroundColor:[UIColor locadorColor]];
}




@end
