//
//  CadastroTesteViewController.swift
//  CodenameReal
//
//  Created by Thiago Borges Jordani on 10/21/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

import UIKit
import CoreData

class CadastroTesteViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @IBOutlet weak var nomeTF: UITextField!
    @IBOutlet weak var cpfTF: UITextField!
    @IBOutlet weak var rgTF: UITextField!
    @IBOutlet weak var nomeMaeTF: UITextField!
    @IBOutlet weak var nomePaiTF: UITextField!
    @IBOutlet weak var genderTf: UITextField!
    @IBOutlet weak var civilStateTF: UITextField!
    @IBOutlet weak var prof: UITextField!
    
    @IBOutlet weak var pictureIV: UIImageView!
    
    
    
    // MARK: UI Methods
    @IBAction func didClickChoosePicture(sender: AnyObject) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.SavedPhotosAlbum) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            imagePicker.allowsEditing = false
            
            presentViewController(imagePicker, animated: true, completion: nil)
        }
    }
    @IBAction func didClickSaveButton() {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let context = appDelegate.getManagedObjectContext()
        let entity =  NSEntityDescription.entityForName("Pessoa", inManagedObjectContext:context)
        let person = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: context)
        
        person.setValue(nomeTF.text, forKey: "name")
        person.setValue(cpfTF.text, forKey: "cpf")
        person.setValue(rgTF.text, forKey: "rg")
        person.setValue(nomeMaeTF.text, forKey: "mother")
        person.setValue(nomePaiTF.text, forKey: "father")
        person.setValue(civilStateTF.text, forKey: "estadoCivil")
        person.setValue(prof.text, forKey: "profissao")
        
        if (pictureIV.image != nil) {
            let imageData: NSData = UIImageJPEGRepresentation(pictureIV.image!, 0.8)!
            person.setValue(imageData, forKey: "picture")
        }
        do {
            try context.save()
            self .performSegueWithIdentifier("CDtestSegue", sender: self)
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    
    
    // MARK: imagePickerControllerDelegate Methods
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        pictureIV.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        pictureIV.contentMode = UIViewContentMode.ScaleToFill
//        saveImageLocally()
        
        pictureIV.hidden = false
        
        dismissViewControllerAnimated(true, completion: nil)
    }
}

