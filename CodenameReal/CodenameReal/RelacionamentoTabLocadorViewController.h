//
//  RelacionamentoTabLocadorViewController.h
//  CodenameReal
//
//  Created by Matheus Coelho Berger on 11/25/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RelacionamentoTabLocadorViewController : UITabBarController <UITabBarControllerDelegate>

@end
