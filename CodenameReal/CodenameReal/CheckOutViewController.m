//
//  CheckOutViewController.m
//  CodenameReal
//
//  Created by Matheus Coelho Berger on 10/14/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

#import "CheckOutViewController.h"

@interface CheckOutViewController ()

@property (strong, nonatomic, readwrite) PayPalConfiguration *payPalConfiguration;

@property (weak, nonatomic) IBOutlet UILabel *labelTituloImovel;
@property (weak, nonatomic) IBOutlet UILabel *labelNomeLocador;
@property (weak, nonatomic) IBOutlet UILabel *labelDataVencimento;
@property (weak, nonatomic) IBOutlet UILabel *labelMoeda;
@property (weak, nonatomic) IBOutlet UILabel *labelValorMensalidade;
@property (weak, nonatomic) IBOutlet UILabel *labelValorTotal;
@property (weak, nonatomic) IBOutlet UILabel *labelStatusPagamento;


@end

@implementation CheckOutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //Config do paypal; checar PayPalConfiguration.h para configurações extras
    
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    self.payPalConfiguration = [[PayPalConfiguration alloc] init];
    self.payPalConfiguration.acceptCreditCards = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //Preconectar com o ambiente PayPal desejado
    
    [PayPalMobile preconnectWithEnvironment:PayPalEnvironmentSandbox];
}

#pragma mark - PayPayl Payments

- (IBAction)paySingleTime:(id)sender
{
    //Passo 1: criar o pagamento
    PayPalPayment *payment = [[PayPalPayment alloc]init];
    
    //Passo 2: setar preço, moeda e descrição
    payment.amount = [[NSDecimalNumber alloc] initWithString:@"700"];
    payment.currencyCode = @"BRL";
    payment.shortDescription = @"Pagamento do aluguel";
    
    //Passo 3: especificar o modo do pagamento
    payment.intent = PayPalPaymentIntentSale;
    
    //Passo 4: olhar PayPalPayment.h para checar outras customizações de pagamentos que possam ser uteis
    
    //Passo 5: checar se o pagamento é processável
    if (!payment.processable)
    {
        //Checar aqui o que pode ter dado errado
    }
    
    //Passo 6: criar o PaypalPaymentViewController a partir do pagamento 'payment' das configurações 'self.payPalConfiguration'
    PayPalPaymentViewController *paymentViewController;
    paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:payment configuration:self.payPalConfiguration delegate:self];
    
    //Passo 7: mostrar o PayPalViewController
    [self presentViewController:paymentViewController animated:YES completion:nil];
}

#pragma mark - métodos do PaypalPaymentDelegate

- (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController didCompletePayment:(PayPalPayment *)completedPayment
{
    //Função para quando o pagamento foi processado com sucesso, mandado para o servidor para verificação e complemento
    [self verifyCompletedPayment:completedPayment];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController
{
    //O pagamento foi cancelado
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)verifyCompletedPayment:(PayPalPayment *)completedPayment
{
    //Enviar dicionario de confirmação completo para o servidor
    
//    NSData *confirmation = [NSJSONSerialization dataWithJSONObject:completedPayment.confirmation options:0 error:nil];
    
    //Mandar a confirmação pro servidor, o servidor deve verificar a prova do pagamento para evitar fraude
    //se não for possivel mandar para o servidor, salvar a prova do pagamento (NSData *confirmation) e tentar depois.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
