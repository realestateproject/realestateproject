//
//  UIColor+Locador.h
//  CodenameReal
//
//  Created by jesse filho on 11/15/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface UIColor (LocadorColor)

+(UIColor*)locadorColor;

+(UIColor*)locatarioColor;

@end