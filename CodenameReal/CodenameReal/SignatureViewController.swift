//
//  SignatureViewController.swift
//  CodenameReal
//
//  Created by Thiago Borges Jordani on 1/18/16.
//  Copyright © 2016 Matheus Coelho Berger. All rights reserved.
//


import UIKit
import CloudKit

class SignatureViewController: UIViewController {
    var signatureView = PJRSignatureView()
    
    let usersManager = UsersManager(database: CKContainer.defaultContainer().publicCloudDatabase)
    var user = Users()
    
    @IBOutlet weak var signatureViewPlaceholder: UIView!
    @IBOutlet weak var nextButton: UIButton!
    var signatureImgSet = false
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.signatureView = PJRSignatureView(frame: self.signatureViewPlaceholder.frame)
        self.view.addSubview(self.signatureView)
        
    }

    @IBAction func saveSignatureImage(sender: AnyObject) {
        Users.novoUser.assinatura = UIImageToCKAssetManager.imageToCKAsset(self.signatureView.getSignatureImage(), tempImageName:"assinatura.png")
        self.signatureImgSet = true
        self.signatureView.userInteractionEnabled = false
    }
    
    @IBAction func clearSignatureImage(sender: AnyObject) {
        self.signatureView.clearSignature()
//        Users.novoUser.assinatura = UIImageToCKAssetManager.imageToCKAsset(UIImage(), tempImageName:"assinatura.png")
        self.signatureImgSet = false
        self.signatureView.userInteractionEnabled = true
    }
    @IBAction func completarCadastro(sender: AnyObject) {
        if (!signatureImgSet){
            print("Sem foto null")
            self.alert("Você precisa de uma assinatura")
        }else{
            dispatch_async(dispatch_get_main_queue(),{
                self.indicatorLoadStart()
            })
            self.user.email = Users.novoUser.email
            
            self.usersManager.checkEmail(user, callback: { (success, users) -> () in
                if success {
                    
                    users?.assinatura = UIImageToCKAssetManager.imageToCKAsset(self.signatureView.getSignatureImage(), tempImageName:"assinatura.png")
                    self.usersManager.updateUsers(users!, callback: { (success) -> () in
                        if success{
                            dispatch_async(dispatch_get_main_queue(),{
                                self.indicatorLoadStop()
                                
                                CodenameRealStaticObjects.loggedUser = Users(record: Users.novoUser.record)
                                CoreDataHelper.saveLoggedUser();
                                self.confirmacao()
                            })
                        }else{
                            print("Erro em  createAccountUsers assinatura 01")
                        }
                    })
                    
                }else{
                    print("Erro em  avancar assinatura 02")
                }
            })
        }
    }
    
    func confirmacao(){
        
//        let alertView = UIAlertController(title: "Massa!", message: "Seus dados foram salvos e serão avaliados pela nossa equipe!", preferredStyle: .Alert)
//        alertView.addAction(UIAlertAction(title: "Continuar", style: .Default, handler: { (alertAction) -> Void in
//            print("OK")
//            
//            let storyboard = UIStoryboard(name: "Locatario", bundle: nil)
//            let vc = storyboard.instantiateViewControllerWithIdentifier("TabBarLocatario") as UIViewController
//            
//            dispatch_async(dispatch_get_main_queue(),{
//                self.presentViewController(vc, animated: true, completion: nil)
//                
//            })
//            
//            
//        }))
        
        let storyboard = UIStoryboard(name: "Locatario", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier("TabBarLocatario") as UIViewController
        
        dispatch_async(dispatch_get_main_queue(),{
            self.presentViewController(vc, animated: true, completion: nil)
            
        })
    }
    
    //MARK: Alert View
    func alert(msg : String){
        let alert = UIAlertController(title: "Ops!!!", message: msg, preferredStyle: UIAlertControllerStyle.Alert)
        
        
        alert.addAction(UIAlertAction(title: "Certo", style: .Default, handler: { action in
            switch action.style{
            case .Default:
                print("default")
                
                print(" colocar foco no textfield")
                
            case .Cancel:
                print("cancel")
                
            case .Destructive:
                print("destructive")
            }
        }))
        
        dispatch_async(dispatch_get_main_queue(),{
            self.indicatorLoadStop()
            self.presentViewController(alert, animated: true, completion: nil)
            
        })
    }
    
    //MARK: Activity Indicator
    
    var activityIndicator = UIActivityIndicatorView()
    
    func indicatorLoadStart (){
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.White)
        activityIndicator.backgroundColor = UIColor(white: 0.1, alpha: 0.7)
        activityIndicator.frame = self.view.frame
        activityIndicator.center = self.view.center
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        self.view.bringSubviewToFront(activityIndicator)
    }
    
    func indicatorLoadStop(){
        activityIndicator.stopAnimating()
        activityIndicator.removeFromSuperview()
    }
}
