//
//  TabBarLogoutController.h
//  CodenameReal
//
//  Created by jesse filho on 11/23/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TabBarLogoutController : UITabBarController <UITabBarControllerDelegate>

@end
