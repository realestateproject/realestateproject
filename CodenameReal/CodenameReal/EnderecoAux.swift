//
//  EnderecoAux.swift
//  CodenameReal
//
//  Created by Thiago Borges Jordani on 11/18/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

import Foundation


class EnderecoAux : NSObject {
    
    var cep = ""
    var complemento = ""
    var idEndereco = ""
    var numero = ""
    var rua = ""
//     var place: Locacao?
    
}