//
//  ImovelHomeViewController.m
//  CodenameReal
//
//  Created by Matheus Coelho Berger on 10/23/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

#import "ImovelHomeViewController.h"

@interface ImovelHomeViewController ()

@property (weak, nonatomic) IBOutlet UILabel *tituloImovel;
@property (weak, nonatomic) IBOutlet UILabel *nomeLocador;

@property (weak, nonatomic) IBOutlet UILabel *moedaMensalidade;
@property (weak, nonatomic) IBOutlet UILabel *valorMensalidade;
@property (weak, nonatomic) IBOutlet UILabel *dataVencimento;

@property (weak, nonatomic) IBOutlet UIImageView *imagemImovel;

@property (weak, nonatomic) IBOutlet UILabel *statusPagamento;
@property (weak, nonatomic) IBOutlet UIButton *botaoPagar;

@property (weak, nonatomic) IBOutlet UIImageView *imagemFundo;

@end

@implementation ImovelHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSLog(@"%@", self.stringMoeda);
    
    self.moedaMensalidade.text = self.stringMoeda;
    
    self.tituloImovel.text = self.titulo;
    self.nomeLocador.text = self.stringNomeLocador;
    
    self.valorMensalidade.text = self.stringValor;
    self.dataVencimento.text = self.stringDataVencimento;
    
    self.statusPagamento.text = self.stringStatusPagamento;
    [self.botaoPagar setTitle:self.textoBotaoPagar forState:UIControlStateNormal];
    
    NSLog(@"%@", self.textoBotaoPagar);
    NSLog(@"%@", self.botaoPagar.titleLabel.text);
    
    //self.imagemFundo.image = [UIImage imageNamed:self.imagemNome];
    self.imagemImovel.image = [UIImage imageNamed:self.imagemNome];
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)goToCheckout:(id)sender
{
    NSLog(@"postando notificação de checkout/pin");
    
    if ([self.botaoPagar.titleLabel.text isEqualToString:@"Digitar PIN de localização"])
    {
        NSLog(@"entrou na notificação do pin");
        [[NSNotificationCenter defaultCenter] postNotificationName:@"goToPin" object:nil];
    }
    else
    {
         [[NSNotificationCenter defaultCenter] postNotificationName:@"goToCheckout" object:nil];
    }
}

- (IBAction)goToRelation:(id)sender
{
    if ([self.botaoPagar.titleLabel.text isEqualToString:@"Digitar PIN de localização"])
    {
        //criar algo pra colocar aqui
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"goToRelation" object:nil];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
