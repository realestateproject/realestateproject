//
//  ConectarViewController.swift
//  CodenameReal
//
//  Created by jesse filho on 11/23/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

import CloudKit


class ConectarViewController: UIViewController, UITextFieldDelegate {
    
    let usersManager = UsersManager(database: CKContainer.defaultContainer().publicCloudDatabase)
    var user = Users()
    @IBAction func FecharTela(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func voltar(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var senha: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.email.delegate = self
        self.senha.delegate = self
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Keyboards
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    func verificarSenha(passwd:String) ->Bool{
        
        if(senhaCallBack == self.senha.text){
            return true
        }else{
            self.senha.text = ""
            return false
        }
 
    }
    
    var senhaCallBack : String = ""
    
    
    @IBAction func verificaEmail(sender: AnyObject) {
        // verificar se este email já está sendo usado
        print(email.text)
        
        if (!self.validateEmail(email.text!)){
            let msg = "Parece que seu email é inválido."
            self.alert(msg)
        }
        else {
            user.email = email.text!
        }
        
    }
    
    @IBAction func conectarConta(sender: AnyObject) {
       
        // quando clicar em conectar ele busca o user e consulta se a senha é igual a que veio
        
        dispatch_async(dispatch_get_main_queue(),{
            self.dismissKeyboard()
            self.indicatorLoadStart()
        })
        
        self.usersManager.checkEmail(user, callback: { (success, users) -> () in
            if success {
                print("email cadastrado")
                print(users!.senha)
                
                self.senhaCallBack = users!.senha
                
                let boolpasswd = self.verificarSenha(self.senha.text!)
                
                if(boolpasswd){
                    print("OK")
                    CodenameRealStaticObjects.loggedUser = Users(record: (users?.record)!)
                    CoreDataHelper.saveLoggedUser()
                    let storyboard = UIStoryboard(name: "Locador", bundle: nil)
                    let vc = storyboard.instantiateViewControllerWithIdentifier("TabBarControllerAddImoveis") as UIViewController
                    dispatch_async(dispatch_get_main_queue(),{
                        self.presentViewController(vc, animated: true, completion: nil)
                    })
                    
                }else{
                    let msg = "Senha incorreta!"
                    self.alert(msg)
                }
                
                
                
                dispatch_async(dispatch_get_main_queue(),{
                    self.indicatorLoadStop()
                })
                
                
                
            }else{
                //mandar criar conta ou avisar que conta não existe
                
                let msg = "Houve falha na conexão ou esse email não está cadastrado, verifique e tente novamente."
                self.alertCriarConta(msg)
                
            }
        })
        
        
        
        
        
        
        
        
    }
    
    // MARK: - validation account CloudKit
    
    func validateEmail(candidate: String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluateWithObject(candidate)
    }

    
    // MARK: - verify account CloudKit
    
    func consultaEmail() {
        // verificar se este email já está sendo usado
        print(email.text)
        
        if (!self.validateEmail(email.text!)){
            let msg = "Parece que seu email é inválido."
            self.alert(msg)
        }
        else {
            user.email = email.text!
            
            
            dispatch_async(dispatch_get_main_queue(),{
                self.indicatorLoadStart()
            })
            
            self.usersManager.checkEmail(user, callback: { (success, users) -> () in
                if success {
                    print("email cadastrado")
                    
                    
                    
                }else{
                    //colocar cursor focado na senha
                    
                    let msg = "Parace que este email não está cadastrado, deseja cadastra-lo?"
                    self.alertCriarConta(msg)
                    
                    self.user.email = self.email.text!
                    self.user.senha = self.senha.text!
                    
                    Users.novoUser.email = self.email.text!
                    
                    self.usersManager.createAccountUsers(self.user, callback: { (success, users) -> () in
                        if success {
                            print("Conta Criada")
                            
                            dispatch_async(dispatch_get_main_queue(),{
                                self.indicatorLoadStop()
                                self.performSegueWithIdentifier("telaCadastroConta", sender: nil)
                            })
                            
                        }else {
                            print("Erro em  createAccountUsers")
                        }
                    })
                }
            })
            
        }
        
    }
    
    // MARK: - activity Indicator
    
    var activityIndicator = UIActivityIndicatorView()
    
    func indicatorLoadStart (){
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.White)
        activityIndicator.backgroundColor = UIColor(white: 0.1, alpha: 0.7)
        activityIndicator.frame = self.view.frame
        activityIndicator.center = self.view.center
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        self.view.bringSubviewToFront(activityIndicator)
    }
    
    func indicatorLoadStop(){
        activityIndicator.stopAnimating()
        activityIndicator.removeFromSuperview()
    }
    
    // MARK: - Alerts
    func alert(msg : String){
        let alert = UIAlertController(title: "Ops!!!", message: msg, preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addAction(UIAlertAction(title: "Certo", style: .Default, handler: { action in
            switch action.style{
            case .Default:
                print("default")
                
                print(" colocar foco no textfield")
                self.senha.text = ""
               
            case .Cancel:
                print("cancel")
                
            case .Destructive:
                print("destructive")
            }
        }))
        
        dispatch_async(dispatch_get_main_queue(),{
            self.indicatorLoadStop()
            self.presentViewController(alert, animated: true, completion: nil)
            
        })
    }
    
    func alertCriarConta(msg : String){
        let alert = UIAlertController(title: "Ops!!!", message: msg, preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addAction(UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.Cancel, handler: nil))
        
        alert.addAction(UIAlertAction(title: "Cadastrar", style: .Default, handler: { action in
            switch action.style{
            case .Default:
                print("default")
                print("Ir para a tela de criar conta")
                self.dismissKeyboard()
                self.performSegueWithIdentifier("criarContaNova", sender: nil)
                
            case .Cancel:
                print("cancel")
                self.dismissKeyboard()
                
            case .Destructive:
                print("destructive")
            }
        }))
        
        dispatch_async(dispatch_get_main_queue(),{
            self.indicatorLoadStop()
            self.presentViewController(alert, animated: true, completion: nil)
            
        })
    }
    
    // MARK: - Segue com Dados
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if (segue.identifier == "criarContaNova"){
            
            if ( segue.destinationViewController.respondsToSelector(Selector("insereEmail:"))){
                segue.destinationViewController.performSelector(Selector("insereEmail:"), withObject: email.text)
            }

        }
        
    }

}
