//
//  Entry.swift
//  CodenameReal
//
//  Created by jesse filho on 11/27/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//


import Foundation
import CloudKit

public class Users : NSObject{

    static let novoUser = Users()
    func resetarInstancia() {
    email = ""
    }
    
    let record: CKRecord
    
    override init(){
        record = CKRecord(recordType: "User")
        super.init()
        email = ""
        
    }
//    
    init(record: CKRecord) {
        self.record = record
    }
//
    init(users:Users){
        record = CKRecord(recordType: "User")
        super.init()
        nome = users.nome
        sobrenome = users.sobrenome
        dataNascimento = users.dataNascimento
        email = users.email
        senha = users.senha
        docFrente = users.docFrente
        docVerso = users.docVerso
        fotoPerfil = users.fotoPerfil
        assinatura = users.assinatura
        endereco = users.endereco
        
        tipoConexao = users.tipoConexao
    
        // perfil
        
        
    }
    
    
    var id: String? {
        return record.recordID.recordName
    }

    var nome: String{
        get{
            return(self.record.objectForKey("Nome") as! String)
        }
        set (val){
            record.setObject(val, forKey: "Nome")
        }
        
    }
    
    var sobrenome: String{
        get{
            if (self.record.objectForKey("Sobrenome") != nil){
                print("nao nulo")
                return (self.record.objectForKey("Sobrenome") as! String)
                
            }else{
                return ""
            }
            
        }
        set (val){
            record.setObject(val, forKey: "Sobrenome")
        }
        
    }
    var endereco: String{
        get{
            if (self.record.objectForKey("Endereco") != nil){
                print("nao nulo")
                return (self.record.objectForKey("Endereco") as! String)
                
            }else{
                return ""
            }
        }
        set (val){
            record.setObject(val, forKey: "Endereco")
        }
        
    }
    
    var email: String{
        get{
            return(self.record.objectForKey("Email") as! String)
        }
        set (val){
            record.setObject(val, forKey: "Email")
        }
        
    }
    
    var dataNascimento: String{
        get{
            return(self.record.objectForKey("DataNascimento") as! String)
        }
        set (val){
            record.setObject(val, forKey: "DataNascimento")
        }
        
    }
    
    var senha: String{
        get{
            return(self.record.objectForKey("Senha") as! String)
        }
        set (val){
            record.setObject(val, forKey: "Senha")
        }
        
    }
    
    
    var assinatura: CKAsset{
        get{
            return(self.record.objectForKey("Assinatura") as! CKAsset)
        }
        set (val){
            record.setObject(val, forKey: "Assinatura")
        }
        
    }
    
    
    
    var docFrente: CKAsset{
        get{
            return(self.record.objectForKey("DocFrente") as! CKAsset)
        }
        set (val){
            record.setObject(val, forKey: "DocFrente")
        }
        
    }
    
    var docVerso: CKAsset{
        get{
            return(self.record.objectForKey("DocVerso") as! CKAsset)
        }
        set (val){
            record.setObject(val, forKey: "DocVerso")
        }
        
    }
    
    var fotoPerfil: UIImage{
        get{
            if let data = NSData(contentsOfURL: (self.record.objectForKey("FotoPerfil") as! CKAsset!).fileURL) {
                return(UIImage(data: data)!)
            } else {
                return UIImage()
            }
            
        }
        set (val){
            record.setObject(UIImageToCKAssetManager.imageToCKAsset(val, tempImageName: "FotoPerfil"), forKey: "FotoPerfil")
        }
        
    }
    
    var createdAt: NSDate {
        return record.creationDate!
    }
    
    var lastModifiedAt: NSDate {
        return record.modificationDate!
    }
    
    var agencia: String{
        get{
            
            if (self.record.objectForKey("Agencia") != nil){
                print("nao nulo")
                return (self.record.objectForKey("Agencia") as! String)

            }else{
                return ""
            }
            
        }
        set (val){
            record.setObject(val, forKey: "Agencia")
        }
        
    }
    
    var conta: String{
        get{
            
            if (self.record.objectForKey("Conta") != nil){
                print("nao nulo")
                return(self.record.objectForKey("Conta") as! String)
            }else{
                print("item conta é null")
                return ""
            }
            
        }
        set (val){
            record.setObject(val, forKey: "Conta")
        }
        
    }
    
    var cpf: String{
        get{
            
            if (self.record.objectForKey("CPF") != nil){
                print("nao nulo")
                return(self.record.objectForKey("CPF") as! String)
            }else{
                print("item conta é null")
                return ""
            }
            
        }
        set (val){
            record.setObject(val, forKey: "CPF")
        }
        
    }
    
    var tipoConexao: String{
        get{
            
            if (self.record.objectForKey("TipoConexao") != nil){
                print("nao nulo")
                return(self.record.objectForKey("TipoConexao") as! String)
            }else{
                print("item TipoConexao é null")
                return ""
            }
            
        }
        set (val){
            record.setObject(val, forKey: "TipoConexao")
        }
        
    }

}


