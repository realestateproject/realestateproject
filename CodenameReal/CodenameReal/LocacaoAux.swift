//
//  LocacaoAux.swift
//  CodenameReal
//
//  Created by Thiago Borges Jordani on 11/18/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

import Foundation

public class LocacaoAux : NSObject {
    var bedrooms: Int = 0
    var contratoIptu: Int = 0
    var garageSpaces: Int = 0
    var idLocacao: Int = 0
    var size: Int = 0
    var toilets: Int = 0
    var valorAluguel: Int = 0
    var valorCondominio: Int = 0
    var tipoLocacao: Int = 0
    var salas: Int = 0
    var locTitle: String = ""
    var resumo: String = ""
    var tipoResidencia: String = ""
    var local = EnderecoAux()
    var owner: PessoaAux?
    var pictures: FotosLocacaoAux?
    var comodidades = ComodidadesAux()
    var pagamento = PagamentoAux()
    var foto: UIImage?
    
    var pin: String = ""
    
    
    static let novaLocacao = LocacaoAux()
    
    func resetarInstancia() {
        
        bedrooms = 0
        contratoIptu = 0
        garageSpaces = 0
        idLocacao = 0
        size = 0
        toilets = 0
        valorAluguel = 0
        valorCondominio = 0
        tipoLocacao = 0
        salas = 0
        locTitle = ""
        resumo = ""
        tipoResidencia = ""
        
        local = EnderecoAux()
        pagamento = PagamentoAux()
        comodidades = ComodidadesAux()
    }
}