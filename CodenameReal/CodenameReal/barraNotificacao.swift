//
//  barraNotificacao.swift
//  CodenameReal
//
//  Created by jesse filho on 11/18/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

import UIKit

protocol ButtonClickedDelegate {
    func publicar()
}


class barraNotificacao: UIView {

    
    var delegate : ButtonClickedDelegate!
    
//    class func instanceFromNib() -> UIView {
//        return UINib(nibName: "nib file name", bundle: nil).instantiateWithOwner(nil, options: nil)[0] as UIView
//    }
    
    @IBAction func publicar(sender: UIButton) {
        print("publicado")
        
        delegate.publicar()
        
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    convenience init()
    {
        self.init()
    }
}
    
    extension UIView {
        class func loadFromNibNamedNotificacao(nibNamed: String, bundle : NSBundle? = nil) -> UIView? {
            return UINib(
                nibName: nibNamed,
                bundle: bundle
                ).instantiateWithOwner(nil, options: nil)[0] as? UIView
        }
    }
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */


