//
//  UIColor+Locador.m
//  CodenameReal
//
//  Created by jesse filho on 11/15/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

#import "UIColor+Locador.h"


@implementation UIColor (LocadorColor)

+(UIColor*)locadorColor {
    return [[UIColor alloc]initWithRed:56.0 green:166.0 blue:181 alpha:1];
}


+(UIColor*)locatarioColor
{
    return [[UIColor alloc] initWithRed:173  green:0 blue:65 alpha:1];
}

@end
