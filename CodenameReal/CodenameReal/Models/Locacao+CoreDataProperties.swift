//
//  Locacao+CoreDataProperties.swift
//  CodenameReal
//
//  Created by Thiago Borges Jordani on 11/30/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Locacao {

    @NSManaged var bedrooms: NSNumber?
    @NSManaged var contratoIptu: NSNumber?
    @NSManaged var garageSpaces: NSNumber?
    @NSManaged var idLocacao: NSNumber?
    @NSManaged var locTitle: String?
    @NSManaged var resumo: String?
    @NSManaged var salas: NSNumber?
    @NSManaged var size: NSNumber?
    @NSManaged var tipoLocacao: NSNumber?
    @NSManaged var toilets: NSNumber?
    @NSManaged var valorAluguel: NSNumber?
    @NSManaged var valorCondominio: NSNumber?
    @NSManaged var comodidades: ComodidadesAux?
    @NSManaged var local: Endereco?
    @NSManaged var owner: NSManagedObject?
    @NSManaged var pictures: FotosLocacao?

}
