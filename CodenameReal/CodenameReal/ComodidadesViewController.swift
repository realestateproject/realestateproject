//
//  ComodidadesViewController.swift
//  CodenameReal
//
//  Created by jesse filho on 11/5/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

import UIKit

class ComodidadesViewController: UITableViewController, UITextFieldDelegate {
    
    //MARK: Vars
    var qntQuartos = 0, qntBanheiros = 0, qntSalas = 0, qntVagas = 0
    
    
    //MARK: IBActions
    @IBAction func metroQuadrados(textField: UITextField) {
        print(textField.text);
        if(textField.text == ""){
            LocacaoAux.novaLocacao.size = 0
        }else{
            LocacaoAux.novaLocacao.size = Int(textField.text!)!
        }
        
    }
    @IBAction func next(sender: AnyObject) {
        self.tabBarController?.selectedIndex = 3
    }
    
    //MARK: IBOutlet
    @IBOutlet weak var textField: UITextField!
    @IBOutlet var comodidadesCheckListCells: [UITableViewCell]!
    @IBOutlet weak var quartosSegmentedControl: UISegmentedControl!
    @IBOutlet weak var banheirosSegmentedControl: UISegmentedControl!
    @IBOutlet weak var salasSegmentedControl: UISegmentedControl!
    @IBOutlet weak var vagasSegmentedControl: UISegmentedControl!
    
    //MARK: Lifecycle Methods
    //Calls this function when the tap is recognized.
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.loadValuesToView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Keyboard
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        print("tapped")
        view.endEditing(true)
    }
    override func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        view.endEditing(true)
    }
    // MARK: Segments Control
    @IBAction func qntQuartos(sender: UISegmentedControl) {
        view.endEditing(true) // close keyboard
        
            if( sender.selectedSegmentIndex == 0 && qntQuartos >= 0){
                
                if qntQuartos <= 0{
                    qntQuartos = 0
                }else{
                    qntQuartos = qntQuartos - 1
                }
            }
        if(sender.selectedSegmentIndex == 1){
             print("Lembrete:  Fazer aparecer teclado ao clica o botão do meio do segmente! Grato")
        }
            if( sender.selectedSegmentIndex == 2 ){
                
                qntQuartos = qntQuartos + 1
            }
        LocacaoAux.novaLocacao.bedrooms = qntQuartos
        sender.setTitle(String(qntQuartos), forSegmentAtIndex: 1)
   }
    
    @IBAction func qntBanheiros(sender: UISegmentedControl) {
        
        view.endEditing(true) // close keyboard
       
        
        if( sender.selectedSegmentIndex == 0 && qntBanheiros >= 0){
            if qntBanheiros == 0{
                qntBanheiros = 0
            }else{
                qntBanheiros = qntBanheiros - 1
            }
        }
        if( sender.selectedSegmentIndex == 2 ){
            qntBanheiros = qntBanheiros + 1
        }
        LocacaoAux.novaLocacao.toilets = qntBanheiros
        sender.setTitle(String(qntBanheiros), forSegmentAtIndex: 1)
    }
    
    @IBAction func qntSalas(sender: UISegmentedControl) {
        view.endEditing(true) // close keyboard
        if( sender.selectedSegmentIndex == 0 && qntSalas >= 0){
            if qntSalas == 0{
                qntSalas = 0
            }else{
                qntSalas = qntSalas - 1
            }
        }
        if( sender.selectedSegmentIndex == 2 ){
            qntSalas = qntSalas + 1
        }
        LocacaoAux.novaLocacao.salas = qntSalas
        sender.setTitle(String(qntSalas), forSegmentAtIndex: 1)
    }
    
    @IBAction func qntVagas(sender: UISegmentedControl) {
        
        if( sender.selectedSegmentIndex == 0 && qntVagas >= 0){
            if qntVagas == 0{
                qntVagas = 0
            }else{
                qntVagas = qntVagas - 1
            }
        }
        if( sender.selectedSegmentIndex == 2 ){
            qntVagas = qntVagas + 1
        }
        LocacaoAux.novaLocacao.garageSpaces = qntVagas
        sender.setTitle(String(qntVagas), forSegmentAtIndex: 1)
    }
    
    //MARK: Table View Delegate
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        //let cell = tableView.dequeueReusableCellWithIdentifier("ListPrototypeCell")
               print("You selected cell #\(indexPath.row)!")
        
        let cell: UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!
        switch indexPath.row{
        case 0:
            view.endEditing(true) // close keyboard
        case 1:
            view.endEditing(true) // close keyboard
        case 2:
            view.endEditing(true) // close keyboard
        case 3:
            view.endEditing(true) // close keyboard
        case 4:
            view.endEditing(true) // close keyboard
        case 6:
            print("Espaço Mobiliado")
            
            if (cell.accessoryType == UITableViewCellAccessoryType.Checkmark){
                cell.accessoryType = UITableViewCellAccessoryType.None
                LocacaoAux.novaLocacao.comodidades.mobiliado = false
            }else{
                cell.accessoryType = UITableViewCellAccessoryType.Checkmark
                LocacaoAux.novaLocacao.comodidades.mobiliado = true
            }
            
        case 7:
            print("Aquecimento Central")
            if (cell.accessoryType == UITableViewCellAccessoryType.Checkmark){
                cell.accessoryType = UITableViewCellAccessoryType.None
                LocacaoAux.novaLocacao.comodidades.aquecimento = false
            }else{
                cell.accessoryType = UITableViewCellAccessoryType.Checkmark
                LocacaoAux.novaLocacao.comodidades.aquecimento = true
            }
        case 8:
            print("Acessível para Caderantes")
            if (cell.accessoryType == UITableViewCellAccessoryType.Checkmark){
                cell.accessoryType = UITableViewCellAccessoryType.None
                LocacaoAux.novaLocacao.comodidades.acessibilidade = false
            }else{
                cell.accessoryType = UITableViewCellAccessoryType.Checkmark
                LocacaoAux.novaLocacao.comodidades.acessibilidade = true
            }
        case 9:
            print("Elevador no prédio")
            if (cell.accessoryType == UITableViewCellAccessoryType.Checkmark){
                cell.accessoryType = UITableViewCellAccessoryType.None
                LocacaoAux.novaLocacao.comodidades.elevadore = false
            }else{
                cell.accessoryType = UITableViewCellAccessoryType.Checkmark
                LocacaoAux.novaLocacao.comodidades.elevadore = true
            }
        case 10:
            print("Piscina")
            if (cell.accessoryType == UITableViewCellAccessoryType.Checkmark){
                cell.accessoryType = UITableViewCellAccessoryType.None
                LocacaoAux.novaLocacao.comodidades.piscina = false
            }else{
                cell.accessoryType = UITableViewCellAccessoryType.Checkmark
                LocacaoAux.novaLocacao.comodidades.piscina = true
            }
        case 11:
            print("Quintal")
            if (cell.accessoryType == UITableViewCellAccessoryType.Checkmark){
                cell.accessoryType = UITableViewCellAccessoryType.None
                LocacaoAux.novaLocacao.comodidades.quintal = false
            }else{
                cell.accessoryType = UITableViewCellAccessoryType.Checkmark
                LocacaoAux.novaLocacao.comodidades.quintal = true
            }
        case 12:
            print("Jardim")
            if (cell.accessoryType == UITableViewCellAccessoryType.Checkmark){
                cell.accessoryType = UITableViewCellAccessoryType.None
                LocacaoAux.novaLocacao.comodidades.jardim = false
            }else{
                cell.accessoryType = UITableViewCellAccessoryType.Checkmark
                LocacaoAux.novaLocacao.comodidades.jardim = true
            }
        case 13:
            print("Permite Animais de Estimação")
            if (cell.accessoryType == UITableViewCellAccessoryType.Checkmark){
                cell.accessoryType = UITableViewCellAccessoryType.None
                LocacaoAux.novaLocacao.comodidades.animais = false
            }else{
                cell.accessoryType = UITableViewCellAccessoryType.Checkmark
                LocacaoAux.novaLocacao.comodidades.animais = true
            }
        case 14:
            print("Portaria 24h")
            if (cell.accessoryType == UITableViewCellAccessoryType.Checkmark){
                cell.accessoryType = UITableViewCellAccessoryType.None
                LocacaoAux.novaLocacao.comodidades.portaria = false
            }else{
                cell.accessoryType = UITableViewCellAccessoryType.Checkmark
                LocacaoAux.novaLocacao.comodidades.portaria = true
            }
        case 15:
            print("Brinquedoteca")
            if (cell.accessoryType == UITableViewCellAccessoryType.Checkmark){
                cell.accessoryType = UITableViewCellAccessoryType.None
                LocacaoAux.novaLocacao.comodidades.brinquedoteca = false
            }else{
                cell.accessoryType = UITableViewCellAccessoryType.Checkmark
                LocacaoAux.novaLocacao.comodidades.brinquedoteca = true
            }
        case 16:
            print("Academia")
            if (cell.accessoryType == UITableViewCellAccessoryType.Checkmark){
                cell.accessoryType = UITableViewCellAccessoryType.None
                LocacaoAux.novaLocacao.comodidades.academia = false
            }else{
                cell.accessoryType = UITableViewCellAccessoryType.Checkmark
                LocacaoAux.novaLocacao.comodidades.academia = true
            }
        
            
            
        default: break
        }
    
    }
    
    //MARK: Custom Methods
    func loadValuesToView(){
        if(LocacaoAux.novaLocacao.size > 0){
            self.textField.text = String(LocacaoAux.novaLocacao.size)
        }
        
        
        qntQuartos = LocacaoAux.novaLocacao.bedrooms
        quartosSegmentedControl.setTitle(String(qntQuartos), forSegmentAtIndex: 1)
        qntBanheiros = LocacaoAux.novaLocacao.toilets
        banheirosSegmentedControl.setTitle(String(qntBanheiros), forSegmentAtIndex: 1)
        qntSalas = LocacaoAux.novaLocacao.salas
        salasSegmentedControl.setTitle(String(qntSalas), forSegmentAtIndex: 1)
        qntVagas = LocacaoAux.novaLocacao.garageSpaces
        vagasSegmentedControl.setTitle(String(qntVagas), forSegmentAtIndex: 1)
        
        for comodidade in comodidadesCheckListCells {
            switch(comodidade.tag){
            case 1:
                if(LocacaoAux.novaLocacao.comodidades.mobiliado){
                    comodidade.accessoryType = UITableViewCellAccessoryType.Checkmark
                }
                break
            case 2:
                if(LocacaoAux.novaLocacao.comodidades.aquecimento){
                    comodidade.accessoryType = UITableViewCellAccessoryType.Checkmark
                }
                break
            case 3:
                if(LocacaoAux.novaLocacao.comodidades.acessibilidade){
                    comodidade.accessoryType = UITableViewCellAccessoryType.Checkmark
                }
                break
            case 4:
                if(LocacaoAux.novaLocacao.comodidades.elevadore){
                    comodidade.accessoryType = UITableViewCellAccessoryType.Checkmark
                }
                break
            case 5:
                if(LocacaoAux.novaLocacao.comodidades.piscina){
                    comodidade.accessoryType = UITableViewCellAccessoryType.Checkmark
                }
                break
            case 6:
                if(LocacaoAux.novaLocacao.comodidades.quintal){
                    comodidade.accessoryType = UITableViewCellAccessoryType.Checkmark
                }
                break
            case 7:
                if(LocacaoAux.novaLocacao.comodidades.jardim){
                    comodidade.accessoryType = UITableViewCellAccessoryType.Checkmark
                }
                break
            case 8:
                if(LocacaoAux.novaLocacao.comodidades.animais){
                    comodidade.accessoryType = UITableViewCellAccessoryType.Checkmark
                }
                break
            case 9:
                if(LocacaoAux.novaLocacao.comodidades.portaria){
                    comodidade.accessoryType = UITableViewCellAccessoryType.Checkmark
                }
                break
            case 10:
                if(LocacaoAux.novaLocacao.comodidades.brinquedoteca){
                    comodidade.accessoryType = UITableViewCellAccessoryType.Checkmark
                }
                break
            case 11:
                if(LocacaoAux.novaLocacao.comodidades.academia){
                    comodidade.accessoryType = UITableViewCellAccessoryType.Checkmark
                }
                break
                
            default:
                break
            }
        }
    }

}
