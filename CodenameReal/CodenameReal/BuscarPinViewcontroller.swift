//
//  BuscarPinViewcontroller.swift
//  CodenameReal
//
//  Created by jesse filho on 12/1/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

import CloudKit

class BuscarPinViewcontroller: UIViewController, UITextFieldDelegate {

    let locacaoManager = LocationManager(database: CKContainer.defaultContainer().publicCloudDatabase)
    var locacao = LocationRecord()
    
    @IBOutlet weak var pin: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dispatch_async(dispatch_get_main_queue(),{
            print("Testando conexão e ao fim, irá carregar os dados")
            self.testeConexao()
        })
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - Keyboards
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.buscaPin()
        return true
    }
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
         self.dismissKeyboard()
        self.buscaPin()
        return true
    }
    
    @IBAction func procurarImovel(sender: AnyObject) {
        self.dismissKeyboard()
        self.buscaPin()
    }
    
    func buscaPin(){
        dispatch_async(dispatch_get_main_queue(),{
            self.indicatorLoadStart()
        })
        
        locacao.pin = self.pin.text!
        self.locacaoManager.fetchPIN(locacao, callback: { (success, locacaoCallBack) -> () in
            if success{
                print(locacaoCallBack?.pin)// retorna toda a os dados de locacao
                print(locacaoCallBack?.locTitle)
    
                self.locacaoBuscado = locacaoCallBack
                
                dispatch_async(dispatch_get_main_queue(),{
                    self.indicatorLoadStop()
                    self.performSegueWithIdentifier("goToImovel", sender: nil)
                })
            }else{
                print("Pin não encontrado, verifique se o pin foi digitado corretamente")
                let msg = "Pin não encontrado, verifique se o pin foi digitado corretamente."
                self.alert(msg)
            }
        })
    }
    
    var locacaoBuscado : LocationRecord?
    
    // MARK: - Segue com Dados
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if (segue.identifier == "goToImovel"){
            
            if ( segue.destinationViewController.respondsToSelector(Selector("insereLocacao:"))){
                segue.destinationViewController.performSelector(Selector("insereLocacao:"), withObject: locacaoBuscado)
            }
            
        }
        
    }
    
    
    
    func goToNewCard(notification : NSNotification){
        self.tabBarController?.selectedIndex = 0
    }
    
    
    // MARK: - activity Indicator
    
    var activityIndicator = UIActivityIndicatorView()
    
    func indicatorLoadStart (){
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.White)
        activityIndicator.backgroundColor = UIColor(white: 0.1, alpha: 0.7)
        activityIndicator.frame = self.view.frame
        activityIndicator.center = self.view.center
        activityIndicator.autoresizesSubviews = true
        activityIndicator.autoresizingMask = .FlexibleHeight
        
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        self.view.bringSubviewToFront(activityIndicator)
    }
    
    func indicatorLoadStop(){
        activityIndicator.stopAnimating()
        activityIndicator.removeFromSuperview()
    }
    
    // MARK: - Alerts
    func alert(msg : String){
        let alert = UIAlertController(title: "Ops!!!", message: msg, preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addAction(UIAlertAction(title: "Certo", style: .Default, handler: { action in
            switch action.style{
            case .Default:
                print("default")
                
                print(" colocar foco no textfield")
                
            case .Cancel:
                print("cancel")
                
            case .Destructive:
                print("destructive")
            }
        }))
        
        dispatch_async(dispatch_get_main_queue(),{
            self.indicatorLoadStop()
            self.presentViewController(alert, animated: true, completion: nil)
            
        })
    }
    
    // MARK: - Teste de Conexão
    func testeConexao (){
        let status = Reach().connectionStatus()
        switch status {
        case .Unknown, .Offline:
            print("Not connected")
            
            let alertController = UIAlertController(title: "Sem Conexão", message: "Verifique sua conexão com a internet e tente novamente", preferredStyle: .Alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .Default) { (action) in
                dispatch_async(dispatch_get_main_queue(),{
                    self.indicatorLoadStop()
                    self.navigationController?.popToRootViewControllerAnimated(true)
                })
                
            }
            alertController.addAction(OKAction)
            
            self.presentViewController(alertController, animated: true) {
                // ...
            }
        case .Online(.WWAN):
            print("Connected via WWAN")
           
            
            
        case .Online(.WiFi):
            print("Connected via WiFi")
            
            
        }
    }
}
