//
//  TableViewController.swift
//  CodenameReal
//
//  Created by jesse filho on 11/15/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

import UIKit
import MobileCoreServices



class DetalhesTableViewController: UITableViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
 
    let imagePicker = UIImagePickerController()
    
    //MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

       
        imagePicker.delegate = self
        
     }
    
    override func viewWillAppear(animated: Bool) {
        
    }
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.view.translatesAutoresizingMaskIntoConstraints = true;
    }
    // MARK: - Table view data source
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        
        print("You selected cell #\(indexPath.row)!")
        
        //let cell: UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!
        switch indexPath.row{
        case 0:
            print(" photo was pressed")
            //show the action sheet (i.e. the little pop-up box from the bottom that allows you to choose whether you want to pick a photo from the photo library or from your camera
            let optionMenu = UIAlertController(title: nil, message: "Escolha sua opção:", preferredStyle: UIAlertControllerStyle.ActionSheet)
            optionMenu.view.alpha = 0.8
            
            let photoLibraryOption = UIAlertAction(title: "Galeria de Fotos", style: .Default, handler: { (alert: UIAlertAction!) -> Void in
                print("from library")
                
                
                //shows the photo library
                self.imagePicker.allowsEditing = true
                self.imagePicker.sourceType = .PhotoLibrary
                self.imagePicker.modalPresentationStyle = .Popover
                self.presentViewController(self.imagePicker, animated: true, completion: nil)
            })
            let cameraOption = UIAlertAction(title: "Camera", style: .Default, handler: { (alert: UIAlertAction!) -> Void in
                print("take a photo")
                //shows the camera
                self.imagePicker.allowsEditing = true
                self.imagePicker.sourceType = .Camera
                self.imagePicker.modalPresentationStyle = .Popover
                self.presentViewController(self.imagePicker, animated: false, completion: nil)
                
            })
            let cancelOption = UIAlertAction(title: "Cancelar", style: .Cancel, handler: {
                (alert: UIAlertAction!) -> Void in
                print("Cancel")
                //self.dismissViewControllerAnimated(true, completion: nil)
                self.navigationController?.popViewControllerAnimated(true)
            })
            
            //Adding the actions to the action sheet. Camera will only show up as an option if the camera is available in the first place.
            optionMenu.addAction(photoLibraryOption)
            optionMenu.addAction(cancelOption)
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) == true {
                optionMenu.addAction(cameraOption)} else {
                print ("I don't have a camera.")
            }
            
            
            self.presentViewController(optionMenu, animated: true, completion: nil)
            
            /*
            just adding extra text for fun
            */

        case 1:
            print("Fui colocar um titulo")
        case 2:
            print("opção de Escrever Resumo")
        case 3:
            print("opção de pagamentos")
        case 4:
            print("Datas que estou disponível")
            
        case 5:
            print("Visualizar")
            let alert = UIAlertController(title: "Salvo", message: "Legal, seus dados foram salvos e seguros. Edite-os quando desejar.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.Cancel, handler: nil))
            alert.view.translatesAutoresizingMaskIntoConstraints = false
            self.presentViewController(alert, animated: true, completion: nil)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { action in
                switch action.style{
                case .Default:
                    print("default")
                    
                    print(" garantir que os dados foram salvos e que possam ser recarregados sempre que o usuario desejar")
                    self.navigationController?.popViewControllerAnimated(true)
                    self.performSegueWithIdentifier("resumo", sender: nil)
                    
                case .Cancel:
                    print("cancel")
                    
                case .Destructive:
                    print("destructive")
                }
            }))

        
        default: break
        }
        
    }
    
    //MARK: - PhotoPickerController
    
    @IBOutlet weak var imageButton: UIButton!
    @IBOutlet weak var imageButtonImage: UIImageView!
    
    // MARK: - Image Picker Delegates
    //The UIImagePickerController is a view controller that gets presented modally. When we select or cancel the picker, it runs the delegate, where we handle the case and dismiss the modal.
    
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        print("finished picking image")
     
    }
    
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        //handle media here i.e. do stuff with photo  
        print("imagePickerController called")
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        imageButtonImage.image = chosenImage

        LocacaoAux.novaLocacao.foto = chosenImage

        dismissViewControllerAnimated(false, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        print("imagePickerControllerDidCancel called")
        
        dismissViewControllerAnimated(true, completion: nil)
        
    }



}
