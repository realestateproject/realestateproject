//
//  ImovelTableViewCell.swift
//  CodenameReal
//
//  Created by Matheus Coelho Berger on 1/12/16.
//  Copyright © 2016 Matheus Coelho Berger. All rights reserved.
//

import UIKit

class ImovelTableViewCell: UITableViewCell, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var imagemImovel: UIImageView!
    @IBOutlet weak var tituloImovel: UILabel!
    @IBOutlet weak var donoImovel: UILabel!
    
    @IBOutlet weak var valorAluguel: UILabel!
    @IBOutlet weak var dataVencimento: UILabel!
    
    @IBOutlet weak var checkoutButton: UIButton!
    @IBOutlet weak var tableview: UITableView!
    
    
    
    
    var valorAluguelData : String = ""
    var dataVencimentoData : String = ""
    
    
    
   
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        
        
        tableview.delegate = self
        tableview.dataSource = self
        
        
        
        if((self.valorAluguel) != nil){
            self.valorAluguelData = self.valorAluguel.text!
        }
        if((self.dataVencimento) != nil){
            self.valorAluguelData = self.valorAluguel.text!
        }
        
        
        let nibName = UINib(nibName: "BasicTableViewCell", bundle:nil)
        tableview.registerNib(nibName, forCellReuseIdentifier: "BasicCell")
        tableview.rowHeight = 55.0
        
        customizarTableView(tableview)
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
   
    
    @IBAction func goToCheckout(sender: AnyObject)
    {
        NSNotificationCenter.defaultCenter().postNotificationName("goToCheckout", object: nil)
    }
    
    // MARK - TableView
    
    
    //Delegate
    
    func customizarTableView(tableView: UITableView) -> Bool
    {
        tableView.reloadData()
        tableView.reloadInputViews()
        
        return true
    }
    
    //Datasource
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 2
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("BasicCell", forIndexPath: indexPath) as! BasicTableViewCell
        
        if indexPath.row == 0
        {
            cell.left.text = "Valor Do Aluguel"
            cell.right.text = "R$ "+self.valorAluguelData
        }
        else
        {
            cell.left.text = "Dia do Vencimento"
            var tst : String = " "
            tst = self.dataVencimentoData
            cell.right.text = "Dia " + tst
        }
        
        print(cell)
        
        return cell
    }

    
}
