//
//  ImovelHomeViewController.h
//  CodenameReal
//
//  Created by Matheus Coelho Berger on 10/23/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImovelHomeViewController : UIViewController

@property (assign, nonatomic) NSUInteger index;

@property (assign, nonatomic) NSString *titulo;
@property (assign, nonatomic) NSString *stringNomeLocador;

@property (strong, nonatomic) NSString *stringMoeda;
@property (weak, nonatomic) NSString *stringValor;
@property (weak, nonatomic) NSString *stringDataVencimento;

@property (weak, nonatomic) NSString *imagemNome;

@property (weak, nonatomic) NSString *stringStatusPagamento;
@property (weak, nonatomic) NSString *textoBotaoPagar;

@end
