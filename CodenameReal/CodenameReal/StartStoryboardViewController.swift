//
//  StartStoryboardViewController.swift
//  CodenameReal
//
//  Created by jesse filho on 11/15/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

import UIKit

class StartStoryboardViewController: UIViewController {

    @IBAction func locadorStoryBoard(sender: AnyObject) {
        
        let storyboard = UIStoryboard(name: "Locador", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier("TabBarControllerAddImoveis") as UIViewController
        presentViewController(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func loactarioStoryBoard(sender: AnyObject) {
        let storyboard = UIStoryboard(name: "Locatario", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier("TabBarLocatario") as UIViewController
        presentViewController(vc, animated: true, completion: nil)
    }
    
    @IBAction func offlineStoryBoard(sender: AnyObject) {
        let storyboard = UIStoryboard(name: "Locatario", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier("offline") as UIViewController
        presentViewController(vc, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
