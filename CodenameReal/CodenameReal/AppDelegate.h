//
//  AppDelegate.h
//  CodenameReal
//
//  Created by Matheus Coelho Berger on 10/6/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
+(instancetype) sharedDelegate;

- (NSURL*)getApplicationDocumentsDirectory;
- (NSManagedObjectModel*)getManagedObjectModel;
- (NSPersistentStoreCoordinator*) getPersistentStoreCoordinator;
- (NSManagedObjectContext*)getManagedObjectContext;
- (void)saveContext;
@end

