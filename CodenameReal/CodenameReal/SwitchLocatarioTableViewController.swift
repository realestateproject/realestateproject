//
//  SwitchLocatarioViewController.swift
//  CodenameReal
//
//  Created by jesse filho on 11/23/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

import UIKit

class SwitchLocatarioTableViewController: UITableViewController, UITextFieldDelegate {
    
    @IBOutlet weak var viewImages: UIView!
    @IBOutlet weak var background: UIImageView!
    @IBOutlet weak var filtro: UIImageView!
    @IBOutlet weak var fotoPerfil: UIImageView!
    
    
    let backgroundFoto : String = "Imagem_FiltroColorido_750x500"
    let fotoLocador : String = "Imagem_FiltroColorido_750x500"
    
    
    @IBAction func sairStoryBoard(sender: AnyObject) {
        CoreDataHelper.removeLoggedUser { (success) -> Void in
            if success {
                CodenameRealStaticObjects.loggedUser.resetarInstancia()
                CodenameRealStaticObjects.resetObjects()
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewControllerWithIdentifier("TabBarLogout") as UIViewController
                self.presentViewController(vc, animated: true, completion: nil)
            } else {
                print("bugou logout")
            }
        }
        
        
    }
    
    @IBAction func locatarioStoryBoard(sender: AnyObject) {
        
        let storyboard = UIStoryboard(name: "Locador", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier("TabBarControllerAddImoveis") as UIViewController
        presentViewController(vc, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.carregaQuadroFotoPerfil()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        
        print("You selected cell #\(indexPath.row)!")
        
        
    }
    
    func carregaQuadroFotoPerfil (){
        
        
        self.fotoPerfil.frame = CGRectMake(0, 0, 140 , 140)
        if(CoreDataHelper.getLoggedUser()){
            self.fotoPerfil.image = CodenameRealStaticObjects.loggedUser.fotoPerfil
            self.background.image = CodenameRealStaticObjects.loggedUser.fotoPerfil
        }else{
            self.fotoPerfil.image =  UIImage(named:"Imagem_Placeholder_Blurview_750x500.png")
        }
        self.fotoPerfil.layer.borderWidth = 2.0
        self.fotoPerfil.layer.cornerRadius = self.fotoPerfil.frame.size.height/2
        self.fotoPerfil.layer.borderColor = UIColor(red: 173/255 , green: 0/255, blue: 65/255, alpha: 1).CGColor
        self.fotoPerfil.clipsToBounds = true
    }
    
}

