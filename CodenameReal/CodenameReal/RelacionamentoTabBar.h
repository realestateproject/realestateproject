//
//  RelacionamentoTabBar.h
//  CodenameReal
//
//  Created by Matheus Coelho Berger on 11/17/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RelacionamentoTabBar : UITabBarController <UITabBarControllerDelegate>

@end
