//
//  TipoResidenciaTableViewController.h
//  CodenameReal
//
//  Created by jesse filho on 10/29/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TipoResidenciaTableViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>


@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSIndexPath* checkedIndexPath;

@end
