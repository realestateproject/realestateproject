//
//  HomeBViewController.h
//  CodenameReal
//
//  Created by Matheus Coelho Berger on 10/27/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeBViewController : UIViewController <UIPageViewControllerDataSource>

@property (strong, nonatomic) UIPageViewController *pageController;

@end
