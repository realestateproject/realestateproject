//
//  TipoLocalidadeMenuVC.swift
//  CodenameReal
//
//  Created by Thiago Borges Jordani on 11/18/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

import UIKit

public class TipoLocalidadeMenuVC : UIViewController {
    
    
    public override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        if identifier == "tipoResidencia" {
            LocacaoAux.novaLocacao.tipoLocacao = 0
        }
        return true
    }
}
