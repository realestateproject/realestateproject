//
//  PagamentoTableViewController.swift
//  CodenameReal
//
//  Created by jesse filho on 11/16/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

import UIKit

class PagamentoTableViewController: UITableViewController, UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate {

    
    //MARK: Vars
    
    var valorAluguel: Double =  0.0
    var iptu: Double = 0.0
    var totalMensal: Float = 0.0
    var minMeses: Int = 0
    var maxMeses: Int = 0
    var arrayDias:[String] = [String]()
   let currencyFormatter = NSNumberFormatter()
    
    //MARK: IBActions
    @IBAction func valorAluguelTextField(textField: UITextField) {
        print(textField.text);
    }
    
    
 
    @IBAction func iptuTextField(textField: UITextField) {
         print(textField.text);
    }
    
    //MARK: IBOutlets
    @IBOutlet weak var valorTotalAluguel: UILabel!
    @IBOutlet weak var valorAlguel: UITextField!
    @IBOutlet weak var valorIPTU: UITextField!
    @IBOutlet weak var tempoMinSegmentedControl: UISegmentedControl!
    @IBOutlet weak var tempoMaxSegmentedControl: UISegmentedControl!
    
    //MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //present the standart of text
        self.standartFormatter()
        
        //date picker
        self.diasPagamento()
        // formatter textfield
        currencyFormatter.numberStyle = NSNumberFormatterStyle.CurrencyStyle
        currencyFormatter.locale = NSLocale(localeIdentifier: "pt_BR")
        
        
        self.minMeses = LocacaoAux.novaLocacao.pagamento.tempoMinContrato
        self.tempoMinSegmentedControl.setTitle(String(minMeses), forSegmentAtIndex: 1)
        self.maxMeses = LocacaoAux.novaLocacao.pagamento.tempoMinContrato
        self.tempoMaxSegmentedControl.setTitle(String(maxMeses), forSegmentAtIndex: 1)
        self.valorAlguel.text = self.currencyStringFromNumber(LocacaoAux.novaLocacao.pagamento.valorAluguel)
        self.valorIPTU.text = self.currencyStringFromNumber(LocacaoAux.novaLocacao.pagamento.valorIPTU)
        self.iptu = LocacaoAux.novaLocacao.pagamento.valorIPTU
        self.valorAluguel = LocacaoAux.novaLocacao.pagamento.valorAluguel

        self.pickerView.selectRow(LocacaoAux.novaLocacao.pagamento.diaVencimento - 1, inComponent: 0, animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Custom Methods
    func calculaTotal (){
        let alluPercent = (self.valorAluguel + self.iptu) * 0.06 //calcula valor do aluguel
        let sum = (self.valorAluguel + self.iptu) - alluPercent
        valorTotalAluguel.text = String(format: "%.2f", sum)
        
        print(valorTotalAluguel.text)
    }
    
    //MARK: Keyboard
    override func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        view.endEditing(true)
        let alluPercent = (self.valorAluguel + self.iptu) * 0.06 //calcula valor do aluguel
        let sum = (self.valorAluguel + self.iptu) - alluPercent
      valorTotalAluguel.text = String(format: "%.2f", sum)
    }

    // MARK: Segments Control
    
    //Ajustar mudança automatica para valores impossiveis de max & min
    @IBAction func valorMinimoMeses(sender: UISegmentedControl) {
        //view.endEditing(true) // close keyboard
        
        
        if( sender.selectedSegmentIndex == 0 && minMeses >= 0){
            
            if minMeses <= 0{
                minMeses = 0
            }else{
                minMeses = minMeses - 1
            }
        }
        if(sender.selectedSegmentIndex == 2 ){
            minMeses = minMeses + 1
            if(maxMeses<minMeses){
                maxMeses = minMeses
                self.tempoMaxSegmentedControl.setTitle(String(maxMeses), forSegmentAtIndex: 1)
            }
        }
        sender.setTitle(String(minMeses), forSegmentAtIndex: 1)
    }
    @IBAction func valorMaximoMeses(sender: UISegmentedControl) {
        view.endEditing(true) // close keyboard
        

        
        if( sender.selectedSegmentIndex == 0 && maxMeses >= 0){
            maxMeses = minMeses
            
            if maxMeses <= minMeses || maxMeses <= 0{
                maxMeses = minMeses
            }else{
                maxMeses = maxMeses - 1
                if(maxMeses<minMeses){
                    minMeses = maxMeses
                    self.tempoMinSegmentedControl.setTitle(String(minMeses), forSegmentAtIndex: 1)
                }
            }
            
           
        }
        if(sender.selectedSegmentIndex == 2 ){
            maxMeses = maxMeses + 1
        }
        sender.setTitle(String(maxMeses), forSegmentAtIndex: 1)
    }

    // MARK: - Table view data source
   override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        
        print("You selected cell #\(indexPath.row)!")
        
        switch indexPath.row{
        case 0:
            print("None")
        case 1:
            print("Colocar valor do aluguel")
        case 3:
            self.calculaTotal()
            view.endEditing(true)
        default: break
        }
    }
    
    //MARK: - TextField
    func standartFormatter() {
        
        valorAlguel.text = "R$0,00"
        valorIPTU.text = "R$0,00"
        
        valorAlguel.addTarget(self, action: "textFieldDidChangeAluguel:", forControlEvents: UIControlEvents.EditingChanged)
        valorIPTU.addTarget(self, action: "textFieldDidChangeIPTU:", forControlEvents: UIControlEvents.EditingChanged)

        
    }
    
    func textFieldDidChangeAluguel(textField: UITextField) {
        let text = textField.text!.stringByReplacingOccurrencesOfString(currencyFormatter.currencySymbol, withString: "").stringByReplacingOccurrencesOfString(currencyFormatter.groupingSeparator, withString: "").stringByReplacingOccurrencesOfString(currencyFormatter.decimalSeparator, withString: "")
        
        print((text as NSString).doubleValue / 100.0)
        self.valorAluguel = (text as NSString).doubleValue / 100.0
        textField.text = currencyFormatter.stringFromNumber((text as NSString).doubleValue / 100.0)
    }
    
    func textFieldDidChangeIPTU(textField: UITextField) {
        let text = textField.text!.stringByReplacingOccurrencesOfString(currencyFormatter.currencySymbol, withString: "").stringByReplacingOccurrencesOfString(currencyFormatter.groupingSeparator, withString: "").stringByReplacingOccurrencesOfString(currencyFormatter.decimalSeparator, withString: "")
        
        print((text as NSString).doubleValue / 100.0)
        self.iptu = (text as NSString).doubleValue / 100.0
        textField.text = currencyFormatter.stringFromNumber((text as NSString).doubleValue / 100.0)
    }
    
    func currencyStringFromNumber(number: Double) -> String {
        let formatter = NSNumberFormatter()
        formatter.numberStyle = NSNumberFormatterStyle.CurrencyStyle
        formatter.locale = NSLocale(localeIdentifier: "pt_BR")
        return formatter.stringFromNumber(number)!
    }
    
    //MARK: - Date Picker
  
    @IBOutlet weak var pickerView: UIPickerView!
    func diasPagamento(){
       
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        arrayDias = ["01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30"]
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayDias.count
    }
    // The data to return for the row and component (column) that's being passed in
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrayDias[row]
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){

            print(row+1)
  
    }
    
    @IBAction func tapToSalve(sender: AnyObject) {
        LocacaoAux.novaLocacao.pagamento.valorAluguel = self.valorAluguel
        LocacaoAux.novaLocacao.pagamento.valorIPTU = self.iptu
        LocacaoAux.novaLocacao.pagamento.tempoMinContrato = self.minMeses
        LocacaoAux.novaLocacao.pagamento.tempoMaxContrato = self.maxMeses
        LocacaoAux.novaLocacao.pagamento.diaVencimento = self.pickerView.selectedRowInComponent(0) + 1
        
        self.navigationController?.popViewControllerAnimated(true)
        // Garantir que tudo seja salvo no coreData aqui
    }
    
}
