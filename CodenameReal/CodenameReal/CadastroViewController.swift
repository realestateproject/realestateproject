//
//  CadastroViewController.swift
//  CodenameReal
//
//  Created by jesse filho on 11/23/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

import CloudKit


class CadastroViewController: UIViewController, UITextFieldDelegate {
   
    let usersManager = UsersManager(database: CKContainer.defaultContainer().publicCloudDatabase)
    var user = Users()
    
    

    @IBOutlet weak var nome: UITextField!
    @IBOutlet weak var sobrenome: UITextField!
    @IBOutlet weak var dataNascimento: UITextField!
    
    @IBOutlet weak var email: UITextField!
    var emailAux: String?
    
    @IBOutlet weak var senha: UITextField!
    var senhaAux: String?

    @IBAction func voltar(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func FecharTela(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        email.text = emailAux!
        senha.text = senhaAux!
        // Do any additional setup after loading the view.
        
        self.nome.delegate = self
        self.sobrenome.delegate = self
        self.dataNascimento.delegate = self
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Keyboards
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    // MARK: - Recebe dados
    func insereEmail(emailTexto: String){
        emailAux = emailTexto
    }
    
    func insereSenha(senhaTexto: String){
        senhaAux = senhaTexto
    }

    // MARK: - Salvando cloudkit
    
    func salvar (){
        if (self.nome.text == "" || self.sobrenome.text == "" || self.dataNascimento.text == ""){
            print("Dados null")
            self.alert("Algum dados foram esquecido.")
        }else{
            dispatch_async(dispatch_get_main_queue(),{
                self.indicatorLoadStart()
            })
            self.user.email = emailAux!
            
            
            self.usersManager.checkEmail(user, callback: { (success, users) -> () in
                if success {
                    users!.nome = self.nome.text!
                    users!.sobrenome = self.sobrenome.text!
                    users!.dataNascimento = self.dataNascimento.text!
                    
                    Users.novoUser.nome = self.nome.text!
                    Users.novoUser.sobrenome = self.sobrenome.text!
                    self.usersManager.updateUsers(users!, callback: { (success) -> () in
                        if success{
                            dispatch_async(dispatch_get_main_queue(),{
                                self.indicatorLoadStop()
                                self.performSegueWithIdentifier("cadastroBasico", sender: nil)
                            })
                        }else{
                            print("Erro em  createAccountUsers")
                        }
                    })
                    
                }else{
                    print("Erro em  createAccountUsers")
                }
            })
        }

    }
    @IBAction func salvarCadastroBasico(sender: AnyObject) {
        
        dispatch_async(dispatch_get_main_queue(),{
            print("Testando conexão e ao fim, irá carregar os dados")
            self.testeConexao()
        })
        
    }
    
    
    // MARK: - Alert
    func alert(msg : String){
        let alert = UIAlertController(title: "Ops!!!", message: msg, preferredStyle: UIAlertControllerStyle.Alert)
        
        
        alert.addAction(UIAlertAction(title: "Certo", style: .Default, handler: { action in
            switch action.style{
            case .Default:
                print("default")
                
                print(" colocar foco no textfield")
                
            case .Cancel:
                print("cancel")
                
            case .Destructive:
                print("destructive")
            }
        }))
        
        dispatch_async(dispatch_get_main_queue(),{
            self.indicatorLoadStop()
            self.presentViewController(alert, animated: true, completion: nil)
            
        })
    }
    
    // MARK: - activity Indicator
    var activityIndicator = UIActivityIndicatorView()
    
    func indicatorLoadStart (){
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.White)
        activityIndicator.backgroundColor = UIColor(white: 0.1, alpha: 0.7)
        activityIndicator.frame = self.view.frame
        activityIndicator.center = self.view.center
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        self.view.bringSubviewToFront(activityIndicator)
    }
    
    func indicatorLoadStop(){
        activityIndicator.stopAnimating()
        activityIndicator.removeFromSuperview()
    }
    
    
    // MARK: - Teste de Conexão
    func testeConexao (){
        let status = Reach().connectionStatus()
        switch status {
        case .Unknown, .Offline:
            print("Not connected")
            
            let alertController = UIAlertController(title: "Sem Conexão", message: "Verifique sua conexão com a internet e tente novamente", preferredStyle: .Alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .Default) { (action) in
                dispatch_async(dispatch_get_main_queue(),{
                    self.indicatorLoadStop()
                    self.navigationController?.popToRootViewControllerAnimated(true)
                })
                
            }
            alertController.addAction(OKAction)
            
            self.presentViewController(alertController, animated: true) {
                // ...
            }
        case .Online(.WWAN):
            print("Connected via WWAN")
            self.salvar()
            
            
        case .Online(.WiFi):
            print("Connected via WiFi")
            self.salvar()
                        
        }
    }

}
