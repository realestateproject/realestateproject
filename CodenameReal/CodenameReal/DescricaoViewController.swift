//
//  DescricaoViewController.swift
//  CodenameReal
//
//  Created by jesse filho on 11/17/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

import UIKit

class DescricaoViewController: UIViewController, UITextViewDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.textView.delegate = self
        // Do any additional setup after loading the view.
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
       
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.startTextView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: IBActions
    @IBOutlet weak var textView: UITextView!
    
    func startTextView(){
        if LocacaoAux.novaLocacao.resumo.characters.count > 0 {
            textView.text = LocacaoAux.novaLocacao.resumo
            textView.becomeFirstResponder()
        }else{
            textView.text = "Digite sua descrição aqui"
            textView.textColor = UIColor.lightGrayColor()
            textView.becomeFirstResponder()
            textView.selectedTextRange = textView.textRangeFromPosition(textView.beginningOfDocument, toPosition: textView.beginningOfDocument)
        }
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        
        // Combine the textView text and the replacement text to
        // create the updated text string
        let currentText:NSString = textView.text
        let updatedText = currentText.stringByReplacingCharactersInRange(range, withString:text)
        
        // If updated text view will be empty, add the placeholder
        // and set the cursor to the beginning of the text view
        if updatedText.isEmpty {
            
            textView.text = "Digite sua descrição aqui"
            textView.textColor = UIColor.lightGrayColor()
            
            textView.selectedTextRange = textView.textRangeFromPosition(textView.beginningOfDocument, toPosition: textView.beginningOfDocument)
            
            return false
        }
            
            // Else if the text view's placeholder is showing and the
            // length of the replacement string is greater than 0, clear
            // the text view and set its color to black to prepare for
            // the user's entry
        else if textView.textColor == UIColor.lightGrayColor() && !text.isEmpty {
            textView.text = nil
            textView.textColor = UIColor.blackColor()
        }
        
        return true
    }
    
    func textViewDidChangeSelection(textView: UITextView) {
        if self.view.window != nil {
            if textView.textColor == UIColor.lightGrayColor() {
                textView.selectedTextRange = textView.textRangeFromPosition(textView.beginningOfDocument, toPosition: textView.beginningOfDocument)
            }
        }
    }
    //escolher enviar dados para o coredata aqui
    @IBAction func tituloSalvo(sender: AnyObject) {
        let alert = UIAlertController(title: "Salvo", message: "Legal, seus dados estão salvos e seguros. Você pode escolher edita-los quando desejar.", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.Cancel, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { action in
            switch action.style{
            case .Default:
                print("default")
                LocacaoAux.novaLocacao.resumo = self.textView.text
                self.navigationController?.popViewControllerAnimated(true)
                
            case .Cancel:
                print("cancel")
                
            case .Destructive:
                print("destructive")
            }
        }))
        
    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    func textViewShouldEndEditing(textView: UITextView) -> Bool {
        print(textView.text)
        return true
    }


}
