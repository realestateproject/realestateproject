//
//  UserManager.swift
//  CodenameReal
//
//  Created by jesse filho on 11/27/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

import Foundation
import CoreLocation
import CloudKit

//internal protocol UsersManager {
//    func createUsers(users: Users, callback: ((success: Bool, users: Users?) -> ())?)
//    func getSummaryOfUsers(callback: (users: [Users]) -> ())
//    func getUsers(usersID: String, callback: (Users) -> ())
//    func updateUsers(users: Users, callback: ((success: Bool) -> ())?)
//    func deleteUsers(users: Users, callback: ((success: Bool) -> ())?)
//}

class UsersManager : NSObject{
    
    
    let database: CKDatabase
    var ckUsers = Users()
    
    override init() {
        self.database = CKContainer.defaultContainer().publicCloudDatabase
    }
    
    init(database: CKDatabase) {
        self.database = database
    }
    
    
    func checkEmail (users: Users, callback:((success: Bool, users: Users?) -> ())?){
        let predicate = NSPredicate(format: "Email == %@", users.email)
        let query = CKQuery(recordType: users.record.recordType , predicate: predicate)
        database.performQuery(query, inZoneWithID: nil) { (records, error) -> Void in
            if error != nil {
                // erro com conexao cloudkit
                print("There was an error: \(error)")
                
                callback?(success: false, users: nil)
            } else {
                print("Record saved successfully")
                if let results = records {
                    print("\(results.count)")
                    if results.count == 0{
                        callback?(success: false, users: self.ckUsers)
                    }else{
                        self.ckUsers = Users(record: results[0])
                        print(self.ckUsers.senha)
                        if (self.ckUsers.senha == "" || self.ckUsers.senha == "0" || self.ckUsers.senha == "1"){
                            callback?(success: false, users: self.ckUsers)
                        }else{
                            callback?(success: true, users: self.ckUsers)
                        }
                        
                    }
                }
            }
        }
        
    }
    
    
    // envio users e recebe callback sobre o que houve
    func createAccountUsers (users: Users, callback:((success: Bool, users: Users?) -> ())?){
        
        
        ckUsers.email = users.email
        ckUsers.senha = users.senha
        
        database.saveRecord(users.record, completionHandler: { (record, error) in
            if error != nil {
                print("There was an error: \(error)")
                callback?(success: false, users: nil)
            } else {
                print("Record saved successfully")
                callback?(success: true, users: self.ckUsers)
            }
        })
        
    }
    
    func createUsers(users: Users, callback:((success: Bool, users: Users?) -> ())?) {
                database.saveRecord(ckUsers.record) { (record, error) in
            if error != nil {
                print("There was an error: \(error)")
                callback?(success: false, users: nil)
            } else {
                print("Record saved successfully")
                callback?(success: true, users: self.ckUsers)
            }
        }
    }
    func updateUser(users: Users, callback:((success: Bool) -> ())?) {
        // Some here to save it
       
        
        let updateOperation = CKModifyRecordsOperation(recordsToSave: [users.record], recordIDsToDelete: nil)
        updateOperation.perRecordCompletionBlock = { record, error in
            if error != nil {
                // Really important to handle this here
                print("Unable to modify record: \(record). Error: \(error)")
            }
        }
        updateOperation.modifyRecordsCompletionBlock = { saved, _, error in
            if let error = error {
                if error.code == CKErrorCode.PartialFailure.rawValue {
                    print("There was a problem completing the operation. The following records had problems: \(error.userInfo[CKPartialErrorsByItemIDKey])")
                }
                callback?(success: false)
            } else {
                callback?(success: true)
            }
        }
        database.addOperation(updateOperation)
    }

    func getSummaryOfUsers(callback: (users: [Users]) -> ()) {
        // Get a list of all notes. Only some keys populated tho
        let query = CKQuery(recordType: "Note", predicate: NSPredicate(value: true))
        let queryOperation = CKQueryOperation(query: query)
        queryOperation.desiredKeys = ["Email"]
        var records = [Users]()
        queryOperation.recordFetchedBlock = { record in records.append(Users(record: record)) }
        queryOperation.queryCompletionBlock = { _ in callback(users: records) }
        
        database.addOperation(queryOperation)
        
    }
    
//    func updateUsers(users: Users, callback:((success: Bool) -> ())?) {
//        // This the more specific version is preferred
//        let cloudKitUsers = Users(users: users)
//        updateUsers(cloudKitUsers, callback: callback)
//    }
    
    func updateUsers(users: Users, callback:((success: Bool) -> ())?) {
        // Some here to save it
        let updateOperation = CKModifyRecordsOperation(recordsToSave: [users.record], recordIDsToDelete: nil)
        updateOperation.perRecordCompletionBlock = { record, error in
            if error != nil {
                // Really important to handle this here
                print("Unable to modify record: \(record). Error: \(error)")
            }
        }
        updateOperation.modifyRecordsCompletionBlock = { saved, _, error in
            if let error = error {
                if error.code == CKErrorCode.PartialFailure.rawValue {
                    print("There was a problem completing the operation. The following records had problems: \(error.userInfo[CKPartialErrorsByItemIDKey])")
                }
                callback?(success: false)
            } else {
                callback?(success: true)
            }
        }
        database.addOperation(updateOperation)
    }
//
//    func deleteNote(note: Note, callback: ((success: Bool) -> ())?) {
//        let deleteOperation = CKModifyRecordsOperation(recordsToSave: nil, recordIDsToDelete: [CKRecordID(recordName: note.id!)])
//        deleteOperation.perRecordCompletionBlock = { record, error in
//            if error != nil {
//                print("Unable to delete record: \(record). Error: \(error)")
//            }
//        }
//        deleteOperation.modifyRecordsCompletionBlock = { _, deleted, error in
//            if let error = error {
//                if error.code == CKErrorCode.PartialFailure.rawValue {
//                    print("There was a problem completing the operation. The following records had problems: \(error.userInfo[CKPartialErrorsByItemIDKey])")
//                }
//                callback?(success: false)
//            }
//            callback?(success: true)
//        }
//        database.addOperation(deleteOperation)
//    }
    
    func getUsers(usersID: String, callback: (Users) -> ()) {
        let recordID = CKRecordID(recordName: usersID)
        database.fetchRecordWithID(recordID) {
            (record, error) in
            if error != nil {
                print("There was an error: \(error)")
            } else {
                guard let record = record else { return }
                let users = Users(record: record)
                callback(users)
            }
        }
    }
    
    
    func update(userData : Users, callback:((success: Bool) -> ())?)
    {
        
        
        ckUsers.nome   = userData.nome
        ckUsers.sobrenome = userData.sobrenome
        ckUsers.agencia = userData.agencia
        ckUsers.conta = userData.conta
        ckUsers.cpf = userData.cpf
        ckUsers.endereco = userData.endereco
       
        database.saveRecord(ckUsers.record) { record, error in
            dispatch_async(dispatch_get_main_queue()) {
                if error != nil {
                    print("There was an error: \(error)")
                    callback?(success: false)
                } else {
                    print("Record saved successfully")
                    callback?(success: true)
                }
                
            }
        }
    }
    

   
    
    
    //end of class
}
