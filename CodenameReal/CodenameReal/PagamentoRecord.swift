////
////  PagamentoRecord.swift
////  CodenameReal
////
////  Created by Thiago Borges Jordani on 12/3/15.
////  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
////
//
//import Foundation
//import CloudKit
//
//class PagamentoRecord: NSObject {
//    let record: CKRecord
//    
//    override init(){
//        record = CKRecord(recordType: "Pagamento")
//    }
//    
//    init(pagamento: PagamentoAux){
//        record = CKRecord(recordType: "Pagamento")
//        
//        super.init()
//        self.valorAluguel = pagamento.valorAluguel
//        self.valorIPTU = pagamento.valorIPTU
//        self.tempoMinContrato = pagamento.tempoMinContrato
//        self.tempoMaxContrato = pagamento.tempoMaxContrato
//        self.diaVencimento = pagamento.diaVencimento
//    }
//    
//    var locacaoPin: String{
//        get{
//            return(self.record.objectForKey("locacaoPin") as! String)
//        }
//        set (val){
//            record.setObject(val, forKey: "locacaoPin")
//        }
//    }
//    var valorAluguel : Double {
//        get{
//            return(self.record.objectForKey("valorAluguel") as! Double)
//        }
//        set (val){
//            record.setObject(val, forKey: "valorAluguel")
//        }
//    }
//    
//    var valorIPTU : Double {
//        get{
//            return(self.record.objectForKey("valorIPTU") as! Double)
//        }
//        set (val){
//            record.setObject(val, forKey: "valorIPTU")
//        }
//    }
//    
//    var tempoMinContrato : Int {
//        get{
//            return(self.record.objectForKey("tempoMinContrato") as! Int)
//        }
//        set (val){
//            record.setObject(val, forKey: "tempoMinContrato")
//        }
//    }
//    
//    var tempoMaxContrato : Int {
//        get{
//            return(self.record.objectForKey("tempoMaxContrato") as! Int)
//        }
//        set (val){
//            record.setObject(val, forKey: "tempoMaxContrato")
//        }
//    }
//    
//    var diaVencimento : Int {
//        get{
//            return(self.record.objectForKey("diaVencimento") as! Int)
//        }
//        set (val){
//            record.setObject(val, forKey: "diaVencimento")
//        }
//    }
//    
//}
//
//