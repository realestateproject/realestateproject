//
//  HomeByScrollView.m
//  CodenameReal
//
//  Created by Matheus Coelho Berger on 10/27/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

#import "HomeByScrollView.h"

@interface HomeByScrollView ()

@property (weak, nonatomic) IBOutlet UILabel *totalCurrencyLabel;
@property (weak, nonatomic) IBOutlet UILabel *delayedCurrencyLabel;

@property (weak, nonatomic) IBOutlet UIScrollView *baseScrollView;
@property (weak, nonatomic) IBOutlet UIView *baseView;

@property (strong, nonatomic) NSMutableArray *imoveis;
@end

@implementation HomeByScrollView 

- (void) viewDidLoad
{
    [super viewDidLoad];
    self.imoveis = [[NSMutableArray alloc] init];
    
    [self.imoveis addObject:@"teste"];
    
    self.baseScrollView.contentSize = self.baseView.frame.size;
}

@end
