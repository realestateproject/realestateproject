//
//  HomeViewController.h
//  CodenameReal
//
//  Created by Matheus Coelho Berger on 10/23/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewController : UIPageViewController <UIPageViewControllerDataSource, UIPageViewControllerDelegate>

@property (strong, nonatomic) UIPageViewController *pageController;

@end
