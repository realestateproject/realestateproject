//
//  RelacionamentoTabLocadorViewController.m
//  CodenameReal
//
//  Created by Matheus Coelho Berger on 11/25/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

#import "RelacionamentoTabLocadorViewController.h"
#import "UIColor+Locador.h"

@interface RelacionamentoTabLocadorViewController ()

@end

@implementation RelacionamentoTabLocadorViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.delegate = self;
    
    [self appearanceTabBar];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:true];
    [self navigationConfigViewWillappear];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:true];
    [self navigationConfigViewWillDisappear];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    [self.tabBar invalidateIntrinsicContentSize];
    CGFloat tabSize = 60.0;
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (UIInterfaceOrientationIsLandscape(orientation))
    {
        tabSize = 100.0;
    }
    CGRect tabFrame = self.tabBar.frame;
    tabFrame.size.height = tabSize;
    tabFrame.origin.y = self.view.frame.origin.y
    ;
    //self.tabBar.bounds;
    self.tabBar.frame = tabFrame;
    
    // Set the translucent property to NO then back to YES to
    // force the UITabBar to reblur, otherwise part of the
    // new frame will be completely transparent if we rotate
    // from a landscape orientation to a portrait orientation.
    
    self.tabBar.translucent = NO;
    self.tabBar.translucent = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void) appearanceTabBar{
    
    //UIColor *locador = [UIColor LocadorColor]
    NSString *imageBackGroundTabBar = @"NavBar_Residencial_base_750x120";
    // itens in tab bar
    NSString *icon_Imovel_Default = @"Relbar_1_Imovel_Default";
    NSString *icon_Imovel_Selected =@"Relbar_1_Imovel_Selected";
//    NSString *icon_Localidade_Default = @"Relbar_2_Perfil_Default";
//    NSString *icon_Localidade_Selected =@"Relbar_2_Perfil_Selected";
    NSString *icon_informacoes_Default = @"Relbar_4_Contrato_Default";
    NSString *icon_informacoes_Selected =@"Relbar_4_Contrato_Selected";
    
    [[UITabBar appearance] setBackgroundImage:[UIImage imageNamed:imageBackGroundTabBar]];
    
    
    
    UITabBarItem *tabBarItem = [self.tabBar.items objectAtIndex:0];
    UIImage *unselectedImage = [UIImage imageNamed:icon_Imovel_Default ];
    UIImage *selectedImage = [UIImage imageNamed:icon_Imovel_Selected ];
    [tabBarItem setImage: [unselectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tabBarItem setSelectedImage: selectedImage];
    
//    UITabBarItem *tabBarItem1 = [self.tabBar.items objectAtIndex:1];
//    UIImage *unselectedImage1 = [UIImage imageNamed:icon_Localidade_Default];
//    UIImage *selectedImage1 = [UIImage imageNamed:icon_Localidade_Selected];
//    [tabBarItem1 setImage: [unselectedImage1 imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
//    [tabBarItem1 setSelectedImage: selectedImage1];
    
    UITabBarItem *tabBarItem2 = [self.tabBar.items objectAtIndex:1];
    UIImage *unselectedImage2 = [UIImage imageNamed:icon_informacoes_Default];
    UIImage *selectedImage2 = [UIImage imageNamed:icon_informacoes_Selected];
    [tabBarItem2 setImage: [unselectedImage2 imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tabBarItem2 setSelectedImage: selectedImage2];
    
}



-(void) navigationConfigViewWillappear{
    
    NSString *imageBackGroundNav = @"NavBar_Residencial_topo_750x128";
    
    UIImage *image = [UIImage imageNamed:imageBackGroundNav];
    [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
    
    
    
    // set tint color of tab bar
    [[UITabBar appearance] setTintColor:[UIColor whiteColor]];
}
-(void) navigationConfigViewWillDisappear{
    [self.tabBarController.tabBar setBackgroundImage:nil];
    [self.tabBarController.tabBar setBackgroundColor:[UIColor locatarioColor]];
    
    // set tint color of nav
    
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setBackgroundColor:[UIColor locadorColor]];
}


@end
