//
//  LocadorHomeViewController.swift
//  CodenameReal
//
//  Created by Thiago Borges Jordani on 11/18/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

import UIKit

public class LocadorHomeViewController : UIViewController {
    override public func viewDidLoad() {
        super.viewDidLoad()
    }
    
    public override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        if (identifier == "addImovel") {
            let newLoc = LocacaoAux.novaLocacao
            newLoc.resetarInstancia()
        }
        return true
    }
}
