//
//  LocationRecord.swift
//  CodenameReal
//
//  Created by Thiago Borges Jordani on 11/30/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

import Foundation
import CloudKit

class LocationRecord: NSObject{
    let record: CKRecord
    var ownerRecord: Users?
//    var comodidades: ComodidadesRecord = ComodidadesRecord()
//    var pagamento: PagamentoRecord = PagamentoRecord()
//    var endereco: EnderecoRecord = EnderecoRecord()
    //MARK: Initializers
    init(record: CKRecord) {
    self.record = record
    }
    
    override init(){
        record = CKRecord(recordType: "Location")
    }
    
    init(locacao: LocacaoAux){
        record = CKRecord(recordType: "Location")
        super.init()
        //imovel
        self.bedrooms = locacao.bedrooms
        self.contratoIptu = locacao.pagamento.valorIPTU
        self.garageSpaces = locacao.garageSpaces
        self.size = locacao.size
        self.toilets = locacao.toilets
        self.tipoLocacao = locacao.tipoLocacao
        self.salas = locacao.salas
        self.locTitle = locacao.locTitle
        self.resumo = locacao.resumo
        self.tipoResidencia = locacao.tipoResidencia
        self.foto = UIImageToCKAssetManager.imageToCKAsset(locacao.foto!, tempImageName: "fotoLocacao")
        //Comodidades
        self.elevadore = locacao.comodidades.elevadore
        self.mobiliado = locacao.comodidades.mobiliado
        self.acessibilidade = locacao.comodidades.acessibilidade
        self.aquecimento = locacao.comodidades.aquecimento
        self.piscina = locacao.comodidades.piscina
        self.quintal = locacao.comodidades.quintal
        self.jardim = locacao.comodidades.jardim
        self.animais = locacao.comodidades.animais
        self.portaria = locacao.comodidades.portaria
        self.brinquedoteca = locacao.comodidades.brinquedoteca
        self.academia = locacao.comodidades.academia
        //Pagamento
        self.valorIPTU = locacao.pagamento.valorIPTU
        self.valorAluguel = locacao.pagamento.valorAluguel
        self.tempoMaxContrato = locacao.pagamento.tempoMaxContrato
        self.tempoMinContrato = locacao.pagamento.tempoMinContrato
        self.diaVencimento = locacao.pagamento.diaVencimento
        self.ownerRecord = Users()
        //Endereço
        self.rua = locacao.local.rua
//        self.comodidades = ComodidadesRecord(comodidades: locacao.comodidades)
//        self.pagamento = PagamentoRecord(pagamento: locacao.pagamento)
//        self.endereco = EnderecoRecord(endereco: locacao.local)

    }
    
    
    
    
    
    //MARK: Getters & Setters
    //MARK: Imovel
    var emailDono: String{
        get{
            return(self.record.objectForKey("EmailDono") as! String)
        }
        set (val){
            record.setObject(val, forKey: "EmailDono")
        }
    }
    var pin: String{
        get{
            return(self.record.objectForKey("Pin") as! String)
        }
        set (val){
            record.setObject(val, forKey: "Pin")
        }
    }
    var bedrooms: Int{
        get{
            return(self.record.objectForKey("Bedrooms") as! Int)
        }
        set (val){
            record.setObject(val, forKey: "Bedrooms")
        }
    }
    var contratoIptu: Double{
        get{
            return(self.record.objectForKey("ContratoIptu") as! Double)
        }
        set (val){
            record.setObject(val, forKey: "ContratoIptu")
        }
        
    }
    var garageSpaces: Int{
        get{
            return(self.record.objectForKey("garageSpaces") as! Int)
        }
        set (val){
            record.setObject(val, forKey: "garageSpaces")
        }
        
    }
    var idLocacao: Int{
        get{
            return(self.record.objectForKey("idLocacao") as! Int)
        }
        set (val){
            record.setObject(val, forKey: "idLocacao")
        }
        
    }
    var size: Int{
        get{
            return(self.record.objectForKey("size") as! Int)
        }
        set (val){
            record.setObject(val, forKey: "size")
        }
        
    }
    var toilets: Int{
        get{
            return(self.record.objectForKey("toilets") as! Int)
        }
        set (val){
            record.setObject(val, forKey: "toilets")
        }
        
    }
    var valorCondominio: Int{
        get{
            return(self.record.objectForKey("valorCondominio") as! Int)
        }
        set (val){
            record.setObject(val, forKey: "valorCondominio")
        }
        
    }
    var tipoLocacao: Int{
        get{
            return(self.record.objectForKey("tipoLocacao") as! Int)
        }
        set (val){
            record.setObject(val, forKey: "tipoLocacao")
        }
        
    }
    var salas: Int{
        get{
            return(self.record.objectForKey("salas") as! Int)
        }
        set (val){
            record.setObject(val, forKey: "salas")
        }
        
    }
    var locTitle: String{
        get{
            return(self.record.objectForKey("locTitle") as! String)
        }
        set (val){
            record.setObject(val, forKey: "locTitle")
        }
        
    }
    var resumo: String{
        get{
            return(self.record.objectForKey("resumo") as! String)
        }
        set (val){
            record.setObject(val, forKey: "resumo")
        }
        
    }
    
    var tipoResidencia: String{
        get{
            return(self.record.objectForKey("tipoResidencia") as! String)
        }
        set (val){
            record.setObject(val, forKey: "tipoResidencia")
        }
        
    }
    
//    var localRef: CKReference{
//        get{
//            return(self.record.objectForKey("local") as! CKReference)
//        }
//        set (val){
//            record.setObject(val, forKey: "local")
//        }
//        
//    }
//    
    var owner: CKReference{
        get{
            return(self.record.objectForKey("owner") as! CKReference)
        }
        set (val){
            record.setObject(val, forKey: "owner")
        }
        
    }
//
//    var comodidadesRef: CKReference{
//        get{
//            return(self.record.objectForKey("comodidades") as! CKReference)
//        }
//        set (val){
//            record.setObject(val, forKey: "comodidades")
//        }
//        
//    }
//    
//    var pagamentoRef: CKReference{
//        get{
//            return(self.record.objectForKey("pagamento") as! CKReference)
//        }
//        set (val){
//            record.setObject(val, forKey: "pagamento")
//        }
//    
//    }
    
    var foto: CKAsset{
        get{
            return(self.record.objectForKey("foto") as! CKAsset)
        }
        set (val){
            record.setObject(val, forKey: "foto")
        }
        
    }
    //MARK: Comodidades
    var elevadore: Bool{
        get{
            return(self.record.objectForKey("elevadore") as! Bool)
        }
        set (val){
            record.setObject(val, forKey: "elevadore")
        }
    }
    var mobiliado: Bool{
        get{
            return(self.record.objectForKey("mobiliado") as! Bool)
        }
        set (val){
            record.setObject(val, forKey: "mobiliado")
        }
    }
    var acessibilidade: Bool{
        get{
            return(self.record.objectForKey("acessibilidade") as! Bool)
        }
        set (val){
            record.setObject(val, forKey: "acessibilidade")
        }
    }
    var aquecimento: Bool{
        get{
            return(self.record.objectForKey("aquecimento") as! Bool)
        }
        set (val){
            record.setObject(val, forKey: "aquecimento")
        }
    }
    var piscina: Bool{
        get{
            return(self.record.objectForKey("piscina") as! Bool)
        }
        set (val){
            record.setObject(val, forKey: "piscina")
        }
    }
    var quintal: Bool{
        get{
            return(self.record.objectForKey("quintal") as! Bool)
        }
        set (val){
            record.setObject(val, forKey: "quintal")
        }
    }
    var jardim: Bool{
        get{
            return(self.record.objectForKey("jardim") as! Bool)
        }
        set (val){
            record.setObject(val, forKey: "jardim")
        }
    }
    var animais: Bool{
        get{
            return(self.record.objectForKey("animais") as! Bool)
        }
        set (val){
            record.setObject(val, forKey: "animais")
        }
    }
    var portaria: Bool{
        get{
            return(self.record.objectForKey("portaria") as! Bool)
        }
        set (val){
            record.setObject(val, forKey: "portaria")
        }
    }
    var brinquedoteca: Bool{
        get{
            return(self.record.objectForKey("brinquedoteca") as! Bool)
        }
        set (val){
            record.setObject(val, forKey: "brinquedoteca")
        }
    }
    var academia: Bool{
        get{
            return(self.record.objectForKey("academia") as! Bool)
        }
        set (val){
            record.setObject(val, forKey: "academia")
        }
    }
    
    //MARK: Pagamento
    var valorAluguel : Double {
        get{
            return(self.record.objectForKey("valorAluguel") as! Double)
        }
        set (val){
            record.setObject(val, forKey: "valorAluguel")
        }
    }
    
    var valorIPTU : Double {
        get{
            return(self.record.objectForKey("valorIPTU") as! Double)
        }
        set (val){
            record.setObject(val, forKey: "valorIPTU")
        }
    }
    
    var tempoMinContrato : Int {
        get{
            return(self.record.objectForKey("tempoMinContrato") as! Int)
        }
        set (val){
            record.setObject(val, forKey: "tempoMinContrato")
        }
    }
    
    var tempoMaxContrato : Int {
        get{
            return(self.record.objectForKey("tempoMaxContrato") as! Int)
        }
        set (val){
            record.setObject(val, forKey: "tempoMaxContrato")
        }
    }
    
    var diaVencimento : Int {
        get{
            return(self.record.objectForKey("diaVencimento") as! Int)
        }
        set (val){
            record.setObject(val, forKey: "diaVencimento")
        }
    }
    //MARK: Endereco
    var cep : String {
        get{
            return(self.record.objectForKey("cep") as! String)
        }
        set (val){
            record.setObject(val, forKey: "cep")
        }
    }
    
    var complemento : String {
        get{
            return(self.record.objectForKey("complemento") as! String)
        }
        set (val){
            record.setObject(val, forKey: "complemento")
        }
    }
    
    var idEndereco : String {
        get{
            return(self.record.objectForKey("idEndereco") as! String)
        }
        set (val){
            record.setObject(val, forKey: "idEndereco")
        }
    }
    
    var numero : String {
        get{
            return(self.record.objectForKey("numero") as! String)
        }
        set (val){
            record.setObject(val, forKey: "numero")
        }
    }
    
    var rua : String {
        get{
            return(self.record.objectForKey("rua") as! String)
        }
        set (val){
            record.setObject(val, forKey: "rua")
        }
    }
}

