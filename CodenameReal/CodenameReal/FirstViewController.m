//
//  FirstViewController.m
//  CodenameReal
//
//  Created by Matheus Coelho Berger on 10/6/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

#import "FirstViewController.h"
#import "HomeTableViewCell.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface FirstViewController ()

@property (weak, nonatomic) IBOutlet UITableView *homeTableView;
@property (weak, nonatomic) NSMutableArray *homes;

@end

@implementation FirstViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.homeTableView.delegate = self;
    self.homeTableView.dataSource = self;
    
    //self.logoutButton.delegate = self;
    
    [self.homes addObject:@"teste"];
    [self.homes addObject:@"trocadilho"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goToCheckout:) name:@"goToCheckout" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goToRelation:) name:@"goToRelation" object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.tabBarController.tabBar.hidden = NO;
}

#pragma TableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView registerClass:[HomeTableViewCell class] forCellReuseIdentifier:@"homeTableViewCell"];
    
    [tableView registerNib:[UINib nibWithNibName:@"HomeTableViewCell" bundle:nil] forCellReuseIdentifier:@"homeTableViewCell"];
    
    HomeTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"homeTableViewCell"];
    
    if (cell == nil)
    {
        cell = [[HomeTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"homeTableViewCell"];
    }
    else
    {
        NSLog(@"deu bom");
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%@", indexPath);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 375;
}

/*
#pragma Logout

- (void)loginButton:(FBSDKLoginButton *)loginButton didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result error:(NSError *)error
{
    NSLog(@"fez login");
}

-(void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton
{
    NSLog(@"fez logout");
    UITabBarController *tab = self.tabBarController;
    if (tab)
    {
        NSLog(@"achou o tab bar");
        [tab performSegueWithIdentifier:@"logout" sender:self];
    }
}
 */

#pragma Observations

- (void) goToCheckout: (NSNotification *)notification
{
    //SEL goToCheckoutSelector = @selector(goToCheckout:);
    
    [self performSegueWithIdentifier:@"goToCheckout" sender:self];
}

- (void) goToRelation: (NSNotification *)notification
{
    [self performSegueWithIdentifier:@"goToRelation" sender:self];
}


@end
