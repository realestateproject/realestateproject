//
//  CheckOutViewController.h
//  CodenameReal
//
//  Created by Matheus Coelho Berger on 10/14/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PayPalMobile.h>

@interface CheckOutViewController : UIViewController <PayPalPaymentDelegate>

@property (weak, nonatomic) NSString *tituloImovel;
@property (weak, nonatomic) NSString *nome;

@property (weak, nonatomic) NSString *dataVencimento;
@property (weak, nonatomic) NSString *valorPagamento;
@property (weak, nonatomic) NSString *moeda;

@property (weak, nonatomic) NSString *statusPagamento;
@end
