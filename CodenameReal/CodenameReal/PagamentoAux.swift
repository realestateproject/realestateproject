//
//  PagamentoAux.swift
//  CodenameReal
//
//  Created by Thiago Borges Jordani on 11/26/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

import Foundation

class PagamentoAux : NSObject {
    var valorAluguel: Double = 0.0
    var valorIPTU: Double = 0.0
    var tempoMinContrato: Int = 0
    var tempoMaxContrato: Int = 0
    var diaVencimento: Int = 0
}