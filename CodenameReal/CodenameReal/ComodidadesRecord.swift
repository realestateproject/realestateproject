////
////  ComodidadesRecord.swift
////  CodenameReal
////
////  Created by Thiago Borges Jordani on 12/3/15.
////  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
////
//
//import Foundation
//import CloudKit
//
//class ComodidadesRecord: NSObject {
//    let record: CKRecord
//    var owner:Users
//    
//    override init(){
//        record = CKRecord(recordType: "Comodidades")
//    }
//    
//    init(comodidades: ComodidadesAux){
//        record = CKRecord(recordType: "Comodidades")
//        
//        super.init()
//        self.elevadore = comodidades.elevadore
//        self.mobiliado = comodidades.mobiliado
//        self.acessibilidade = comodidades.acessibilidade
//        self.aquecimento = comodidades.aquecimento
//        self.piscina = comodidades.piscina
//        self.quintal = comodidades.quintal
//        self.jardim = comodidades.jardim
//        self.animais = comodidades.animais
//        self.portaria = comodidades.portaria
//        self.brinquedoteca = comodidades.brinquedoteca
//        self.academia = comodidades.academia
//    }
//    
//    var locacaoPin: String{
//        get{
//            return(self.record.objectForKey("locacaoPin") as! String)
//        }
//        set (val){
//            record.setObject(val, forKey: "locacaoPin")
//        }
//    }
//    var elevadore: Bool{
//        get{
//            return(self.record.objectForKey("elevadore") as! Bool)
//        }
//        set (val){
//            record.setObject(val, forKey: "elevadore")
//        }
//    }
//    var mobiliado: Bool{
//        get{
//            return(self.record.objectForKey("mobiliado") as! Bool)
//        }
//        set (val){
//            record.setObject(val, forKey: "mobiliado")
//        }
//    }
//    var acessibilidade: Bool{
//        get{
//            return(self.record.objectForKey("acessibilidade") as! Bool)
//        }
//        set (val){
//            record.setObject(val, forKey: "acessibilidade")
//        }
//    }
//    var aquecimento: Bool{
//        get{
//            return(self.record.objectForKey("aquecimento") as! Bool)
//        }
//        set (val){
//            record.setObject(val, forKey: "aquecimento")
//        }
//    }
//    var piscina: Bool{
//        get{
//            return(self.record.objectForKey("piscina") as! Bool)
//        }
//        set (val){
//            record.setObject(val, forKey: "piscina")
//        }
//    }
//    var quintal: Bool{
//        get{
//            return(self.record.objectForKey("quintal") as! Bool)
//        }
//        set (val){
//            record.setObject(val, forKey: "quintal")
//        }
//    }
//    var jardim: Bool{
//        get{
//            return(self.record.objectForKey("jardim") as! Bool)
//        }
//        set (val){
//            record.setObject(val, forKey: "jardim")
//        }
//    }
//    var animais: Bool{
//        get{
//            return(self.record.objectForKey("animais") as! Bool)
//        }
//        set (val){
//            record.setObject(val, forKey: "animais")
//        }
//    }
//    var portaria: Bool{
//        get{
//            return(self.record.objectForKey("portaria") as! Bool)
//        }
//        set (val){
//            record.setObject(val, forKey: "portaria")
//        }
//    }
//    var brinquedoteca: Bool{
//        get{
//            return(self.record.objectForKey("brinquedoteca") as! Bool)
//        }
//        set (val){
//            record.setObject(val, forKey: "brinquedoteca")
//        }
//    }
//    var academia: Bool{
//        get{
//            return(self.record.objectForKey("academia") as! Bool)
//        }
//        set (val){
//            record.setObject(val, forKey: "academia")
//        }
//    }
//}