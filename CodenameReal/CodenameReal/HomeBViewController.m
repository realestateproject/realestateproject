//
//  HomeBViewController.m
//  CodenameReal
//
//  Created by Matheus Coelho Berger on 10/27/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

#import "HomeBViewController.h"
#import "ImovelHomeViewController.h"
#import "CodenameReal-Swift.h"
#import <CloudKit/CloudKit.h>

@interface HomeBViewController ()

@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) NSMutableArray *imoveis;
@property (weak, nonatomic) IBOutlet UIVisualEffectView *baseForPageView;

@property (weak, nonatomic) IBOutlet UILabel *moedaValorTotal;
@property (weak, nonatomic) IBOutlet UILabel *moedaValorAtrasado;

@property (weak, nonatomic) IBOutlet UILabel *valorTotal;
@property (weak, nonatomic) IBOutlet UILabel *valorAtrasado;
@property (weak, nonatomic) IBOutlet UIImageView *imageFudo;

@end

@implementation HomeBViewController


#pragma mark - Activity Indicator Methods
-(void)indicatorLoadStart{
    self.activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    self.activityIndicator.backgroundColor = [UIColor colorWithWhite:0.1 alpha:0.7];
    self.activityIndicator.frame = self.view.frame;
    self.activityIndicator.center = self.view.center;
    [self.activityIndicator startAnimating];
    [self.view addSubview: self.activityIndicator];
    [self.view bringSubviewToFront: self.activityIndicator];
}

-(void)indicatorLoadStop{
    [self.activityIndicator stopAnimating];
    [self.activityIndicator removeFromSuperview];
}

#pragma mark - Alert Methods
-(void)alert:(NSString*)text{
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Ops!!!" message:text preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Certo" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        switch (action.style){
            case UIAlertActionStyleDefault:
                NSLog(@"default");
                
                NSLog(@" colocar foco no textfield");
                
            case UIAlertActionStyleCancel:
                NSLog(@"cancel");
                
            case UIAlertActionStyleDestructive:
                NSLog(@"destructive");
        }
    }]];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alert animated:true completion:nil];
    });
}

#pragma mark - Lifecycle Methods
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.moedaValorTotal.text = @"BRL";
    self.moedaValorAtrasado.text = @"BRL";
    
    self.valorTotal.text = @"700,00";
    self.valorAtrasado.text = @"0,00";
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goToCheckout:) name:@"goToCheckout" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goToRelation:) name:@"goToRelation" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goToPin:) name:@"goToPin" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addCard:) name:@"addCard" object:nil];
    
    self.imoveis = [[NSMutableArray alloc] init];
    
    
    //gambiarra
    
    ImovelHomeViewController *imovel = [self viewControllerAtIndex:0];
    NSLog(@"o view controller é: %@", imovel);
    
    self.imageFudo.image = [UIImage imageNamed:@"Background_750x1334"];
    
    [self.imoveis addObject:imovel];
    
    NSLog(@"ele reconheceu que o page view controller nao foi criado");
    
    NSLog(@"criando pageview controller");
    
    self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    
    self.pageController.dataSource = self;
    
    [self.pageController.view setFrame:self.baseForPageView.frame];
    
    [self.pageController setViewControllers:self.imoveis direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    [self addChildViewController:self.pageController];
    [self.view addSubview:self.pageController.view];
    [self.pageController didMoveToParentViewController:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.tabBarController.tabBar.hidden = NO;
    
    if (![CodenameRealStaticObjects updatedLocatarioLocations]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self indicatorLoadStart];
        });
        [[[LocationManager alloc]initWithDatabase:[[CKContainer defaultContainer] privateCloudDatabase]] fetchLocationsForLocatarioWithEmail:[CodenameRealStaticObjects loggedUser].email callback:^( BOOL success, NSArray<LocationRecord *> * results) {
            if (success) {
                self.imoveis = results.mutableCopy;
                CodenameRealStaticObjects.locatadorLocations = results.mutableCopy;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self addCard:nil];
                    [self indicatorLoadStop];
                });
                NSLog(@"Recebeu: %lu imóveis",(unsigned long)[results count]);
                
                CodenameRealStaticObjects.updatedLocatadorLocations = true;
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self indicatorLoadStop];
                });
                [self alert:@"Não foi possivel carregar imoveis"];
            }
        }];
    }else{
        dispatch_async(dispatch_get_main_queue(), ^{
            self.imoveis = CodenameRealStaticObjects.locatarioLocations.mutableCopy;
            //            [self.pageController reloadInputViews];
            [self addCard:nil];
        });
    }
}

#pragma PageView

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = [(ImovelHomeViewController *)viewController index];
    
    if (index == 0)
    {
        return nil;
    }
    
    index--;
    
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = [(ImovelHomeViewController *)viewController index];
    
    if (index == [self.imoveis count] -1)
    {
        return nil;
    }
    
    index++;
    
    return [self viewControllerAtIndex:index];
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return [self.imoveis count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;
}

- (ImovelHomeViewController *) viewControllerAtIndex:(NSUInteger)index
{
    ImovelHomeViewController *imovel = [[ImovelHomeViewController alloc] init];
    
    imovel = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeContentView"];
    
    NSLog(@"existem %lu objetos dentro do array", (unsigned long)[self.imoveis count]);
    
    switch (index)
    {
        case 0:
            imovel.stringMoeda = @"BRL";
            
            imovel.titulo = @"Apartamento nas Graças";
            imovel.stringNomeLocador = @"Milone da Silva";
            
            imovel.stringValor = @"700,00";
            imovel.stringDataVencimento = @"5 de dezembro";
            
            imovel.stringStatusPagamento = @"O prazo do pagamento é até amanhã";
            imovel.textoBotaoPagar = @"Pagar Aluguel";
            
            imovel.imagemNome = @"Foto_placeholder_750x612";
            
            imovel.index = index;
            break;
        case 1:
            imovel.stringMoeda = @"BRL";
            
            imovel.titulo = @"Stronda's Apartament";
            imovel.stringNomeLocador = @"Stronda Sativo";
            
            imovel.stringValor = @"500,00";
            imovel.stringDataVencimento = @"November 5th";
            
            imovel.stringStatusPagamento = @"Everything ok";
            imovel.textoBotaoPagar = @"Pay rent";
            
            imovel.imagemNome = @"Foto_placeholder_750x612";
            
            imovel.index = index;
            break;
        default:
            break;
    }

    return imovel;
}

#pragma Observations

- (void) goToCheckout: (NSNotification *)notification
{
    //SEL goToCheckoutSelector = @selector(goToCheckout:);
    
    [self performSegueWithIdentifier:@"goToCheckout" sender:self];
}

- (void) goToRelation: (NSNotification *)notification
{
    [self performSegueWithIdentifier:@"goToRelation" sender:self];
}

- (void) goToPin: (NSNotification *) notification
{
    [self.tabBarController setSelectedIndex:1];
}

- (void)addCard: (NSNotification *) notification
{
    NSLog(@"entrou na função de add card");
    
   
    if (!self.pageController)
    {
        ImovelHomeViewController *imovel = [self viewControllerAtIndex:0];
        NSLog(@"o view controller é: %@", imovel);
        
        self.imageFudo.image = [UIImage imageNamed:@"Background_750x1334"];
        
        [self.imoveis addObject:imovel];
        
        NSLog(@"ele reconheceu que o page view controller nao foi criado");
        
        NSLog(@"criando pageview controller");
        
        self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
         
        self.pageController.dataSource = self;
         
        [self.pageController.view setFrame:self.baseForPageView.frame];
         
        [self.pageController setViewControllers:self.imoveis direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
         
        [self addChildViewController:self.pageController];
        [self.view addSubview:self.pageController.view];
        [self.pageController didMoveToParentViewController:self];
    }
    else
    {
        NSLog(@"adicionando objeto no array");
        
        [self.imoveis addObject:@"teste"];
        
        [self.pageController reloadInputViews];
    }
}

@end
