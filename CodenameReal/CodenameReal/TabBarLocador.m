//
//  TabBarLocadorViewController.m
//  CodenameReal
//
//  Created by jesse filho on 11/25/15.
//  Copyright © 2015 Matheus Coelho Berger. All rights reserved.
//

#import "TabBarLocador.h"
#import "UIColor+Locador.h"



// itens in tab bar
NSString *icon_Alugados_Default = @"Tabbar_1_Alugados_Default_50x50";
NSString *icon_Alugados_Selected =@"Tabbar_1_Alugados_Selected_50x50";
NSString *icon_BuscaLocador_Default = @"Tabbar_2_Busca_Default";
NSString *icon_BuscaLocador_Selected =@"Tabbar_2_Busca_Selected";
NSString *icon_Home_Default = @"Tabbar_1_Home_Default";
NSString *icon_Home_Selected =@"Tabbar_1_Home_Selected";
NSString *icon_PerfilLocador_Default = @"Tabbar_5_UserBar_Default";
NSString *icon_PerfilLocador_Selected =@"Tabbar_5_UserBar";


@interface TabBarLocador ()

@end

@implementation TabBarLocador

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self ResetApparence];
    [self appearanceTabBar];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:true];
    [self ResetApparence];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) appearanceTabBar{
    
    [[UITabBar appearance] setShadowImage:nil];
    
    UITabBarItem *tabBarItem = [self.tabBar.items objectAtIndex:0];
    UIImage *unselectedImage = [UIImage imageNamed:icon_Alugados_Default];
    UIImage *selectedImage = [UIImage imageNamed:icon_Alugados_Selected];
    [tabBarItem setImage: [unselectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tabBarItem setSelectedImage: selectedImage];
    
    UITabBarItem *tabBarItem1 = [self.tabBar.items objectAtIndex:1];
    UIImage *unselectedImage1 = [UIImage imageNamed:icon_BuscaLocador_Default];
    UIImage *selectedImage1 = [UIImage imageNamed:icon_BuscaLocador_Selected];
    [tabBarItem1 setImage: [unselectedImage1 imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tabBarItem1 setSelectedImage: selectedImage1];
    
    UITabBarItem *tabBarItem2 = [self.tabBar.items objectAtIndex:2];
    UIImage *unselectedImage2 = [UIImage imageNamed:icon_Home_Default];
    UIImage *selectedImage2 = [UIImage imageNamed:icon_Home_Selected];
    [tabBarItem2 setImage: [unselectedImage2 imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tabBarItem2 setSelectedImage: selectedImage2];
    
    UITabBarItem *tabBarItem3 = [self.tabBar.items objectAtIndex:3];
    UIImage *unselectedImage3 = [UIImage imageNamed:icon_PerfilLocador_Default];
    UIImage *selectedImage3 = [UIImage imageNamed:icon_PerfilLocador_Selected];
    [tabBarItem3 setImage: [unselectedImage3 imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tabBarItem3 setSelectedImage: selectedImage3];
    

 
}

-(void) ResetApparence{
    //Reset to native style bar
    [[UITabBar appearance] setShadowImage:nil];
    [[UITabBar appearance] setBackgroundImage:nil];
    [[UITabBar appearance] setTintColor:[UIColor whiteColor]];
}

@end
